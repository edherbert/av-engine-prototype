#include "iOS.h"
//#include "../../Base.h"

//#include <SDL_main.h>
#import <Foundation/Foundation.h>

int main(int argc, char *argv[]){
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
