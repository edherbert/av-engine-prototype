#ifndef __MACOS__H
#define __MACOS__H

#import "Foundation/Foundation.h"
#import <AppKit/NSApplication.h>
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CVDisplayLink.h>

#include "../../Window.h"

#include <Ogre.h>
#include <OgreHlmsPbs.h>
#include <Compositor/OgreCompositorManager2.h>

#include <iostream>

@interface AppDelegate : NSObject <NSApplicationDelegate>{
    Window *window;
    
    Ogre::Root *root;
    std::string bundlePath;
    Ogre::CompositorWorkspace* workspace;
    Ogre::SceneManager *sceneManager;
    Ogre::Camera *camera;
    
    //CVDisplayLinkRef *displayLink;
    NSTimer *timer;
    
    Ogre::SceneNode *node;
    float count;
}

- (void)go;
- (void)renderOneFrame: (id)sender;
- (void)shutdown;

@property (retain, atomic) NSTimer *mTimer;
@property (nonatomic) NSTimeInterval mLastFrameTime;

@end

@implementation AppDelegate

@synthesize mTimer;
@dynamic mLastFrameTime;

- (NSTimeInterval)mLastFrameTime{
	//return mLastFrameTime;
    return 0;
}

- (void)setLastFrameTime:(NSTimeInterval)frameInterval{
	
}

- (void)go{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	//mLastFrameTime = 1;
	mTimer = nil;

    mTimer = [[NSTimer timerWithTimeInterval: 0.001 target:self selector:@selector(renderOneFrame:) userInfo:self repeats:true] retain];
    [[NSRunLoop currentRunLoop] addTimer:mTimer forMode: NSDefaultRunLoopMode];
    [[NSRunLoop currentRunLoop] addTimer:mTimer forMode: NSEventTrackingRunLoopMode];
    
    NSLog(@"The application is starting");

    [pool release];
}

- (void)applicationWillFinishLaunching:(NSNotification *)notification{
    NSLog(@"Doing this bit");
    bundlePath = [[[NSBundle mainBundle] resourcePath] cStringUsingEncoding:NSUTF8StringEncoding];
    
    //root = new Ogre::Root(bundlePath + "/setup/plugins_mac.cfg");
    root = new Ogre::Root();
    root->loadPlugin("RenderSystem_Metal");
    root->loadPlugin("RenderSystem_GL3Plus");
    //root->loadPlugin("OgreHlmsPbs");
    
    //See if this returns a variable and then try to do something with that.
    root->showConfigDialog();
    
    root->addResourceLocation(bundlePath + "/Media", "FileSystem");
    
    //root->addResourceLocation("../media", "FileSystem");
    root->getRenderSystem()->setConfigOption( "sRGB Gamma Conversion", "Yes" );
    root->initialise(false);
    
    
    window = new Window();
    
    
    
    Ogre::String rootHlmsFolder = "../setup/Hlms";
    
    //Ogre::Archive *archiveLibrary = Ogre::ArchiveManager::getSingletonPtr()->load("/home/edward/Documents/ogre/Samples/Media/Hlms/Common/GLSL", "FileSystem", true );
    
    Ogre::Archive *archiveLibrary = Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Media/Hlms/Common/Metal", "FileSystem", true );
    //Ogre::Archive *archiveLibrary = Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Media/Hlms/Common/GLSL", "FileSystem", true );
    
    Ogre::ArchiveVec library;
    library.push_back( archiveLibrary );
    
    Ogre::Archive *archivePbs = Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Media/Hlms/Pbs/Metal", "FileSystem", true );
    //Ogre::Archive *archivePbs = Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Media/Hlms/Pbs/GLSL", "FileSystem", true );
    Ogre::HlmsPbs *hlmsPbs = OGRE_NEW Ogre::HlmsPbs( archivePbs, &library );
    
    root->getHlmsManager()->registerHlms(hlmsPbs);
    
    
    sceneManager = root->createSceneManager(Ogre::ST_GENERIC, 2, Ogre::INSTANCING_CULLING_SINGLETHREAD, "Scene Manager");
    
    
    camera = sceneManager->createCamera("Main Camera");
    
    
    
    Ogre::CompositorManager2 *compositorManager = root->getCompositorManager2();
    
    const Ogre::String workspaceName("test Workspace");
    if(!compositorManager->hasWorkspaceDefinition(workspaceName)){
        compositorManager->createBasicWorkspaceDef(workspaceName, Ogre::ColourValue(1, 0, 0, 1), Ogre::IdString());
    }
    
    workspace = compositorManager->addWorkspace(sceneManager, window->getRenderWindow(), camera, workspaceName, true);
    
    
    node = sceneManager->getRootSceneNode()->createChildSceneNode(Ogre::SCENE_DYNAMIC);
    Ogre::Item *item = sceneManager->createItem("ogrehead2.mesh", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
    node->attachObject((Ogre::MovableObject*)item);
    
    sceneManager->setAmbientLight( Ogre::ColourValue( 0.3f, 0.3f, 0.3f ), Ogre::ColourValue( 0.02f, 0.53f, 0.96f ) * 0.1f, Ogre::Vector3::UNIT_Y );
    
    camera->setPosition(0, 0, 150);
    camera->lookAt(0, 0, 0);
}

- (void)applicationDidFinishLaunching:(NSNotification *)application{
    /*void *uiViewPtr = 0;
    window->getRenderWindow()->getCustomAttribute("MetalRenderTargetCommon", uiViewPtr);
    NSView *view = CFBridgingRelease(uiViewPtr);*/
	//timer = [[NSview]]
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1/60 target:self selector:@selector(redraw) userInfo:nil repeats:YES];

	[self go];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification{

}

- (void)shutdown{
	[NSApp terminate:nil];
}

- (void)renderOneFrame:(id)sender{

}

- (void)redraw{
    root->renderOneFrame();
    
    count++;
    
    node->setPosition(count, count, count);
    
}


@end

#endif
