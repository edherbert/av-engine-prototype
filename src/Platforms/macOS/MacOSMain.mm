#import "Foundation/Foundation.h"
#include "../../Base.h"

//Start the mac side of things
//This is only compiled on the mac
//Most of the interesting code is in the header
int main(int argc, char *argv[]){
    @autoreleasepool {
        //Get the resource path for base
        Base *base = new Base([[[NSBundle mainBundle] resourcePath] cStringUsingEncoding:NSUTF8StringEncoding]);
        while(base->isRunning()){
            base->update();
        }
    }
    
    return 0;
}
