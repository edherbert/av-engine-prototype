#include "../../Base.h"

int main(){
    AV::Base *base = new AV::Base("../resources");

    while(base->isRunning()){
        base->update();
    }

    return 0;
}
