#ifndef __EVENT_MANAGER_H__
#define __EVENT_MANAGER_H__

//#include <iostream>

#include "Events/Event.h"
//#include "EventReceiver.h"

#include <vector>

namespace AV{
  class Event;
  class EventReceiver;

  class InputEvent;
  class DialogEvent;
  class StatsEvent;
  class TypedEvent;
  class ActionEvent;
  class WorldEvent;
  class TriggerEvent;

  class EventManager{
  public:
    EventManager();
    ~EventManager();

    //static void transmitEvent(Event e);

    static void subscribeToEvent(EventReceiver *receiver, EventType type);
    static void unsubscribeToEvent(EventReceiver *receiver, EventType type);

    /*template <typename E>
    static void transmitEvent(E e){
      switch(e.getEventType()){
        case EVENT_TYPE_NULL:
          std::cout << "The event is null" << '\n';
          break;
        case EVENT_TYPE_INPUT:
          _transmitEvent(e, inputEventSubscribers);
          break;
      }
      //_transmitEvent(e);
    }*/

    static void transmitInputEvent(InputEvent &e);
    static void transmitDialogEvent(DialogEvent &e);
    static void transmitStatsEvent(StatsEvent &e);
    static void transmitTypedEvent(TypedEvent &e);
    static void transmitActionEvent(ActionEvent &e);
    static void transmitWorldEvent(WorldEvent &e);
    static void transmitTriggerEvent(TriggerEvent &e);


  private:
    //static void transmitInputEvent(InputEvent &e);
    static std::vector<EventReceiver*>& _getSubscriberList(EventType type);

    template <typename E>
    static void _transmitEvent(E e, std::vector<EventReceiver*> &v){
      for(EventReceiver* r : v){
        //r->notifyInputEvent((InputEvent&)e);
        r->notifyInputEvent(e);
      }
    }

    static std::vector<EventReceiver*> inputEventSubscribers;
    static std::vector<EventReceiver*> dialogEventSubscribers;
    static std::vector<EventReceiver*> statsEventSubscribers;
    static std::vector<EventReceiver*> typedEventSubscribers;
    static std::vector<EventReceiver*> actionEventSubscribers;
    static std::vector<EventReceiver*> worldEventSubscribers;
    static std::vector<EventReceiver*> triggerEventSubscribers;
  };
}

#endif
