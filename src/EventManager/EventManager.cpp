#include "EventManager.h"

#include "EventReceiver.h"

#include "Events/InputEvent.h"
#include "Events/DialogEvent.h"

#include <iostream>

namespace AV{

  std::vector<EventReceiver*> EventManager::inputEventSubscribers = std::vector<EventReceiver*>();
  std::vector<EventReceiver*> EventManager::dialogEventSubscribers = std::vector<EventReceiver*>();
  std::vector<EventReceiver*> EventManager::statsEventSubscribers = std::vector<EventReceiver*>();
  std::vector<EventReceiver*> EventManager::typedEventSubscribers = std::vector<EventReceiver*>();
  std::vector<EventReceiver*> EventManager::actionEventSubscribers = std::vector<EventReceiver*>();
  std::vector<EventReceiver*> EventManager::worldEventSubscribers = std::vector<EventReceiver*>();
  std::vector<EventReceiver*> EventManager::triggerEventSubscribers = std::vector<EventReceiver*>();

  EventManager::EventManager(){

  }

  EventManager::~EventManager(){

  }

  // void EventManager::transmitEvent(Event e){
  //   switch(e.getEventType()){
  //     case EVENT_TYPE_NULL:
  //       std::cout << "The event is null" << '\n';
  //       break;
  //     case EVENT_TYPE_INPUT:
  //       transmitInputEvent(e);
  //       break;
  //   }
  // }

  /*void EventManager::transmitInputEvent(InputEvent &e){
    for(EventReceiver* r : inputEventSubscribers){
      //r->notifyInputEvent((InputEvent&)e);
      r->notifyInputEvent(e);
    }
  }*/

  void EventManager::transmitInputEvent(InputEvent &e){
    for(EventReceiver* r : inputEventSubscribers){
      r->notifyInputEvent(e);
    }
  }

  void EventManager::transmitDialogEvent(DialogEvent &e){
    for(EventReceiver* r : dialogEventSubscribers){
      r->notifyDialogEvent(e);
    }
  }

  void EventManager::transmitStatsEvent(StatsEvent &e){
    for(EventReceiver* r : statsEventSubscribers){
      r->notifyStatsEvent(e);
    }
  }

  void EventManager::transmitTypedEvent(TypedEvent &e){
    for(EventReceiver* r : typedEventSubscribers){
      r->notifyTypedEvent(e);
    }
  }

  void EventManager::transmitActionEvent(ActionEvent &e){
    for(EventReceiver* r : actionEventSubscribers){
      r->notifyActionEvent(e);
    }
  }

  void EventManager::transmitWorldEvent(WorldEvent &e){
    for(EventReceiver* r : worldEventSubscribers){
      r->notifyWorldEvent(e);
    }
  }

  void EventManager::transmitTriggerEvent(TriggerEvent &e){
    for(EventReceiver* r : triggerEventSubscribers){
      r->notifyTriggerEvent(e);
    }
  }

  void EventManager::subscribeToEvent(EventReceiver *receiver, EventType type){
    _getSubscriberList(type).push_back(receiver);
  }

  void EventManager::unsubscribeToEvent(EventReceiver *receiver, EventType type){
    std::vector<EventReceiver*>& list = _getSubscriberList(type);
    bool found = false;
    for(int i = 0; i < list.size(); i++){
      if(list[i] == receiver){
        found = true;
        list.erase(list.begin() + i);
        break;
      }
    }
    if(!found) std::cout << "That receiver could not be unsubscribed because it couldn't be found in the list." << '\n';
  }

  std::vector<EventReceiver*>& EventManager::_getSubscriberList(EventType type){
    switch(type){
      case EVENT_TYPE_INPUT:
        return inputEventSubscribers;
        break;
      case EVENT_TYPE_DIALOG:
        return dialogEventSubscribers;
        break;
      case EVENT_TYPE_STATS:
        return statsEventSubscribers;
        break;
      case EVENT_TYPE_TYPED:
        return typedEventSubscribers;
        break;
      case EVENT_TYPE_ACTION:
        return actionEventSubscribers;
        break;
      case EVENT_TYPE_WORLD:
        return worldEventSubscribers;
        break;
      case EVENT_TYPE_TRIGGER:
        return triggerEventSubscribers;
        break;
    }
  }
}
