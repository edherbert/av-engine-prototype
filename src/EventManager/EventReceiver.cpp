#include "EventReceiver.h"

#include <iostream>

namespace AV{
  EventReceiver::EventReceiver(){

  }

  EventReceiver::~EventReceiver(){

  }

  void EventReceiver::notifyInputEvent(InputEvent &e){

  }

  void EventReceiver::notifyDialogEvent(DialogEvent &e){

  }

  void EventReceiver::notifyStatsEvent(StatsEvent &e){

  }

  void EventReceiver::notifyTypedEvent(TypedEvent &e){

  }

  void EventReceiver::notifyActionEvent(ActionEvent &e){

  }

  void EventReceiver::notifyWorldEvent(WorldEvent &e){

  }

  void EventReceiver::notifyTriggerEvent(TriggerEvent &e){
    
  }
}
