#ifndef __EVENT_RECEIVER_H__
#define __EVENT_RECEIVER_H__

//#include "Events/Event.h"
//#include "Events/InputEvent.h"

namespace AV{
  class InputEvent;
  class DialogEvent;
  class StatsEvent;
  class TypedEvent;
  class ActionEvent;
  class WorldEvent;
  class TriggerEvent;

  class EventReceiver{
  public:
    EventReceiver();
    ~EventReceiver();

    virtual void notifyInputEvent(InputEvent &e);
    virtual void notifyDialogEvent(DialogEvent &e);
    virtual void notifyStatsEvent(StatsEvent &e);
    virtual void notifyTypedEvent(TypedEvent &e);
    virtual void notifyActionEvent(ActionEvent &e);
    virtual void notifyWorldEvent(WorldEvent &e);
    virtual void notifyTriggerEvent(TriggerEvent &e);
  };
}

#endif
