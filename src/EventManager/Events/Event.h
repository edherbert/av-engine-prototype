#ifndef __EVENT_H__
#define __EVENT_H__

namespace AV{
  enum EventType{
    EVENT_TYPE_NULL,
    EVENT_TYPE_INPUT,
    EVENT_TYPE_DIALOG,
    EVENT_TYPE_STATS,
    EVENT_TYPE_TYPED,
    EVENT_TYPE_ACTION,
    EVENT_TYPE_WORLD,
    EVENT_TYPE_TRIGGER
  };

  class Event{
  public:
    Event(EventType e) : e(e) { };
    ~Event() { };

    EventType getEventType() { return e; }

  private:
    EventType e;
  };
}

#endif
