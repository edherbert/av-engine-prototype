#pragma once

#include "Event.h"
#include "../../world/Slots/ESlotPosition.h"
#include <OgreString.h>

namespace AV{
  enum WORLD_EVENT_TYPE{
    WORLD_EVENT_NULL,
    WORLD_EVENT_CHUNK_LOADED,
    WORLD_EVENT_E_CHUNK_LOADED,
    WORLD_EVENT_CHUNK_UNLOADED,
    WORLD_EVENT_E_CHUNK_UNLOADED,
    WORLD_EVENT_MAP_CHANGED
  };

  class WorldEvent : public Event{
  public:
    WorldEvent() : Event(EVENT_TYPE_WORLD) { };
    ~WorldEvent() { };

    WORLD_EVENT_TYPE eventType = WORLD_EVENT_NULL;

    ESlotPosition eSlot;
    Ogre::String mapName = "";
  };
}
