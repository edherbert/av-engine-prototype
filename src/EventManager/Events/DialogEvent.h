#ifndef __DIALOG_EVENT_H__
#define __DIALOG_EVENT_H__

#include "Event.h"
#include <string>

namespace AV{
  enum DIALOG_EVENT_TYPE{
    DIALOG_NULL,
    DIALOG_START,
    DIALOG_END,
    DIALOG_INCREMENT,
    DIALOG_TEXT
  };
  class DialogEvent : public Event{
  public:
    DialogEvent()
      : Event(EVENT_TYPE_DIALOG) { };

    DIALOG_EVENT_TYPE dialogType = DIALOG_NULL;

    std::string dialogPath = "";
    int startingBlock = 0;

    const char* dialogText = "";
  };
}

#endif
