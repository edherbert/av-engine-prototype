#ifndef __INPUT_EVENT_H__
#define __INPUT_EVENT_H__

#include "Event.h"
#include "../../Input.h"

namespace AV{
  enum InputType{
    TYPE_NULL,
    KEY_DOWN,
    KEY_UP
  };

  class InputEvent : public Event{
  public:
    InputEvent(InputType i, INPUT_TYPE k)
      : inputType(i),
        keyValue(k),
        Event(EVENT_TYPE_INPUT) { };

    InputType inputType = TYPE_NULL;
    INPUT_TYPE keyValue = I_NULL;
  };
}

#endif
