#pragma once

#include "Event.h"

#include "../../Input.h"
#include <string>

namespace AV{
  enum ActionDataActionType{
    ACTION_TRIGGER_TYPE_SCRIPT,
    ACTION_TRIGGER_TYPE_DIALOG
  };

  struct ActionData{
    std::string actionDescription = "talk";
    INPUT_TYPE inputId = I_ACCEPT;
    ActionDataActionType actionType = ACTION_TRIGGER_TYPE_DIALOG;
    std::string resourceName = "emptyDialog";
  };

  enum ACTION_EVENT_TYPE{
    ACTION_EVENT_TYPE_ENTER,
    ACTION_EVENT_TYPE_LEAVE
  };

  class ActionEvent : public Event{
  public:
    ActionEvent() : Event(EVENT_TYPE_TYPED) { };
    ~ActionEvent() { };

    ActionData data;
    ACTION_EVENT_TYPE eventType;
  };
}
