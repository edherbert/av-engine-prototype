#pragma once

#include "Event.h"

namespace AV{
  class TArea;

  enum TRIGGER_EVENT_TYPE{
    TRIGGER_EVENT_NULL,
    TRIGGER_EVENT_TAREA_BOX,
    TRIGGER_EVENT_TAREA_OUTER_BOX,
    TRIGGER_EVENT_TAREA_TRIGGER
  };

  class TriggerEvent : public Event{
  public:
    TriggerEvent() : Event(EVENT_TYPE_TRIGGER) { };
    ~TriggerEvent() { };

    TArea *tarea = 0;
    bool triggerEntered = true;

    TRIGGER_EVENT_TYPE type = TRIGGER_EVENT_NULL;
  };
}
