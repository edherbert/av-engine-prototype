#pragma once

#include "Event.h"

namespace AV{
  enum TypedEventType{
    TYPE_EVENT_NULL,
    TYPED_EVENT_CHAR,
    TYPED_EVENT_START,
    TYPED_EVENT_COMMAND,
    TYPED_EVENT_END
  };
  enum TypedEventCommand{
    TYPED_EVENT_BACKSPACE,
    TYPED_EVENT_ENTER
  };
  class TypedEvent : public Event{
  public:
    TypedEvent() : Event(EVENT_TYPE_TYPED) { };
    ~TypedEvent() { };

    TypedEventType type = TYPE_EVENT_NULL;
    const char* text;
    TypedEventCommand command;
  };
}
