#ifndef __STATS_EVENT_H__
#define __STATS_EVENT_H__

#include "Event.h"
#include "../../world/Slots/SlotPosition.h"
//#include "../../world/Slots/SlotManager.h"
#include "OgreVector3.h"

namespace AV{
  class ChunkData;
  enum STATS_EVENT_TYPE{
    STATS_NULL,
    STATS_SLOT_PLAYER_POS,
    STATS_OGRE_PLAYER_POS,
    STATS_ORIGIN_POS,
    STATS_LOADED_CHUNKS,
    STATS_MAP_CHUNKS
  };
  class StatsEvent : public Event{
  public:
    StatsEvent()
      : Event(EVENT_TYPE_STATS) { };

    STATS_EVENT_TYPE statsType = STATS_NULL;

    SlotPosition slotValue;
    Ogre::Vector3 position;

    std::vector<ChunkData> *chunkVector = 0;
  };
}

#endif
