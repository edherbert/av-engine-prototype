#include "Base.h"

#include <iostream>

#include <SDL.h>
#include "Window.h"

#include <Ogre.h>
#include <OgreHlmsPbs.h>
#include <OgreHlmsUnlit.h>
#include <Compositor/OgreCompositorManager2.h>

#include "OgreMap.h"
#include "world/World.h"
#include "gui/GuiManager.h"

#include "world/settings.h"
//#include "world/entity/Position.h"
//#include "world/entity/OgreMeshComponent.h"

#include "OgreOverlaySystem.h"
#include <OgreOverlayManager.h>

#include "EventManager/EventManager.h"

#include "scripting/ScriptManager.h"
#include "scripting/TerminalManager.h"
#include "scripting/Script.h"

#ifdef OGRE_STATIC_LIB
    #include <OgreMetalPlugin.h>
#endif

namespace AV{
	Base::Base(std::string bundlePath) :
		bundlePath(bundlePath){
		initialise();
	}

	Base::Base(){
		initialise();
	}

	void Base::initialise(){
		setupRoot();

    Ogre::v1::OverlaySystem *overlaySystem = OGRE_NEW Ogre::v1::OverlaySystem();

		window = new AV::Window();

		registerHLMS();

		root->addResourceLocation(bundlePath + "/models", "FileSystem");
    root->addResourceLocation(bundlePath + "/materials", "FileSystem");
    root->addResourceLocation(bundlePath + "/fonts", "FileSystem");
    root->addResourceLocation(bundlePath + "/maps/terrain", "FileSystem");

    root->addResourceLocation("/home/edward/Documents/ogre/build/lib/", "FileSystem");

    root->loadPlugin("Plugin_ParticleFX");

		Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups(false);

    world = new AV::World();
    guiManager = new GuiManager(world->getSceneManager(), overlaySystem);

    workspace = setupCompositor();

    eventManager = new EventManager();

    //world = new AV::World(CHUNK_SIZE_WORLD, TERRAIN_RESOLUTION);
    ScriptManager *scriptManager = new ScriptManager(world);
    TerminalManager *terminalManager = new TerminalManager(scriptManager);
    guiManager->setupTerminalManager(terminalManager);
    /*world->loadChunk("overworld", 0, 0);
    world->loadChunk("overworld", 1, 0);

    world->loadChunk("cave", 0, 0);

    world->setChunkVisible("overworld", 0, 0);
    world->setChunkVisible("cave", 0, 0);*/

    //entityx::Entity entity = world->createEntity();
    //entity.assign<Position>(Ogre::Vector3(0, 0, 0));

    //world->unloadChunk("main", 0, 0);

    // Script s;
    // s.compileFile("../resources/scripts/test.nut");
    // s.run();

    //Event e(EVENT_TYPE_INPUT);
    //EventManager::transmitEvent(e);

    //ogreMap = new AV::OgreMap(sceneManager, window, camera, world);

    /*for(int y = 0; y < 1; y++){
      for(int x = 0; x < 1; x++){
        world->setLoadLocation(x, y, 0, 0);
      }
    }*/

    // Ogre::ParticleSystem *particleSystem = world->getSceneManager()->createParticleSystem("Examples/Fireworks");
    // world->getSceneManager()->getRootSceneNode()->attachObject(particleSystem);
}

	Base::~Base(){
		delete window;
    delete ogreMap;
	}

	void Base::update(){
		window->update();
    //ogreMap->update();

    if(window->getKey(KEY_ESC)) window->close();
    //if(window->getKey(KEY_F)) world->setChunkVisible("cave", 0, 0);
    //if(window->getKey(KEY_G)) world->setChunkInvisible("cave", 0, 0);

    world->update();

		root->renderOneFrame();
	}

	void Base::setupRoot(){
    #ifndef __APPLE__
      //If not apple then use the plugins file.
      root = new Ogre::Root(bundlePath + "/setup/plugins.cfg");
    #endif

		#ifdef __APPLE__
        root = new Ogre::Root();
		    #ifdef OGRE_STATIC_LIB
		        Ogre::LogManager::getSingleton().logMessage("Installing metal as a static plugin");
		        root->installPlugin(new Ogre::MetalPlugin());
		    #else
		        Ogre::LogManager::getSingleton().logMessage("Installing metal as a dynamic plugin");
		        root->loadPlugin("RenderSystem_Metal");
		    #endif
		#endif

		root->setRenderSystem(root->getAvailableRenderers()[0]);

		//See if this returns a variable and then try to do something with that.
		//root->showConfigDialog();

		//root->addResourceLocation("../media", "FileSystem");
		root->getRenderSystem()->setConfigOption( "sRGB Gamma Conversion", "Yes" );
		root->initialise(false);
	}

	void Base::registerHLMS(){
		Ogre::ArchiveVec library;
		//library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Common/Metal", "FileSystem", true ));
		//library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Common/GLSL", "FileSystem", true ));
    library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Common/GLSL", "FileSystem", true ));
    library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Unlit/Any", "FileSystem", true ));


    //library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Pbs/Any", "FileSystem", true ));
    //library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Common/Any", "FileSystem", true ));
    library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Pbs/Any", "FileSystem", true ));
    library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Common/Any", "FileSystem", true ));

		Ogre::Archive *archivePbs;
    Ogre::Archive *archiveUnlit;

		Ogre::RenderSystem *renderSystem = Ogre::Root::getSingletonPtr()->getRenderSystem();
		if(renderSystem->getName() == "Metal Rendering Subsystem"){
		    archivePbs = Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Pbs/Metal", "FileSystem", true );
		}else{
		    archivePbs = Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Pbs/GLSL", "FileSystem", true );
        archiveUnlit = Ogre::ArchiveManager::getSingletonPtr()->load(bundlePath + "/Hlms/Unlit/GLSL", "FileSystem", true );
		}
		Ogre::HlmsPbs *hlmsPbs = OGRE_NEW Ogre::HlmsPbs( archivePbs, &library );
    Ogre::HlmsUnlit *hlmsUnlit = OGRE_NEW Ogre::HlmsUnlit( archiveUnlit, &library );

		root->getHlmsManager()->registerHlms(hlmsPbs);
    root->getHlmsManager()->registerHlms(hlmsUnlit);
	}

	Ogre::CompositorWorkspace* Base::setupCompositor(){
    Ogre::CompositorManager2 *compositorManager = root->getCompositorManager2();

    const Ogre::String workspaceName("test Workspace");
    if(!compositorManager->hasWorkspaceDefinition(workspaceName)){
        compositorManager->createBasicWorkspaceDef(workspaceName, Ogre::ColourValue(0, 0, 0, 1), Ogre::IdString());
    }

    return compositorManager->addWorkspace(world->getSceneManager(), window->getRenderWindow(), world->getCamera(), workspaceName, true);
	}

	Ogre::RenderWindow* Base::getRenderWindow(){
		return window->getRenderWindow();
	}

	bool Base::isRunning(){
    return window->isOpen();
	}
}
