#ifndef __BASE_H__
#define __BASE_H__

#include <OgreRoot.h>
#include <OgreCamera.h>
#include <OgreSceneManager.h>

namespace AV{
	class OgreMap;
	class Window;
	class World;
	class EventManager;
	class GuiManager;
	class ActionManager;

	class Base{
	public:
		Base();
		Base(std::string bundlePath);
		~Base();

		void update();

		Window *window;
		EventManager *eventManager;

		AV::OgreMap *ogreMap;
		AV::World *world;
		GuiManager *guiManager;
		ActionManager *actionManager;

		void setupRoot();
		void registerHLMS();

		Ogre::Root *root;

		std::string bundlePath = "";

		Ogre::RenderWindow* getRenderWindow();

		//Ogre::SceneNode *node;

		bool isRunning();

	private:
		void initialise();

		Ogre::CompositorWorkspace* setupCompositor();
		Ogre::CompositorWorkspace *workspace;
	};
}
#endif
