#include <iostream>

#include "Window.h"
#include <OgreRoot.h>

#include "EventManager/EventManager.h"
#include "EventManager/Events/InputEvent.h"
#include "EventManager/Events/TypedEvent.h"

#include "Input.h"

#include <SDL_syswm.h>
#include <SDL.h>

namespace AV{
	Window::Window(){
		EventManager::subscribeToEvent(this, EVENT_TYPE_TYPED);


	#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
		SDL_Surface *surface = 0;
	#endif

		for(int i = 0; i < 300; i++){
		    keys[i] = false;
		}

	#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
		if(SDL_Init(SDL_INIT_VIDEO) < 0){
		    std::cout << "The SDL window couldn't be created" << std::endl;
		}
	#endif

	SDL_version compiled;
SDL_version linked;

SDL_VERSION(&compiled);
SDL_GetVersion(&linked);
printf("We compiled against SDL version %d.%d.%d ...\n",
       compiled.major, compiled.minor, compiled.patch);
printf("But we are linking against SDL version %d.%d.%d.\n",
       linked.major, linked.minor, linked.patch);



		bool openGL = true;

		Ogre::RenderSystem *renderSystem = Ogre::Root::getSingletonPtr()->getRenderSystem();
		if(renderSystem->getName() == "Metal Rendering Subsystem"){
		    openGL = false;
		}

		#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
		Uint32 flags = 0;
		flags = SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE;

		if(openGL){
		    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
		    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		    flags = flags | SDL_WINDOW_OPENGL;
		}

		window = SDL_CreateWindow("Window", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, flags);

		/*SDL_Renderer *render = SDL_CreateRenderer(window, -1, 0);

		SDL_SetRenderDrawColor(render, 255, 0, 0, 0);

		SDL_RenderClear(render);
		SDL_RenderPresent(render);*/

		SDL_SysWMinfo info;
		SDL_VERSION(&info.version);

		std::string strings[INPUT_COUNT] = {"", "W", "S", "A", "D", "Up", "Down", "Left", "Right", "Space", "Enter", "G"};
		Input::setInputStrings(strings);


		SDL_GLContext context;
		if(openGL) context = SDL_GL_CreateContext(window);

		if(SDL_GetWindowWMInfo(window, &info)){
	#endif
		    if(openGL){
		        SDL_GL_MakeCurrent(window, context);
		    }

		    Ogre::NameValuePairList params;
				params["gamma"] = true;
		#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE && OGRE_PLATFORM != OGRE_PLATFORM_IOS
		    params["macAPI"] = "cocoa";
		    if(!openGL){
		        params["parentWindowHandle"] = Ogre::StringConverter::toString((unsigned long)info.info.cocoa.window.contentView);
		        params["macAPICocoaUseNSView"] = true;
		    }else{
		        params["parentWindowHandle"] = Ogre::StringConverter::toString((unsigned long)info.info.cocoa.window.contentView);
		    }
		#endif

		#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
			if(openGL){
				params["currentGLContext"] = "true";

				// SDL_SysWMinfo wmInfo;
				// SDL_VERSION(&wmInfo.version);
				// SDL_GetWindowWMInfo(window, &wmInfo);
				// Ogre::String winHandle = Ogre::StringConverter::toString( (uintptr_t)wmInfo.info.x11.window );
				//
				// params.insert( std::make_pair("parentWindowHandle",  winHandle) );
			}
		#endif

		    renderWindow = Ogre::Root::getSingleton().createRenderWindow("Ogre Window", 500, 400, false, &params);
		    renderWindow->setVisible(true);

		    open = true;
	#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
		}
	#endif
	//SDL_StartTextInput();
	SDL_StopTextInput();
	}

	void Window::update(){
	#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
		SDL_PumpEvents();

		SDL_Event event;

		while(SDL_PollEvent(&event)){
		    switch(event.type){
		        case SDL_QUIT:
		            close();
		            break;
						case SDL_TEXTINPUT:{
							TypedEvent typedEvent;
							typedEvent.type = TYPED_EVENT_CHAR;
							typedEvent.text = event.text.text;
							EventManager::transmitTypedEvent(typedEvent);

							break;
						}
						case SDL_TEXTEDITING:{
							if(event.key.keysym.sym == SDLK_BACKSPACE){
							}
							break;
						}
		        case SDL_KEYDOWN:
								if(event.key.keysym.sym == SDLK_BACKSPACE){
									TypedEvent typedEvent;
									typedEvent.type = TYPED_EVENT_COMMAND;
									typedEvent.command = TYPED_EVENT_BACKSPACE;
									EventManager::transmitTypedEvent(typedEvent);
								}
								if(event.key.keysym.sym == SDLK_RETURN){
									TypedEvent typedEvent;
									typedEvent.type = TYPED_EVENT_COMMAND;
									typedEvent.command = TYPED_EVENT_ENTER;
									EventManager::transmitTypedEvent(typedEvent);
								}
		            keyPressed(event);
		            break;
		        case SDL_KEYUP:
		            keyReleased(event);
		            break;
		        case SDL_MOUSEMOTION:
		            updateMouse(event);
		            break;
		        case SDL_WINDOWEVENT:
		            switch(event.window.event){
		                case SDL_WINDOWEVENT_RESIZED:
		                    resizeWindow(event);
		                    break;
		                case SDL_WINDOWEVENT_LEAVE:
		                    if(mouseGrab) warpToCentre();
		                    break;
		            }
		    }
		}

		SDL_UpdateWindowSurface(window);
		SDL_GL_SwapWindow(window);
	#endif
	}

	void Window::setMouseGrab(bool grab){
	#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
		mouseGrab = grab;
		SDL_ShowCursor(!grab);
	#endif
	}

	void Window::close(){
		open = false;

		renderWindow->destroy();
		#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
		SDL_DestroyWindow(window);
		SDL_Quit();
		#endif
	}

	void Window::keyPressed(SDL_Event &event){
		keys[event.key.keysym.scancode] = true;

		INPUT_TYPE t = I_NULL;
		switch(event.key.keysym.scancode){
			case KEY_D:
				t = I_RIGHT;
				break;
			case KEY_W:
				t = I_UP;
				break;
			case KEY_S:
				t = I_DOWN;
				break;
			case KEY_A:
				t = I_LEFT;
				break;
			case KEY_RETURN:
				t = I_ACCEPT;
				break;
			case KEY_SPACE:
				t = I_JUMP;
				break;
			case KEY_G:
				t = I_DECLINE;
				break;
			case KEY_F1:
				t = I_DEBUG_TERMINAL;
				break;
			case KEY_F2:
				t = I_DEBUG_PLAYER_CONTROLLER;
				break;
			case 82:
				t = I_AIM_UP;
				break;
			case 81:
				t = I_AIM_DOWN;
				break;
			case KEY_LEFT:
				t = I_AIM_LEFT;
				break;
			case KEY_RIGHT:
				t = I_AIM_RIGHT;
				break;
		}
		if(t != I_NULL){
			Input::setInput(t, true);
			if(event.key.repeat == 0){
				InputEvent e(KEY_DOWN, t);
				EventManager::transmitInputEvent(e);
			}
		}

	}

	void Window::keyReleased(SDL_Event &event){
		keys[event.key.keysym.scancode] = false;

		INPUT_TYPE t = I_NULL;
		switch(event.key.keysym.scancode){
			case KEY_D:
				t = I_RIGHT;
				break;
			case KEY_W:
				t = I_UP;
				break;
			case KEY_S:
				t = I_DOWN;
				break;
			case KEY_A:
				t = I_LEFT;
				break;
			case KEY_RETURN:
				t = I_ACCEPT;
				break;
			case KEY_SPACE:
				t = I_JUMP;
				break;
			case KEY_G:
				t = I_DECLINE;
				break;
			case KEY_F1:
				t = I_DEBUG_TERMINAL;
				break;
			case KEY_F2:
				t = I_DEBUG_PLAYER_CONTROLLER;
				break;
			case 82:
				t = I_AIM_UP;
				break;
			case 81:
				t = I_AIM_DOWN;
				break;
			case KEY_LEFT:
				t = I_AIM_LEFT;
				break;
			case KEY_RIGHT:
				t = I_AIM_RIGHT;
				break;
		}
		if(t != I_NULL){
			Input::setInput(t, false);
			if(event.key.repeat == 0){
				InputEvent e(KEY_UP, t);
				EventManager::transmitInputEvent(e);
			}
		}

	}

	bool Window::getKey(int k){
		return keys[k];
	}

	bool Window::isOpen(){
		return open;
	}

	void Window::resizeWindow(SDL_Event &event){
		width = event.window.data1;
		height = event.window.data2;

		renderWindow->resize(width, height);
	}

	void Window::updateMouse(SDL_Event &event){
		mouseX = event.motion.x;
		mouseY = event.motion.y;

		xOffset = -(prevX - mouseX);
		yOffset = prevY - mouseY;

		prevX = mouseX;
		prevY = mouseY;

		if(mouseGrab){
		    if(mouseX <= 0 ||
		       mouseY <= 0 ||
		       mouseY >= width ||
		       mouseX >= height) warpToCentre();
		}
	}

	Ogre::RenderWindow* Window::getRenderWindow(){
		return renderWindow;
	}

	void Window::warpToCentre(){
		mouseX = width / 2;
		mouseY = height / 2;

		prevX = mouseX;
		prevY = mouseY;
		offsetX = 0;
		offsetY = 0;

		SDL_WarpMouseInWindow(window, mouseX, mouseY);
	}

	void Window::notifyTypedEvent(TypedEvent &e){
		if(e.type == TYPED_EVENT_START){
			SDL_StartTextInput();
		}
		if(e.type == TYPED_EVENT_END){
			SDL_StopTextInput();
		}
	}
}
