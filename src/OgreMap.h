#ifndef __OGREOgreMap_H__
#define __OGREOgreMap_H__

#include "Ogre.h"

namespace AV{
  class Window;
  class World;

  class OgreMap{
  public:
    OgreMap(Ogre::SceneManager *sceneManager, Window *window, Ogre::Camera *camera, AV::World *world);
    ~OgreMap();

    void update();

  private:
    Ogre::SceneManager *sceneManager;
    Ogre::Camera *camera;
    Window *window;
    AV::World *world;

    Ogre::Real yaw;
    Ogre::Real pitch;

    const float cameraSpeed = 1.0f;

    void pointCamera(int xOffset, int yOffset);
    float radians(float value);
  };
}
#endif
