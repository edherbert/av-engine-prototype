#include "Input.h"
namespace AV{
  bool Input::activeInputs[INPUT_COUNT] = {};
  std::string Input::inputStrings[INPUT_COUNT] = {};

  Input::Input(){

  }

  Input::~Input(){

  }

  void Input::setInput(INPUT_TYPE type, bool value){
    activeInputs[type] = value;
  }

  bool Input::getInput(INPUT_TYPE type){
    return activeInputs[(int)type];
  }

  void Input::setInputStrings(std::string (&is)[INPUT_COUNT]){
    for(int i = 0; i < INPUT_COUNT; i++){
      Input::inputStrings[i] = is[i];
    }
  }

  const std::string& Input::getInputString(INPUT_TYPE i){
    return Input::inputStrings[i];
  }
}
