#include "WorldScripts.h"

#include <iostream>

namespace AV{
WorldScripts::WorldScripts(){

}

WorldScripts::~WorldScripts(){

}

SQInteger WorldScripts::loadChunk(HSQUIRRELVM v){
  std::cout << "Loading a chunk" << '\n';
}

void WorldScripts::setupWrapper(HSQUIRRELVM v){
  addFunction(v, loadChunk, "loadChunk");
}
}
