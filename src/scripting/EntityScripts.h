#ifndef __ENTITY_SCRIPTS_H__
#define __ENTITY_SCRIPTS_H__

#include "ScriptObject.h"

namespace AV{
class EntityManager;
class ScriptManager;

class EntityScripts : public ScriptObject{
friend ScriptManager;
public:
  EntityScripts(EntityManager *entityManager);
  ~EntityScripts();

private:

  static SQInteger createEntity(HSQUIRRELVM v);
  static SQInteger destroyEntity(HSQUIRRELVM v);
  static SQInteger moveEntity(HSQUIRRELVM v);
  static SQInteger setPlayer(HSQUIRRELVM v);
  static SQInteger setController(HSQUIRRELVM v);

  void setupWrapper(HSQUIRRELVM v);

  static EntityManager *entityManager;
};
}
#endif
