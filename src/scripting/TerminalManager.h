#pragma once

#include <squirrel.h>

namespace AV{
  class ScriptManager;
  class TerminalGui;

  class TerminalManager{
  friend TerminalGui;
  public:
    TerminalManager(ScriptManager *scriptManager);
    ~TerminalManager();

    void runCommand(const char* cmd);

    static void printToGui(HSQUIRRELVM v, const SQChar *s, ...);

  private:
    HSQUIRRELVM sqvm;

    static SQInteger clearTerminal(HSQUIRRELVM v);

    static TerminalGui *terminalGui;
  };
}
