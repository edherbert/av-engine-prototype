#include "Script.h"

#include "ScriptManager.h"

#include <iostream>
#include <cstring>

namespace AV{
Script::Script(HSQUIRRELVM vm)
  : vm(vm){
    sq_resetobject(&obj);
}

Script::Script(){
  sq_resetobject(&obj);
  vm = ScriptManager::sqvm;
}

Script::~Script(){
  release();
}

void Script::compileFile(const SQChar *path){
  //release();
  sq_release(vm, &obj);
  sq_resetobject(&obj);

  //This might not work if it's pushing the closure into the stack.
  //If lots of things share the stack this could lead to issues.
  if(SQ_FAILED(sqstd_loadfile(vm, path, true))){
    std::cout << "loading file failed" << '\n';
    return;
  }

  sq_getstackobj(vm,-1,&obj);
  sq_addref(vm, &obj);
  sq_pop(vm, 1);


  available = true;
}

void Script::run(){
  if(!available) return;

  SQInteger top = sq_gettop(vm);
  sq_pushobject(vm, obj);
  sq_pushroottable(vm);
  sq_call(vm, 1, false, true);

  sq_settop(vm, top);
}

void Script::runFunction(const SQChar *entry){
  if(!available) return;

  //The object that will contain the function (I think).
   //Object ret(vm);
   int top = sq_gettop(vm);

   sq_pushroottable(vm);

    HSQOBJECT ret;
    sq_resetobject(&ret);

   sq_pushobject(vm, obj);
   sq_pushstring(vm, entry, strlen(entry));

   if (SQ_FAILED(sq_get(vm, -3))) {
     //If a function with that name cannot be found then restore the stack to the beginning one and return.
     std::cout << "Failed to get the function" << '\n';
     //sq_pop(vm, 1);
     sq_settop(vm, top);
     return;
   }

   //Gets an object from the stack and stores it in the ret variable.
   //This object is the one returned from sq_get
   sq_getstackobj(vm, -1, &ret);
   //Add a reference to the new value at the top of the stack.
   //I don't really know what this does but you have to do it.
   sq_addref(vm, &ret);
   sq_pop(vm, 2);


   //callfunc
   //Push both that function object (ret) and it's scope container (obj)
   sq_pushobject(vm, ret);
   sq_pushobject(vm, obj);


   //call and return
   if(SQ_FAILED(sq_call(vm, 1, false, false))){
     std::cout << "Error in the function call." << '\n';
   }

   //sq_pop(vm, 1);
   sq_settop(vm, top);
}

void Script::release(){
  if(!sq_isnull(obj)) {
    sq_resetobject(&obj);
    sq_release(vm, &obj);
  }
  available = false;
}
}
