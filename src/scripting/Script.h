#ifndef __SCRIPT_H__
#define __SCRIPT_H__

#include <squirrel.h>
#include <sqstdio.h>
#include <sqstdaux.h>

namespace AV{
class ScriptManager;

class Script{
public:
  Script(HSQUIRRELVM vm);
  Script();
  ~Script();

  void compileFile(const SQChar *path);
  void run();
  void runFunction(const SQChar *entry);
  void release();

private:
  HSQUIRRELVM vm;
  HSQOBJECT obj;

  bool available = false;
};
}
#endif
