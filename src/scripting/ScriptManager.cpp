#include "ScriptManager.h"

#include <stdarg.h>
#include <stdio.h>

#include "components/MovementComponentScripts.h"
#include "components/HealthComponentScripts.h"
#include "components/OgreMeshComponentScripts.h"
#include "components/PlayerInputComponentScripts.h"
#include "components/PlayerDialogComponentScripts.h"
#include "components/RigidBodyComponentScripts.h"
#include "components/ActionTriggerComponentScripts.h"

#include "WorldScripts.h"
#include "EntityScripts.h"
#include "CameraScripts.h"
#include "DialogScripts.h"
#include "PlayerScripts.h"
#include "GameScripts.h"

#include "../world/World.h"

#include "Script.h"

#include <iostream>

#ifdef SQUNICODE
#define scvprintf vwprintf
#else
#define scvprintf vprintf
#endif

namespace AV{
void printfunc(HSQUIRRELVM v, const SQChar *s, ...){
  va_list arglist;
  va_start(arglist, s);
  scvprintf(s, arglist);
  va_end(arglist);
  std::cout << '\n';
}

HSQUIRRELVM ScriptManager::sqvm = sq_open(1024);

//ScriptManager::ScriptManager(EntityManager *entityManager){
ScriptManager::ScriptManager(World *world){
  //sqvm = sq_open(1024);


  ComponentScripts::entityManager = world->getEntityManager();

  worldScripts = new WorldScripts();
  entityScripts = new EntityScripts(world->getEntityManager());
  cameraScripts = new CameraScripts(world->getCamera());
  dialogScripts = new DialogScripts();
  playerScripts = new PlayerScripts(world);
  gameScripts = new GameScripts();

  movementComponentScripts = new MovementComponentScripts();
  healthComponentScripts = new HealthComponentScripts();

  //Setup the vm with the namespaces.
  setupVM(sqvm);

  //sq_pushroottable(sqvm);

  //sqstd_dofile(sqvm, _SC("../scripts/test.nut"), 0, 1);
  //runScript("../scripts/test.nut", "thing");
  //runScript("../scripts/another.nut");
}

ScriptManager::~ScriptManager(){
  sq_close(sqvm);

  delete worldScripts;
  delete entityScripts;
  delete cameraScripts;
  delete gameScripts;
}

void ScriptManager::setupVM(HSQUIRRELVM vm){
  sq_setprintfunc(vm, printfunc, NULL);

  sq_pushroottable(vm);

  createComponentNamespace(vm);
  createWorldNamespace(vm);
  createEntityNamespace(vm);
  createCameraNamespace(vm);
  createDialogNamespace(vm);
  createPlayerNamespace(vm);
  createGameNamespace(vm);

  sq_pop(vm,1);
}

void ScriptManager::update(Script *s){
  s->runFunction("test");
}

void ScriptManager::createComponentNamespace(HSQUIRRELVM vm){
  //The name
  sq_pushstring(vm, _SC("component"), -1);
  //The actual table to be tied to the name.
  sq_newtable(vm);
  //Add the different components.
  movementComponentScripts->setupWrapper(vm);
  healthComponentScripts->setupWrapper(vm);

  OgreMeshComponentScripts o;
  o.setupWrapper(vm);

  PlayerInputComponentScripts playerInput;
  playerInput.setupWrapper(vm);

  PlayerDialogComponentScripts playerDialog;
  playerDialog.setupWrapper(vm);

  RigidBodyComponentScripts rigidBody;
  rigidBody.setupWrapper(vm);

  ActionTriggerComponentScripts actionTrigger;
  actionTrigger.setupWrapper(vm);

  sq_newslot(vm, -3, false);
  //Push the component to the root table.
  //sq_newslot(sqvm, -3 , false);

  //sq_pop(sqvm,1); //pops the root table
}

void ScriptManager::createWorldNamespace(HSQUIRRELVM vm){
  sq_pushstring(vm, _SC("world"), -1);
  sq_newtable(vm);

  worldScripts->setupWrapper(vm);
  //World::setupWorldScripts();

  sq_newslot(vm, -3 , false);
}

void ScriptManager::createEntityNamespace(HSQUIRRELVM vm){
  sq_pushstring(vm, _SC("entity"), -1);
  sq_newtable(vm);

  entityScripts->setupWrapper(vm);
  //World::setupEntityScripts();


  sq_newslot(vm, -3 , false);
}

void ScriptManager::createCameraNamespace(HSQUIRRELVM vm){
  sq_pushstring(vm, _SC("camera"), -1);
  sq_newtable(vm);

  cameraScripts->setupWrapper(vm);

  sq_newslot(vm, -3 , false);
}

void ScriptManager::createDialogNamespace(HSQUIRRELVM vm){
  sq_pushstring(vm, _SC("dialog"), -1);
  sq_newtable(vm);

  dialogScripts->setupWrapper(vm);

  sq_newslot(vm, -3 , false);
}

void ScriptManager::createPlayerNamespace(HSQUIRRELVM vm){
  sq_pushstring(vm, _SC("player"), -1);
  sq_newtable(vm);

  playerScripts->setupWrapper(vm);

  sq_newslot(vm, -3 , false);
}

void ScriptManager::createGameNamespace(HSQUIRRELVM vm){
  sq_pushstring(vm, _SC("game"), -1);
  sq_newtable(vm);

  gameScripts->setupWrapper(vm);

  sq_newslot(vm, -3 , false);
}

void ScriptManager::runScript(const char *scriptPath, const char *fname){
  std::cout << "Running script." << '\n';
  std::cout << scriptPath << '\n';
  sq_pushroottable(sqvm);
  if(SQ_SUCCEEDED(sqstd_dofile(sqvm, _SC(scriptPath), 0, 1))){
  //if(sqstd_loadfile(sqvm, scriptPath, true)){

    if(fname != ""){
      callFunction(sqvm, fname);
    }
  }else{
    std::cout << "There was a problem loading that script file." << '\n';
  }
}

void ScriptManager::callFunction(HSQUIRRELVM sq, const SQChar *s){
  int top = sq_gettop(sq);
  //sq_pushroottable(sq);

  sq_pushstring(sq,_SC(s),-1);

  if(SQ_SUCCEEDED(sq_get(sq,-2))) {
    sq_pushroottable(sq);

    sq_call(sq, 1, 0, 0);
  }

  sq_settop(sq, top);
}

void ScriptManager::debugStack(HSQUIRRELVM sq){
    int top = sq_gettop(sq);
    if(top <= 0){
      std::cout << "Nothing in the stack!" << '\n';
      return;
    }
    //This push root table sometimes causes problems.
    //sq_pushroottable(sq);
    while(top >= 0) {
        SQObjectType objectType = sq_gettype(sq, top);
        //Type type = Type(objectType);
        std::cout << "stack index: " << top << " type: " << typeToStr(objectType) << std::endl;
        top--;
    }
}
}
