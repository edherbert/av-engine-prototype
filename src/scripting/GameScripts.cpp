#include "GameScripts.h"

#include "OgreVector3.h"
#include <iostream>

#include "../serialisation/SaveManager.h"

namespace AV{
  GameScripts::GameScripts(){

  }

  GameScripts::~GameScripts(){

  }

  SQInteger GameScripts::saveGame(HSQUIRRELVM v){
    SaveManager::saveGame();
  }

  void GameScripts::setupWrapper(HSQUIRRELVM v){
    addFunction(v, saveGame, "save");
  }
}
