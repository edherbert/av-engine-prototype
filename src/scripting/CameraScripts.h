#ifndef __CAMERA_SCRIPTS_H__
#define __CAMERA_SCRIPTS_H__

#include "ScriptObject.h"

namespace Ogre{
  class Camera;
}

namespace AV{
  class ScriptManager;
  class CameraScripts : public ScriptObject{
    friend ScriptManager;
  public:
    CameraScripts(Ogre::Camera *camera);
    ~CameraScripts();

    static SQInteger moveCamera(HSQUIRRELVM v);
    static SQInteger lookAt(HSQUIRRELVM v);

    void setupWrapper(HSQUIRRELVM v);

  private:
    static Ogre::Camera *camera;
  };
}

#endif
