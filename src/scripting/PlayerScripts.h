#pragma once

#include "ScriptObject.h"

namespace AV{
  class World;

  class PlayerScripts : public ScriptObject{
  public:
    PlayerScripts(World *world);
    ~PlayerScripts();

    void setupWrapper(HSQUIRRELVM v);
  private:
    static World *world;

    static SQInteger teleportPlayer(HSQUIRRELVM v);
  };
};
