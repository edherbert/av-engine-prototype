#include "DialogScripts.h"

#include <iostream>

#include "../EventManager/EventManager.h"
#include "../EventManager/Events/DialogEvent.h"

namespace AV{
  DialogScripts::DialogScripts(){

  }

  DialogScripts::~DialogScripts(){

  }

  SQInteger DialogScripts::loadAndStartDialog(HSQUIRRELVM v){
    SQInteger startingBlock = 0;
    sq_getinteger(v, -1, &startingBlock);

    const SQChar *dialogPath;
    sq_getstring(v, -2, &dialogPath);

    sq_pop(v, 2);


    std::cout << dialogPath << '\n';
    DialogEvent d;
    d.dialogType = DIALOG_START;
    //d.dialogPath = "../resources/dialog/converse.xml";
    d.dialogPath = dialogPath;
    d.startingBlock = startingBlock;
    EventManager::transmitDialogEvent(d);

    return 0;
  }

  void DialogScripts::setupWrapper(HSQUIRRELVM v){
    addFunction(v, loadAndStartDialog, "loadAndStart");
  }
}
