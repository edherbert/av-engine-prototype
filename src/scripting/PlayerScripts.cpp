#include "PlayerScripts.h"

#include <iostream>
#include "../world/World.h"

#include "OgreVector3.h"

namespace AV{
  World* PlayerScripts::world = 0;

  PlayerScripts::PlayerScripts(World *world){
    PlayerScripts::world = world;
  }

  PlayerScripts::~PlayerScripts(){

  }

  SQInteger PlayerScripts::teleportPlayer(HSQUIRRELVM v){
    SQInteger chunkX;
    SQInteger chunkY;
    SQInteger posX;
    SQInteger posY;
    SQInteger posZ;
    const SQChar *mapName;

    sq_getstring(v, -1, &mapName);
    sq_getinteger(v, -2, &posZ);
    sq_getinteger(v, -3, &posY);
    sq_getinteger(v, -4, &posX);
    sq_getinteger(v, -5, &chunkY);
    sq_getinteger(v, -6, &chunkX);

    sq_pop(v, 6);

    world->teleportPlayer(chunkX, chunkY, Ogre::Vector3(posX, posY, posZ), Ogre::String(mapName));
  }

  void PlayerScripts::setupWrapper(HSQUIRRELVM v){
    addFunction(v, teleportPlayer, "tele");
  }
}
