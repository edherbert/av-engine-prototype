#include "TerminalManager.h"

#include <stdarg.h>
#include <stdio.h>

#include <sqstdio.h>
#include <sqstdaux.h>

#include <cstring>
#include <iostream>

#include "ScriptManager.h"
#include "../gui/TerminalGui.h"

#ifdef SQUNICODE
#define scvprintf vwprintf
#else
#define scvprintf vprintf
#endif

namespace AV{
  TerminalGui* TerminalManager::terminalGui = 0;

  TerminalManager::TerminalManager(ScriptManager *scriptManager){
    sqvm = sq_open(1024);
    scriptManager->setupVM(sqvm);

    sq_setprintfunc(sqvm, printToGui, NULL);

    sq_pushroottable(sqvm);
    sq_pushstring(sqvm, _SC("clear"), -1);
    sq_newclosure(sqvm,clearTerminal,0);
    sq_newslot(sqvm,-3,SQFalse);

    sq_pop(sqvm, 1);

    // run_script("print(\"Hello from the termianl manager\")");
    // run_script("x <- 10;");
    // run_script("print(x);");
  }

  TerminalManager::~TerminalManager(){
    sq_close(sqvm);
  }

  SQInteger TerminalManager::clearTerminal(HSQUIRRELVM v){
    terminalGui->clearTerminal();
  }

  void TerminalManager::printToGui(HSQUIRRELVM v, const SQChar *s, ...){
    va_list vl;
    va_start(vl, s);
    char buffer[2048];
    std::vsnprintf(buffer, 2048, s, vl);

    terminalGui->addStringToTerminal(buffer);
  }

  void TerminalManager::runCommand(const char* cmd){
    sq_compilebuffer(sqvm,cmd,(int)strlen(cmd)*sizeof(SQChar),"compile",1);
    sq_pushroottable(sqvm);
    if(SQ_FAILED(sq_call(sqvm,1,1,0))){
      terminalGui->addStringToTerminal("Error");
    }
  }
}
