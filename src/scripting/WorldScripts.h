#ifndef __WORLD_SCRIPTS_H__
#define __WORLD_SCRIPTS_H__

#include "ScriptObject.h"

namespace AV{
class ScriptManager;

class WorldScripts : public ScriptObject{
friend ScriptManager;
public:
  WorldScripts();
  ~WorldScripts();

private:

  static SQInteger loadChunk(HSQUIRRELVM v);

  void setupWrapper(HSQUIRRELVM v);
};
}
#endif
