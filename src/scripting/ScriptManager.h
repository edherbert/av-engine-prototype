#ifndef __SCRIPT_MANAGER_H__
#define __SCRIPT_MANAGER_H__

#include <squirrel.h>
#include <sqstdio.h>
#include <sqstdaux.h>

namespace AV{
class MovementComponentScripts;
class HealthComponentScripts;

class World;
class WorldScripts;
class EntityScripts;
class CameraScripts;
class DialogScripts;
class PlayerScripts;
class GameScripts;
class EntityManager;
class Script;

class ScriptManager{
  friend Script;
public:
  ScriptManager(World *world);
  ~ScriptManager();

  void update(Script *s);

  static void runScript(const char *scriptPath, const char *fname = "");

  static void debugStack(HSQUIRRELVM sq);

  void setupVM(HSQUIRRELVM vm);

  static const char* typeToStr(SQObjectType type) {
      switch (type) {
      case OT_INTEGER: return "INTEGER";
      case OT_FLOAT: return "FLOAT";
      case OT_BOOL: return "BOOL";
      case OT_STRING: return "STRING";
      case OT_TABLE: return "TABLE";
      case OT_ARRAY: return "ARRAY";
      case OT_USERDATA: return "USERDATA";
      case OT_CLOSURE: return "CLOSURE";
      case OT_NATIVECLOSURE: return "NATIVECLOSURE";
      case OT_GENERATOR: return "GENERATOR";
      case OT_USERPOINTER: return "USERPOINTER";
      case OT_THREAD: return "THREAD";
      case OT_FUNCPROTO: return "FUNCPROTO";
      case OT_CLASS: return "CLASS";
      case OT_INSTANCE: return "INSTANCE";
      case OT_WEAKREF: return "WEAKREF";
      case OT_OUTER: return "OUTER";
      default: return "UNKNOWN";
      }
  }

private:
  static HSQUIRRELVM sqvm;

  void createComponentNamespace(HSQUIRRELVM vm);
  void createWorldNamespace(HSQUIRRELVM vm);
  void createEntityNamespace(HSQUIRRELVM vm);
  void createCameraNamespace(HSQUIRRELVM vm);
  void createDialogNamespace(HSQUIRRELVM vm);
  void createPlayerNamespace(HSQUIRRELVM vm);
  void createGameNamespace(HSQUIRRELVM vm);

  static void callFunction(HSQUIRRELVM sq, const SQChar *s = "");

  MovementComponentScripts *movementComponentScripts;
  HealthComponentScripts *healthComponentScripts;

  WorldScripts *worldScripts;
  EntityScripts *entityScripts;
  CameraScripts *cameraScripts;
  DialogScripts *dialogScripts;
  PlayerScripts *playerScripts;
  GameScripts *gameScripts;
};
}
#endif
