#ifndef __SCRIPT_OBJECT_H__
#define __SCRIPT_OBJECT_H__

#include <squirrel.h>

class ScriptObject{
public:
  ScriptObject();
  ~ScriptObject();

  void addFunction(HSQUIRRELVM v, SQFUNCTION f, const char *fname);
};

#endif
