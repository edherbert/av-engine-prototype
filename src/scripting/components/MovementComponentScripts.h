#ifndef __MOVEMENT_COMPONENT_SCRIPTS_H__
#define __MOVEMENT_COMPONENT_SCRIPTS_H__

#include "ComponentScripts.h"

namespace AV{
class MovementComponentScripts : public ComponentScripts{
public:
  MovementComponentScripts();
  ~MovementComponentScripts();

  void setupWrapper(HSQUIRRELVM v);

private:
  static SQInteger add(HSQUIRRELVM v);
  static SQInteger remove(HSQUIRRELVM v);

  static SQInteger componentMovementSetSpeed(HSQUIRRELVM v);
  static SQInteger componentMovementGetSpeed(HSQUIRRELVM v);

};
}
#endif
