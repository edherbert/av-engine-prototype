#ifndef __OGRE_MESH_COMPONENT_SCRIPTS_H__
#define __OGRE_MESH_COMPONENT_SCRIPTS_H__

#include "Ogre.h";

#include "ComponentScripts.h"
#include "../../world/Entity/components/OgreMeshComponent.h"
#include "../../world/Entity/EntityMeshManager.h"


namespace AV{
class OgreMeshComponentScripts : public ComponentScripts{
public:
  OgreMeshComponentScripts() {}
  ~OgreMeshComponentScripts() {}

  void setupWrapper(HSQUIRRELVM v){
    sq_pushstring(v, _SC("ogreMesh"), -1);
    sq_newtable(v);

    addFunction(v, add, "add");
    addFunction(v, remove, "remove");

    sq_newslot(v, -3, false);
  }

private:
  static SQInteger remove(HSQUIRRELVM v){
    _removeComponent<OgreMeshComponent>(v);
    return 1;
  }


  static SQInteger add(HSQUIRRELVM v){
    const SQChar *meshPath;
    sq_getstring(v, -1, &meshPath);
    sq_poptop(v);

    _addComponent<OgreMeshComponent>(v, EntityMeshManager::createOgreMesh(meshPath));
    return 1;
  }

};
}

#endif
