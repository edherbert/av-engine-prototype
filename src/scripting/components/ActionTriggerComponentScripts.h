#pragma once

#include "ComponentScripts.h"

#include "../../world/Entity/components/ActionTriggerComponent.h"
#include "../../world/Physics/BulletManager.h"
#include "../../world/Entity/EntityManager.h"

namespace AV{
  class ActionTriggerComponentScripts : public ComponentScripts{
  public:
    ActionTriggerComponentScripts() {};
    ~ActionTriggerComponentScripts() {};

    void setupWrapper(HSQUIRRELVM v){
      sq_pushstring(v, _SC("actionTrigger"), -1);
      sq_newtable(v);

      addFunction(v, add, "add");
      addFunction(v, remove, "remove");
      addFunction(v, setActionDescription, "setActionDescription");
      addFunction(v, setActionResource, "setActionResource");
      addFunction(v, setActionType, "setActionType");
      addFunction(v, setActionInputTrigger, "setActionInputTrigger");

      sq_newslot(v, -3, false);
    }
  private:
    static SQInteger remove(HSQUIRRELVM v){
      _removeComponent<ActionTriggerComponent>(v);
      return 1;
    }

    static SQInteger setActionDescription(HSQUIRRELVM v){
      SQInteger id;
      sq_getinteger(v, -1, &id);
      sq_poptop(v);

      const SQChar *string;
      sq_getstring(v, -1, &string);
      sq_poptop(v);

      entityx::Entity e = getEntityHandle(v);

      if(e.valid()){
        entityx::ComponentHandle<ActionTriggerComponent> comp = e.component<ActionTriggerComponent>();

        ActionData* d = getActionData(comp.get(), id);

        d->actionDescription = std::string(string);
      }
    }

    static SQInteger setActionType(HSQUIRRELVM v){
      SQInteger id;
      sq_getinteger(v, -1, &id);
      sq_poptop(v);

      SQInteger num;
      sq_getinteger(v, -1, &num);
      sq_poptop(v);

      entityx::Entity e = getEntityHandle(v);

      if(e.valid()){
        entityx::ComponentHandle<ActionTriggerComponent> comp = e.component<ActionTriggerComponent>();
        //comp.get()->data.actionDescription = std::string(string);
        ActionData* d = getActionData(comp.get(), id);

        if(num == 0) d->actionType = ACTION_TRIGGER_TYPE_SCRIPT;
        if(num == 1) d->actionType = ACTION_TRIGGER_TYPE_DIALOG;
      }
    }

    static SQInteger setActionInputTrigger(HSQUIRRELVM v){
      SQInteger id;
      sq_getinteger(v, -1, &id);
      sq_poptop(v);

      SQInteger num;
      sq_getinteger(v, -1, &num);
      sq_poptop(v);

      entityx::Entity e = getEntityHandle(v);

      if(e.valid()){
        entityx::ComponentHandle<ActionTriggerComponent> comp = e.component<ActionTriggerComponent>();
        //comp.get()->data.actionDescription = std::string(string);
        ActionData* d = getActionData(comp.get(), id);

        if(num == 0) d->inputId = I_ACCEPT;
        if(num == 1) d->inputId = I_DECLINE;
      }
    }

    static SQInteger setActionResource(HSQUIRRELVM v){
      SQInteger id;
      sq_getinteger(v, -1, &id);
      sq_poptop(v);

      const SQChar *string;
      sq_getstring(v, -1, &string);
      sq_poptop(v);

      entityx::Entity e = getEntityHandle(v);

      if(e.valid()){
        entityx::ComponentHandle<ActionTriggerComponent> comp = e.component<ActionTriggerComponent>();
        ActionData* d = getActionData(comp.get(), id);
        if(d) d->resourceName = std::string(string);
        //comp.get()->data.resourceName = std::string(string);
      }
    }

    static ActionData* getActionData(ActionTriggerComponent *comp, int id){
      //Get the data for the action components.
      if(comp == 0) return 0;
      if(id == 0) return &(comp->data);

      //If the component doesn't exist then create and and return it.
      if(id == 1){
        if(comp->data2 == 0){
          comp->data2 = new ActionData();
        }
        return comp->data2;
      }
      if(id == 2){
        if(comp->data3 == 0){
          comp->data3 = new ActionData();
        }
        return comp->data3;
      }
      if(id == 3){
        if(comp->data4 == 0){
          comp->data4 = new ActionData();
        }
        return comp->data4;
      }
      //If none of the correct ids were specified, return 0.
      return 0;
    }


    static SQInteger add(HSQUIRRELVM v){
      entityx::Entity e = getEntityHandle(v);
      entityx::ComponentHandle<PositionComponent> pos = e.component<PositionComponent>();

      Ogre::Vector3 position = Ogre::Vector3::ZERO;
      if(pos){
        position = pos.get()->pos.toOgre();
      }

      btCollisionObject *object = BulletManager::createCollisionObject(position, COLLISION_OBJECT_TYPE_ACTION);

      ActionTriggerComponent *comp = _addComponent<ActionTriggerComponent>(v, object);

      // comp->data.actionDescription = "Wow it's an action";
      // comp->data.inputId = I_ACCEPT;
      // comp->data.actionType = ACTION_TRIGGER_TYPE_DIALOG;
      // comp->data.resourceName = "dialog1.xml";

      //BulletManager::setObjectPointer(object, (void*)comp);
      BulletManager::setActionTriggerPointer(object, e);
      return 1;
    }
  };
}
