#include "ComponentScripts.h"
#include "../../world/Entity/EntityManager.h"

namespace AV{
EntityManager* ComponentScripts::entityManager = 0;

ComponentScripts::ComponentScripts(){

}

ComponentScripts::~ComponentScripts(){

}

void ComponentScripts::setupWrapper(HSQUIRRELVM v){

}

entityx::Entity ComponentScripts::getEntityHandle(HSQUIRRELVM v, SQInteger pos){
  SQUserPointer pointer;

  sq_getuserdata(v, pos, &pointer, NULL);

  entityx::Entity::Id *id = static_cast<entityx::Entity::Id*>(pointer);
  entityx::Entity e = entityManager->getEntity(*id);

  return e;
}
}
