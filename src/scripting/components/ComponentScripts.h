#ifndef __COMPONENT_SCRIPTS_H__
#define __COMPONENT_SCRIPTS_H__

#include <squirrel.h>
#include "entityx/entityx.h"
#include "../ScriptObject.h"

namespace AV{
class ScriptManager;
class EntityManager;

class ComponentScripts : public ScriptObject{
  friend ScriptManager;

public:
  ComponentScripts();
  ~ComponentScripts();

protected:

  virtual void setupWrapper(HSQUIRRELVM v);

  static EntityManager* entityManager;
  static entityx::Entity getEntityHandle(HSQUIRRELVM v, SQInteger pos = -1);

  template <typename T, typename ... Args>
  static T* _addComponent(HSQUIRRELVM v, Args && ... args){
    entityx::Entity e = getEntityHandle(v);

    if(e.valid() && !e.has_component<T>()){
      T* comp = (e.assign<T>(std::forward<Args>(args) ...)).get();
      sq_pushbool(v, true);
      return comp;
    }else{
      if(!e.valid()) std::cerr << "That entity is not valid." << '\n';
      else std::cerr << "Could not create the component. That entity already has that component." << '\n';
      sq_pushbool(v, false);
    }

    //No component was created, so return 0 as the pointer.
    return 0;
  }

  template <typename T>
  static void _removeComponent(HSQUIRRELVM v){
    entityx::Entity e = getEntityHandle(v);
    if(e.valid() && e.has_component<T>()){
      e.remove<T>();
      sq_pushbool(v, true);
    }else{
      if(!e.valid()) std::cerr << "That entity is not valid." << '\n';
      else std::cerr << "Could not remove the component. That entity does not have that component." << '\n';
      sq_pushbool(v, false);
    }
  }
};
}
#endif
