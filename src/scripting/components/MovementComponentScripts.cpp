#include "MovementComponentScripts.h"

#include <iostream>
#include "../../world/Entity/components/MovementComponent.h"
#include "../../world/Entity/EntityManager.h"

namespace AV{
MovementComponentScripts::MovementComponentScripts(){

}

MovementComponentScripts::~MovementComponentScripts(){

}

SQInteger MovementComponentScripts::componentMovementSetSpeed(HSQUIRRELVM v){
  SQFloat speed = 0;

  sq_getfloat(v, -1, &speed);
  entityx::Entity e = getEntityHandle(v, -2);

  if(!e.valid()){
    sq_pushbool(v, false);
    return 1;
  }

  entityx::ComponentHandle<MovementComponent> c = e.component<MovementComponent>();
  if(c){
    c.get()->speed = speed;
    sq_pushbool(v, true);
  }else{
    sq_pushbool(v, false);
  }

  return 1;
}

SQInteger MovementComponentScripts::componentMovementGetSpeed(HSQUIRRELVM v){
  entityx::Entity e = getEntityHandle(v);

  if(!e.valid()){
    sq_pushnull(v);
    return 1;
  }

  entityx::ComponentHandle<MovementComponent> c = e.component<MovementComponent>();
  if(c){
    SQFloat f = c.get()->speed;
    sq_pushfloat(v, f);
  }else{
    sq_pushnull(v);
  }

  return 1;
}

void MovementComponentScripts::setupWrapper(HSQUIRRELVM v){
  sq_pushstring(v, _SC("movement"), -1);
  sq_newtable(v);

  addFunction(v, componentMovementSetSpeed, "setSpeed");
  addFunction(v, componentMovementGetSpeed, "getSpeed");
  addFunction(v, add, "add");
  addFunction(v, remove, "remove");

  sq_newslot(v, -3, false);
}

//Find out what the situation is with knights.
//Maybe get a referenc from king
//Look for a good flat.
//Mention I'm on a gap year, how does it work for a year.
//Find out when the current contract ends.
//Will I need to pay poll tax.

//Why did I comment that in the code? What a wanker.
//In case you're wondering I did find a nice flat (only for a year though and the toilet's broken)
//Emailed the estate agents about it a month ago, still not fixed.
//They're all the same if I'm going to be honest.
//Happy to take a cut of the rent money but when it comes to do something about it it's all 'oh we forgot :3'
//Why am I writing this?
//Probably because it's quarter past 11 and I need to do the dishes for tomorrow.
//Anyway thanks for checking in, see you later!

SQInteger MovementComponentScripts::add(HSQUIRRELVM v){
  _addComponent<MovementComponent>(v, 0, Ogre::Vector3(1, 0, 0));
  return 1;
}

SQInteger MovementComponentScripts::remove(HSQUIRRELVM v){
  _removeComponent<MovementComponent>(v);
  return 1;
}
}
