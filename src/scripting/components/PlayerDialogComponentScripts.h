#ifndef __PLAYER_DIALOG_COMPONENT_SCRIPTS_H__
#define __PLAYER_DIALOG_COMPONENT_SCRIPTS_H__

#include "Ogre.h";

#include "ComponentScripts.h"
#include "../../world/Entity/components/PlayerDialogComponent.h"

namespace AV{
class PlayerDialogComponentScripts : public ComponentScripts{
public:
  PlayerDialogComponentScripts() {}
  ~PlayerDialogComponentScripts() {}

  void setupWrapper(HSQUIRRELVM v){
    sq_pushstring(v, _SC("playerDialog"), -1);
    sq_newtable(v);

    addFunction(v, add, "add");
    addFunction(v, remove, "remove");

    sq_newslot(v, -3, false);
  }

private:
  static SQInteger remove(HSQUIRRELVM v){
    _removeComponent<PlayerDialogComponent>(v);
    return 1;
  }


  static SQInteger add(HSQUIRRELVM v){
    SQInteger blockId = 0;
    sq_getinteger(v, -1, &blockId);

    const SQChar *meshPath;
    sq_getstring(v, -2, &meshPath);

    sq_pop(v, 2);

    _addComponent<PlayerDialogComponent>(v, meshPath, blockId);
    return 1;
  }

};
}

#endif
