#ifndef __PLAYER_INPUT_COMPONENT_SCRIPTS_H__
#define __PLAYER_INPUT_COMPONENT_SCRIPTS_H__

#include "Ogre.h";

#include "ComponentScripts.h"
#include "../../world/Entity/components/PlayerInputComponent.h"

namespace AV{
class PlayerInputComponentScripts : public ComponentScripts{
public:
  PlayerInputComponentScripts() {}
  ~PlayerInputComponentScripts() {}

  void setupWrapper(HSQUIRRELVM v){
    sq_pushstring(v, _SC("playerInput"), -1);
    sq_newtable(v);

    addFunction(v, add, "add");
    addFunction(v, remove, "remove");

    sq_newslot(v, -3, false);
  }

private:
  static SQInteger remove(HSQUIRRELVM v){
    _removeComponent<PlayerInputComponent>(v);
    return 1;
  }


  static SQInteger add(HSQUIRRELVM v){
    _addComponent<PlayerInputComponent>(v);
    return 1;
  }

};
}

#endif
