#ifndef __HEALTH_COMPONENT_SCRIPTS_H__
#define __HEALTH_COMPONENT_SCRIPTS_H__

#include "ComponentScripts.h"

namespace AV{
class HealthComponentScripts : public ComponentScripts{
public:
  HealthComponentScripts();
  ~HealthComponentScripts();

  void setupWrapper(HSQUIRRELVM v);

private:
  static SQInteger add(HSQUIRRELVM v);
  static SQInteger remove(HSQUIRRELVM v);

  static SQInteger componentMovementSetHealth(HSQUIRRELVM v);
  static SQInteger componentMovementGetHealth(HSQUIRRELVM v);

};
}
#endif
