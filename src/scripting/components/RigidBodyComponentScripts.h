#ifndef __RIGID_BODY_COMPONENT_SCRIPTS_H__
#define __RIGID_BODY_COMPONENT_SCRIPTS_H__

#include "ComponentScripts.h"
#include "../../world/Physics/BulletManager.h"
#include "../../world/Entity/components/RigidBodyComponent.h"
#include "../../world/Entity/EntityBodyManager.h"

#include "btBulletDynamicsCommon.h"

namespace AV{
class RigidBodyComponentScripts : public ComponentScripts{
public:
  RigidBodyComponentScripts() {}
  ~RigidBodyComponentScripts() {}

  void setupWrapper(HSQUIRRELVM v){
    sq_pushstring(v, _SC("rigidBody"), -1);
    sq_newtable(v);

    addFunction(v, add, "add");
    addFunction(v, addCharacterController, "addCharacterController");
    addFunction(v, remove, "remove");

    sq_newslot(v, -3, false);
  }

private:
  static SQInteger remove(HSQUIRRELVM v){
    entityx::Entity e = getEntityHandle(v);
    entityx::ComponentHandle<RigidBodyComponent> body = e.component<RigidBodyComponent>();
    if(body)
      BulletManager::destroyRigidBody(body.get()->body);

    _removeComponent<RigidBodyComponent>(v);
    return 1;
  }

  static btRigidBody* _addFromTable(HSQUIRRELVM v){
    SQFloat x,y,z,sX,sY,sZ,mass;
    const SQChar *type;
    x = y = z = 10;
    sX = sY = sZ = mass = 1;

    sq_pushnull(v);  //null iterator
    while(SQ_SUCCEEDED(sq_next(v,-2)))
    {
        //here -1 is the value and -2 is the key

        const SQChar *key;
        sq_getstring(v, -2, &key);

        if(strcmp(key, "type")) sq_getstring(v, -1, &type);
        else if(strcmp(key, "mass")) sq_getfloat(v, -1, &mass);
        else if(strcmp(key, "posX")) sq_getfloat(v, -1, &x);
        else if(strcmp(key, "posY")) sq_getfloat(v, -1, &y);
        else if(strcmp(key, "posZ")) sq_getfloat(v, -1, &z);
        else if(strcmp(key, "scaleX")) sq_getfloat(v, -1, &sX);
        else if(strcmp(key, "scaleY")) sq_getfloat(v, -1, &sY);
        else if(strcmp(key, "scaleZ")) sq_getfloat(v, -1, &sZ);

        sq_pop(v,2); //pops key and val before the next iteration
    }

    sq_pop(v,1); //pops the null iterator

    //btRigidBody *body = EntityBodyManager::createCubeBody(btVector3(x, y, z), btVector3(sX, 40, sZ));
    btVector3 pos(x, y, z);
    btVector3 scale(sX, sY, sZ);
    btRigidBody *body = EntityBodyManager::createCubeBody(pos, scale);
    //btRigidBody *body = EntityBodyManager::createRigidBody(ENTITY_BODY_CUBE, btVector3(x, 50, z), btVector3(sX, sY, sZ));

    return body;
  }

  static btRigidBody* _addAsNormal(HSQUIRRELVM v){
    SQFloat posX, posY, posZ;
    SQFloat scaleX, scaleY, scaleZ;
    SQFloat mass;
    SQInteger type;

    sq_getfloat(v, -1, &mass);
    sq_getfloat(v, -2, &scaleZ);
    sq_getfloat(v, -3, &scaleY);
    sq_getfloat(v, -4, &scaleX);
    sq_getfloat(v, -5, &posZ);
    sq_getfloat(v, -6, &posY);
    sq_getfloat(v, -7, &posX);
    sq_getinteger(v, -8, &type);

    sq_pop(v, 8);

    //return EntityBodyManager::createCubeBody(btVector3(posX, posY, posZ), btVector3(scaleX, scaleY, scaleZ));
    return EntityBodyManager::createRigidBody((ENTITY_BODY_SHAPE_TYPE)type, btVector3(posX, posY, posZ), btVector3(scaleX, scaleY, scaleZ), mass);
  }

  static SQInteger add(HSQUIRRELVM v){
    //Check what's at the top of the stack to determine which functionality to perform.
    int top = sq_gettop(v);
    SQObjectType objectType = sq_gettype(v, top);

    btRigidBody *body;
    if(objectType == OT_TABLE){
      body = _addFromTable(v);
    }
    else{
      body = _addAsNormal(v);
    }

    _addComponent<RigidBodyComponent>(v, body);
    return 1;
  }

  static SQInteger addCharacterController(HSQUIRRELVM v){
    btRigidBody *body = EntityBodyManager::createCapsuleBody(btVector3(0, 0, 0), 1, 6);
    //btRigidBody *body = EntityBodyManager::createCubeBody(btVector3(0, 0, 0), btVector3(1, 1, 1));

    _addComponent<RigidBodyComponent>(v, body);

    return 1;
  }

};
}

#endif
