#include "HealthComponentScripts.h"

#include <iostream>
#include "../../world/Entity/components/HealthComponent.h"
#include "../../world/Entity/EntityManager.h"

namespace AV{
HealthComponentScripts::HealthComponentScripts(){

}

HealthComponentScripts::~HealthComponentScripts(){

}

SQInteger HealthComponentScripts::componentMovementSetHealth(HSQUIRRELVM v){
  SQInteger health = 0;

  sq_getinteger(v, -1, &health);
  entityx::Entity e = getEntityHandle(v, -2);

  if(!e.valid()){
    sq_pushbool(v, false);
    return 1;
  }

  entityx::ComponentHandle<HealthComponent> c = e.component<HealthComponent>();
  if(c){
    c.get()->health = health;
    sq_pushbool(v, true);
  }else{
    sq_pushbool(v, false);
  }

  return 1;
}

SQInteger HealthComponentScripts::componentMovementGetHealth(HSQUIRRELVM v){
  entityx::Entity e = getEntityHandle(v);

  if(!e.valid()){
    sq_pushnull(v);
    return 1;
  }

  entityx::ComponentHandle<HealthComponent> c = e.component<HealthComponent>();
  if(c){
    SQInteger h = c.get()->health;
    sq_pushinteger(v, h);
  }else{
    sq_pushnull(v);
  }

  return 1;
}

void HealthComponentScripts::setupWrapper(HSQUIRRELVM v){
  sq_pushstring(v, _SC("health"), -1);
  sq_newtable(v);

  addFunction(v, componentMovementSetHealth, "setHealth");
  addFunction(v, componentMovementGetHealth, "getHealth");
  addFunction(v, add, "add");
  addFunction(v, remove, "remove");

  sq_newslot(v, -3, false);
}

SQInteger HealthComponentScripts::add(HSQUIRRELVM v){
  _addComponent<HealthComponent>(v, 10);
  return 1;
}

SQInteger HealthComponentScripts::remove(HSQUIRRELVM v){
  _removeComponent<HealthComponent>(v);
  return 1;
}
}
