#ifndef __DIALOG_SCRIPTS_H__
#define __DIALOG_SCRIPTS_H__

#include "ScriptObject.h"

namespace AV{
  class ScriptManager;
  class DialogScripts : public ScriptObject{
  friend ScriptManager;
  public:
    DialogScripts();
    ~DialogScripts();

    static SQInteger loadAndStartDialog(HSQUIRRELVM v);

    void setupWrapper(HSQUIRRELVM v);
  };
}

#endif
