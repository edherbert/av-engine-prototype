#include "EntityScripts.h"

#include "../world/Entity/EntityManager.h"
#include "../world/Entity/Player/PlayerManager.h"

#include "../world/Entity/components/MetaComponent.h"

#include "ScriptManager.h"

#include <iostream>

namespace AV{
EntityManager* EntityScripts::entityManager = 0;

EntityScripts::EntityScripts(EntityManager *entityManager){
    EntityScripts::entityManager = entityManager;
}

EntityScripts::~EntityScripts(){

}

SQInteger EntityScripts::createEntity(HSQUIRRELVM v){
  std::cout << "Creating an Entity" << '\n';

  SQFloat x, y, z;
  sq_getfloat(v, -1, &z);
  sq_getfloat(v, -2, &y);
  sq_getfloat(v, -3, &x);

  SQInteger chunkX, chunkY;
  sq_getinteger(v, -4, &chunkY);
  sq_getinteger(v, -5, &chunkX);

  sq_pop(v, 5);

  std::cout << x << " " << y << " " << z << " " << chunkX << " " << chunkY << '\n';

  //entityx::Entity::Id id = EntityScripts::entityManager->createEntity(0, 0, Ogre::Vector3(0, 0, 0)).id();
  entityx::Entity::Id id = EntityScripts::entityManager->createEntity(chunkX, chunkY, Ogre::Vector3(x, y, z)).id();

  void* pointer = (void*)sq_newuserdata(v, sizeof(entityx::Entity::Id));
  new (pointer) entityx::Entity::Id(id);

  return 1;
}

SQInteger EntityScripts::destroyEntity(HSQUIRRELVM v){
  SQUserPointer pointer;

  sq_getuserdata(v, -1, &pointer, NULL);

  entityx::Entity::Id *id = static_cast<entityx::Entity::Id*>(pointer);
  entityx::Entity e = entityManager->getEntity(*id);

  if(e.valid()){
    e.destroy();
    sq_pushbool(v, true);
  }else{
    sq_pushbool(v, false);
  }
  return 1;
}

SQInteger EntityScripts::moveEntity(HSQUIRRELVM v){
  SQFloat x, y, z;
  sq_getfloat(v, -1, &z);
  sq_getfloat(v, -2, &y);
  sq_getfloat(v, -3, &x);

  sq_pop(v, 3);

  SQUserPointer pointer;

  sq_getuserdata(v, -1, &pointer, NULL);

  entityx::Entity::Id *id = static_cast<entityx::Entity::Id*>(pointer);
  entityx::Entity e = entityManager->getEntity(*id);

  if(e.valid()){
    entityManager->moveEntity(e, Ogre::Vector3(x, y, z));
    sq_pushbool(v, true);
  }else{
    sq_pushbool(v, false);
  }
  return 1;
}

SQInteger EntityScripts::setPlayer(HSQUIRRELVM v){
  SQUserPointer pointer;

  sq_getuserdata(v, -1, &pointer, NULL);

  entityx::Entity::Id *id = static_cast<entityx::Entity::Id*>(pointer);
  entityx::Entity e = entityManager->getEntity(*id);

  if(e.valid()){
    entityManager->getPlayerManager()->setPlayer(e);
  }
}

SQInteger EntityScripts::setController(HSQUIRRELVM v){
  SQUserPointer pointer;

  sq_getuserdata(v, -2, &pointer, NULL);

  entityx::Entity::Id *id = static_cast<entityx::Entity::Id*>(pointer);
  entityx::Entity e = entityManager->getEntity(*id);

  SQInteger controllerId;
  sq_getinteger(v, -1, &controllerId);
  sq_pop(v, 1);

  if(e.valid()){
    entityManager->setEntityController(e, (CONTROLLER_TYPE)controllerId);
  }
}

void EntityScripts::setupWrapper(HSQUIRRELVM v){
  addFunction(v, createEntity, "createEntity");
  addFunction(v, destroyEntity, "destroyEntity");
  addFunction(v, moveEntity, "move");
  addFunction(v, setPlayer, "setPlayer");
  addFunction(v, setController, "setController");
}
}
