#include "CameraScripts.h"

#include "OgreCamera.h"

namespace AV{
  Ogre::Camera* CameraScripts::camera = 0;

  CameraScripts::CameraScripts(Ogre::Camera *camera){
      CameraScripts::camera = camera;
  }

  CameraScripts::~CameraScripts(){

  }

  SQInteger CameraScripts::moveCamera(HSQUIRRELVM v){
    SQFloat x, y, z;
    sq_getfloat(v, -1, &z);
    sq_getfloat(v, -2, &y);
    sq_getfloat(v, -3, &x);

    sq_pop(v, 3);

    camera->setPosition(Ogre::Vector3(x, y, z));
  }

  SQInteger CameraScripts::lookAt(HSQUIRRELVM v){
    SQFloat x, y, z;
    sq_getfloat(v, -1, &z);
    sq_getfloat(v, -2, &y);
    sq_getfloat(v, -3, &x);

    sq_pop(v, 3);

    camera->lookAt(Ogre::Vector3(x, y, z));
  }

  void CameraScripts::setupWrapper(HSQUIRRELVM v){
    addFunction(v, moveCamera, "setPosition");
    addFunction(v, lookAt, "lookAt");
  }
}
