#pragma once

#include "ScriptObject.h"

namespace AV{
  class GameScripts : public ScriptObject{
  public:
    GameScripts();
    ~GameScripts();

    void setupWrapper(HSQUIRRELVM v);
  private:
    static SQInteger saveGame(HSQUIRRELVM v);
  };
};
