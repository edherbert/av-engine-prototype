//The size of each chunk in relation to world size.
#define CHUNK_SIZE_WORLD 500
//The number of vertices on the terrain
//#define TERRAIN_RESOLUTION 128
#define TERRAIN_RESOLUTION 512

//The number of vertices in a cell.
//The terrain resolution is divided by the cell resolution to find the number of cells needed.
#define CELL_RESOLUTION 64
//The height of the terrain vertices in the y axis
#define TERRAIN_HEIGHT 100
