#ifndef __WORLD_H__
#define __WORLD_H__

#include <vector>
#include <string>

#include <OgreString.h>
#include "Slots/SlotPosition.h"

#include "OgreVector3.h"
//#include "entityx/entityx.h"

namespace AV{
  class SlotManager;
  class EntityManager;
  class ScriptManager;
  class DialogManager;
  class BulletManager;
  class ChunkRadiusLoader;
  class TAreaManager;

  class World{
    friend ScriptManager;
  public:
    World();
    ~World();

    EntityManager* getEntityManager();
    SlotManager* getSlotManager() { return slotManager; };
    BulletManager* getBulletManager() { return bulletManager; };
    ChunkRadiusLoader* getChunkRadiusLoader() { return chunkRadiusLoader; };
    Ogre::Camera* getCamera();
    Ogre::SceneManager* getSceneManager();

    void loadChunk(Ogre::String mapName, int chunkX, int chunkY);

    void teleportPlayer(int chunkX, int chunkY, Ogre::Vector3 position, Ogre::String mapName = "");

    void update();

    void setOrigin(SlotPosition newOrigin);
    SlotPosition getOrigin() { return worldOrigin; };

    Ogre::String getCurrentMap();

    bool testVal = false;

    /*void loadChunk(Ogre::String mapName, int chunkX, int chunkY);
    void unloadChunk(Ogre::String mapName, int chunkX, int chunkY);

    void setChunkVisible(Ogre::String mapName, int chunkX, int chunkY);
    void setChunkInvisible(Ogre::String mapName, int chunkX, int chunkY);

    void moveEntity(entityx::Entity::Id *id, Ogre::Vector3 ammount);

    void update();

    entityx::Entity getEntity(entityx::Entity::Id *id);
    entityx::Entity createEntity(int chunkX, int chunkY, Ogre::Vector3 position);

    //entityx::ComponentHandle getComponent(int componentId, entityx::Entity::Id *id);

    void getComponent(entityx::Entity::Id *id);*/

  private:
    SlotManager *slotManager;
    EntityManager *entityManager;
    DialogManager *dialogManager;
    BulletManager *bulletManager;
    ChunkRadiusLoader *chunkRadiusLoader;
    TAreaManager *tAreaManager;
    //ScriptManager *scriptManager;

    Ogre::Camera *camera;
    Ogre::SceneManager *sceneManager;

    SlotPosition worldOrigin = {0, 0, Ogre::Vector3(0, 0, 0)};

    void _setupWorld();
  };
}

#endif
