#pragma once

#include "OgreVector3.h"
#include "entityx/entityx.h"

class btCollisionWorld;
class btCollisionShape;
class btCollisionObject;
class btDefaultSerializer;

namespace AV{
  class DamageData;
  class EntityManager;

  enum DAMAGE_OBJECT_TYPE{
    DAMAGE_OBJECT_TYPE_SENDER,
    DAMAGE_OBJECT_TYPE_RECEIVER
  };

  struct SenderPointerData{
    SenderPointerData(entityx::Entity::Id eId, DamageData* data) : eId(eId), data(data) { }
    entityx::Entity::Id eId;
    DamageData *data;
  };


  class DamageWorldManager{
  public:
    DamageWorldManager();
    ~DamageWorldManager();

    static void update();

    static btCollisionObject* createDamageSender(Ogre::Vector3 pos, DamageData *data, btCollisionShape *shape, entityx::Entity::Id eId = entityx::Entity::INVALID, bool addToWorld = true);
    static btCollisionObject* createDamageReceiver(Ogre::Vector3 pos, entityx::Entity &e, btCollisionShape *shape, bool addToWorld = true);

    static void destroyCollisionObject(btCollisionObject *object);

    static EntityManager *entityManager;

    static void serialise(const std::string& filePath, btDefaultSerializer *serialiser);

  private:
    static btCollisionWorld *collisionWorld;

    static btCollisionObject* _createCollisionObject(Ogre::Vector3 pos, DAMAGE_OBJECT_TYPE type, void *data, btCollisionShape *shape, bool addToWorld);
  };
};
