#include "DamageWorldManager.h"

#include <iostream>
#include "btBulletDynamicsCommon.h"
#include "../Entity/Damage/AfflictionData.h"
#include "../Entity/Damage/DamageInflictor.h"

#include "../Entity/EntityManager.h"

#include <iostream>
#include <fstream>
#include <stdio.h>


namespace AV{
  btCollisionWorld* DamageWorldManager::collisionWorld = 0;
  EntityManager* DamageWorldManager::entityManager = 0;

  DamageWorldManager::DamageWorldManager(){
    btBroadphaseInterface* broadphaseCollision = new btDbvtBroadphase();
    btDefaultCollisionConfiguration* collisionWorldConfiguration = new btDefaultCollisionConfiguration();
    btCollisionDispatcher* collisionDispatcher = new btCollisionDispatcher(collisionWorldConfiguration);

    collisionWorld = new btCollisionWorld(collisionDispatcher, broadphaseCollision, collisionWorldConfiguration);


    DamageData *data = new DamageData();
    data->attackPower = 10;

    // createCollisionObject(Ogre::Vector3(20, 0, 20), DAMAGE_OBJECT_TYPE_SENDER, new btBoxShape(btVector3(10, 1, 1)));
    // createCollisionObject(Ogre::Vector3(0.5, 0.5, 0.5), DAMAGE_OBJECT_TYPE_RECEIVER, new btBoxShape(btVector3(1, 1, 1)));

    // entityx::Entity e;
    btCollisionObject *obj1 = createDamageSender(Ogre::Vector3(20, 0, 20), data, new btBoxShape(btVector3(10, 5, 5)));
    // btCollisionObject *obj2 = createDamageReceiver(Ogre::Vector3(0.5, 0.5, 0.5), e, new btBoxShape(btVector3(1, 1, 1)));

    // destroyCollisionObject(obj1);
    // destroyCollisionObject(obj2);
  }

  DamageWorldManager::~DamageWorldManager(){

  }

  btCollisionObject* DamageWorldManager::createDamageSender(Ogre::Vector3 pos, DamageData *data, btCollisionShape *shape, entityx::Entity::Id eId, bool addToWorld){
    //The entity needs to be wrapped around the actual data.
    SenderPointerData *senderData = new SenderPointerData(eId, data);

    btCollisionObject *body = _createCollisionObject(pos, DAMAGE_OBJECT_TYPE_SENDER, (void*) senderData, shape, addToWorld);

    return body;
  }

  btCollisionObject* DamageWorldManager::createDamageReceiver(Ogre::Vector3 pos, entityx::Entity &e, btCollisionShape *shape, bool addToWorld){
    //entityx::Entity *entity = new entityx::Entity(e);
    entityx::Entity::Id* id = new entityx::Entity::Id(e.id());

    return _createCollisionObject(pos, DAMAGE_OBJECT_TYPE_RECEIVER, (void*) id, shape, addToWorld);
  }

  btCollisionObject* DamageWorldManager::_createCollisionObject(Ogre::Vector3 pos, DAMAGE_OBJECT_TYPE type, void *data, btCollisionShape *shape, bool addToWorld){
    btCollisionObject *object = new btCollisionObject();
    object->setCollisionShape(shape);
    object->getWorldTransform().setOrigin(btVector3(pos.x, pos.y, pos.z));

    object->setUserIndex((int)type);
    object->setUserPointer(data);

    if(addToWorld) collisionWorld->addCollisionObject(object);

    return object;
  }

  void DamageWorldManager::serialise(const std::string& filePath, btDefaultSerializer *serialiser){
    std::cout << "Serialising damage world: " << filePath << '\n';

    collisionWorld->serialize(serialiser);
    FILE* file = fopen(filePath.c_str(), "wb");
    fwrite(serialiser->getBufferPointer(),serialiser->getCurrentBufferSize(),1, file);
    fclose(file);

    delete serialiser;
  }

  void DamageWorldManager::destroyCollisionObject(btCollisionObject *object){
    void* objPointer = object->getUserPointer();
    DAMAGE_OBJECT_TYPE type = (DAMAGE_OBJECT_TYPE) object->getUserIndex();
    if(objPointer != 0){
      if(type == DAMAGE_OBJECT_TYPE_SENDER){
        //You need to unwrap the data wrap to delete it properly.
        SenderPointerData *senderData = (SenderPointerData*) objPointer;
        delete senderData->data;
        delete senderData;
      }
      else if(type == DAMAGE_OBJECT_TYPE_RECEIVER) delete (entityx::Entity::Id*)objPointer;
    }

    collisionWorld->removeCollisionObject(object);
  }

  void DamageWorldManager::update(){
    collisionWorld->performDiscreteCollisionDetection();

    //btAlignedObjectArray<btCollisionObject*> pairArray = playerCollisionShape->getOverlappingPairs();

    int numManifolds = collisionWorld->getDispatcher()->getNumManifolds();
    for (int i = 0; i < numManifolds; i++){
      btPersistentManifold* contactManifold = collisionWorld->getDispatcher()->getManifoldByIndexInternal(i);
      btCollisionObject* obA = (btCollisionObject*)contactManifold->getBody0();
      btCollisionObject* obB = (btCollisionObject*)contactManifold->getBody1();

      int typeA = obA->getUserIndex();
      int typeB = obB->getUserIndex();

      if(typeA == typeB) continue;

      btCollisionObject *sender;
      btCollisionObject *receiver;
      //The two shapes are senders and receivers, so assign them to some pointers.
      if(typeA == DAMAGE_OBJECT_TYPE_SENDER){
        sender = obA;
        receiver = obB;
      }else{
        sender = obB;
        receiver = obA;
      }

      SenderPointerData *senderPointerData = (SenderPointerData*) sender->getUserPointer();
      DamageData *damageData = senderPointerData->data;

      entityx::Entity::Id *id = static_cast<entityx::Entity::Id*>(receiver->getUserPointer());
      entityx::Entity targetEntity = entityManager->getEntity(*id);

      //It's possible two colliders will collide with the same entity at once.
      //If the entity's already been removed by the first then it's no longer valid, so check that.
      if(!targetEntity.valid()){
        std::cout << "Entity not valid." << '\n';
        continue;
      }
      DamageInflictor::damageEntity(targetEntity, *damageData);

      if(damageData->destroyOnDamage){
        //If the entity is going to be destroyed, firstly get their id to destroy them.
        entityx::Entity::Id senderEntityId = senderPointerData->eId;
        if(senderEntityId != entityx::Entity::INVALID){
          entityManager->destroyEntity(senderEntityId);
        }
      }

    }
  }
}
