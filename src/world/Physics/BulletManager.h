#ifndef __BULLET_MANAGER_H__
#define __BULLET_MANAGER_H__

class btBroadphaseInterface;
class btDefaultCollisionConfiguration;
class btCollisionDispatcher;
class btSequentialImpulseConstraintSolver;
class btDiscreteDynamicsWorld;

class btCollisionWorld;
class btCollisionObject;
class btGhostObject;
class btRigidBody;
class btCollisionShape;

#include "../../EventManager/EventReceiver.h"
#include <OgreVector3.h>
#include <string>
#include <set>
#include "BulletCollision/CollisionDispatch/btCollisionWorld.h"

namespace entityx{
  class Entity;
}

namespace AV{
  class RigidBodyComponentScripts;
  class PlayerManager;
  class ShapeFileParser;
  class ShapeChunk;
  class TArea;
  class PlayerControllerManager;
  class EntityBodyManager;
  class ControllerManager;
  class DamageWorldManager;

  struct collisionShapeData{
    collisionShapeData(std::string scriptName)
      : scriptName(scriptName) {}
    std::string scriptName = "";
  };

  enum CollisionObjectType{
    COLLISION_OBJECT_TYPE_NULL,
    COLLISION_OBJECT_TYPE_TRIGGER,
    COLLISION_OBJECT_TYPE_ACTION,

    COLLISION_OBJECT_TYPE_TAREA_BOX,
    COLLISION_OBJECT_TYPE_TAREA_OUTER_BOX,
    COLLISION_OBJECT_TYPE_TAREA_TRIGGER
  };

  class BulletManager : public EventReceiver{
  friend RigidBodyComponentScripts;
  friend PlayerManager;
  friend PlayerControllerManager;
  friend ShapeFileParser;
  friend ShapeChunk;
  friend TArea;
  friend EntityBodyManager;
  friend ControllerManager;
  public:
    BulletManager();
    ~BulletManager();

    void update();

    static btCollisionObject* createCollisionObject(Ogre::Vector3 pos, CollisionObjectType type, bool addToWorld = true);
    static btCollisionObject* createCollisionObject(Ogre::Vector3 pos, CollisionObjectType type, btCollisionShape *shape, bool addToWorld = true);

    static void destroyCollisionObject(btCollisionObject* object);
    static void destroyRigidBody(btRigidBody *body);

    static void addCollisionObject(btCollisionObject *object);
    static void removeCollisionObject(btCollisionObject *object);
    static void setObjectOrigin(Ogre::Vector3 pos, btCollisionObject *object);

    static void setActionTriggerPointer(btCollisionObject *object, entityx::Entity &entity);
    static void setTAreaPointer(btCollisionObject *object, TArea *tarea);

    static void serialise(const std::string& saveDirectory);

    static btCollisionWorld::ClosestRayResultCallback rayCastWorld(btVector3 btFrom, btVector3 btTo);

  private:
    ShapeFileParser *shapeFileParser;
    DamageWorldManager *damageWorldManager;

    btBroadphaseInterface* broadphase;
    btDefaultCollisionConfiguration* collisionConfiguration;
    btCollisionDispatcher* dispatcher;
    btSequentialImpulseConstraintSolver* solver;

    btGhostObject *playerCollisionShape;

    static btCollisionShape *defaultShape;

    void notifyStatsEvent(StatsEvent &e);

    static btDiscreteDynamicsWorld* world;
    static btCollisionWorld *collisionWorld;

    static void playerEnteredObject(btCollisionObject *object);
    static void playerLeftObject(btCollisionObject *object);

    static void handleActionTriggerCollision(btCollisionObject *object, bool actionEnter);
    static void handleTriggerCollision(btCollisionObject *object, bool triggerEntered);

    static std::set<btCollisionObject*> newContacts;
    int collisionCount = 0;

  };
}

#endif
