#include "BulletManager.h"

#include "btBulletDynamicsCommon.h"

#include <iostream>
#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "DamageWorldManager.h"

#include "../Slots/eChunkFileParsers/ShapeFileParser.h"

#include "../../EventManager/EventManager.h"
#include "../../EventManager/Events/StatsEvent.h"
#include "../../EventManager/Events/ActionEvent.h"
#include "../../EventManager/Events/TriggerEvent.h"

#include "../Entity/components/ActionTriggerComponent.h"
#include "entityx/entityx.h"

namespace AV{
  btDiscreteDynamicsWorld* BulletManager::world = 0;
  btCollisionWorld* BulletManager::collisionWorld = 0;
  btCollisionShape* BulletManager::defaultShape = new btBoxShape(btVector3(50, 50, 50));
  std::set<btCollisionObject*> BulletManager::newContacts = std::set<btCollisionObject*>();

  BulletManager::BulletManager(){
    EventManager::subscribeToEvent(this, EVENT_TYPE_STATS);

    damageWorldManager = new DamageWorldManager();

    shapeFileParser = new ShapeFileParser(this);

    broadphase = new btDbvtBroadphase();

    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);

    solver = new btSequentialImpulseConstraintSolver();


    BulletManager::world = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
    world->setGravity(btVector3(0, -9.8, 0));


    btScalar mass = 0;
    btVector3 fallInertia(0, 0, 0);
    btCollisionShape *fallShape = new btBoxShape(btVector3(10, 10, 10));
    fallShape->calculateLocalInertia(mass, fallInertia);

    btDefaultMotionState *fallMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));

    btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);
    fallRigidBodyCI.m_friction = 1;

    //btRigidBody *fallRigidBody = new btRigidBody(fallRigidBodyCI);
    btCollisionObject *fallRigidBody = new btCollisionObject();
    fallRigidBody->setCollisionShape(fallShape);
    //BulletManager::world->addRigidBody(fallRigidBody);
    //BulletManager::world->addCollisionObject(fallRigidBody);


    //Collision world
    btBroadphaseInterface* broadphaseCollision = new btDbvtBroadphase();
    btDefaultCollisionConfiguration* collisionWorldConfiguration = new btDefaultCollisionConfiguration();
    btCollisionDispatcher* collisionDispatcher = new btCollisionDispatcher(collisionWorldConfiguration);
    //btSequentialImpulseConstraintSolver* collisionSolver;

    BulletManager::collisionWorld = new btCollisionWorld(collisionDispatcher, broadphaseCollision, collisionWorldConfiguration);

    //gContactAddedCallback  = sContactAddedCallback;

    // first = new btCollisionObject();
    // first->setCollisionShape(fallShape);
    // collisionWorld->addCollisionObject(first);
    // first->getWorldTransform().setOrigin(btVector3(0, 0, pos));

    // first = new btGhostObject();
    // first->setCollisionShape(fallShape);
    // collisionWorld->addCollisionObject(first);
    // first->getWorldTransform().setOrigin(btVector3(0, 0, pos));

    // btCollisionObject *second = new btCollisionObject();
    // second->setCollisionShape(fallShape);
    // collisionWorld->addCollisionObject(second);
    // second->getWorldTransform().setOrigin(btVector3(0, 0, 0));

    playerCollisionShape = new btGhostObject();
    playerCollisionShape->setCollisionShape(new btBoxShape(btVector3(1, 1, 1)));
    BulletManager::collisionWorld->addCollisionObject(playerCollisionShape);
    playerCollisionShape->getWorldTransform().setOrigin(btVector3(0, 0, 0));


    //playerCollisionShape->setCollisionFlags(playerCollisionShape->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);

    // for(int y = 0; y < 10; y++){
    //   createCollisionObject(Ogre::Vector3(y * 50 + 100, 0, 0), COLLISION_OBJECT_TYPE_ACTION);
    // }

    // btCollisionObject* test = createCollisionObject(Ogre::Vector3::ZERO);
    // destroyCollisionObject(test);

    BulletManager::collisionWorld->getPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());

  }

  void BulletManager::notifyStatsEvent(StatsEvent &e){
    if(e.statsType == STATS_OGRE_PLAYER_POS){
      playerCollisionShape->getWorldTransform().setOrigin(btVector3(e.position.x, e.position.y, e.position.z));
    }
  }

  BulletManager::~BulletManager(){
    delete world;
    delete solver;
    delete collisionConfiguration;
    delete dispatcher;
    delete broadphase;

    delete shapeFileParser;
    delete damageWorldManager;
  }

  btCollisionObject* BulletManager::createCollisionObject(Ogre::Vector3 pos, CollisionObjectType type, bool addToWorld){
    return createCollisionObject(pos, type, defaultShape, addToWorld);
  }

  btCollisionObject* BulletManager::createCollisionObject(Ogre::Vector3 pos, CollisionObjectType type, btCollisionShape *shape, bool addToWorld){
    btCollisionObject *object = new btCollisionObject();
    object->setCollisionShape(shape);
    object->getWorldTransform().setOrigin(btVector3(pos.x, pos.y, pos.z));

    if(addToWorld) BulletManager::collisionWorld->addCollisionObject(object);

    object->setUserIndex(type);
    // if(type == COLLISION_OBJECT_TYPE_TRIGGER){
    //   object->setUserPointer((void*)new collisionShapeData("Something"));
    // }else if(type == COLLISION_OBJECT_TYPE_ACTION){
    //
    // }

    return object;
  }

  void BulletManager::setActionTriggerPointer(btCollisionObject *object, entityx::Entity &entity){
    if(object->getUserIndex() != COLLISION_OBJECT_TYPE_ACTION) return;

    entityx::Entity *p = new entityx::Entity(entity);
    object->setUserPointer(p);
  }

  void BulletManager::setTAreaPointer(btCollisionObject *object, TArea *tarea){
    const int id = object->getUserIndex();
    if(id != COLLISION_OBJECT_TYPE_TAREA_BOX && id != COLLISION_OBJECT_TYPE_TAREA_OUTER_BOX && id != COLLISION_OBJECT_TYPE_TAREA_TRIGGER) return;

    object->setUserPointer(tarea);
  }

  void BulletManager::serialise(const std::string& saveDirectory){
    std::string filePath = saveDirectory + "DynamicsWorld.bullet";
    std::cout << "Serialising dynamic world: " << filePath << '\n';

    btDefaultSerializer *serialiser = new btDefaultSerializer();

    //Dynamic world
    world->serialize(serialiser);
    FILE* file = fopen(filePath.c_str(), "wb");
    fwrite(serialiser->getBufferPointer(),serialiser->getCurrentBufferSize(),1, file);
    fclose(file);

    //Collision World
    filePath = saveDirectory + "CollisionWorld.bullet";
    std::cout << "Serialising collision world: " << filePath << '\n';

    collisionWorld->serialize(serialiser);
    file = fopen(filePath.c_str(), "wb");
    fwrite(serialiser->getBufferPointer(),serialiser->getCurrentBufferSize(),1, file);
    fclose(file);

    delete serialiser;
  }

  void BulletManager::destroyCollisionObject(btCollisionObject* object){
    //Destroying it means checking if the player is doing a collision with the entity at the moment.
    //If they are, then check to see if it's in the collision set.
    std::set<btCollisionObject*>::iterator it = newContacts.find(object);
    if(it != newContacts.end()){
      //The object was found in the list, which means the player is colliding with it at the moment.
      newContacts.erase(it);
      //Do the logic for when the player leaves an action trigger.
      playerLeftObject(object);
    }

    BulletManager::collisionWorld->removeCollisionObject(object);

    //TODO fix this for triggers.
    //Remove it's data.
    //delete (collisionShapeData*)object->getUserPointer();
    delete (entityx::Entity*)object->getUserPointer();
    //Destroy it
    delete object;
  }

  void BulletManager::addCollisionObject(btCollisionObject *object){
    collisionWorld->addCollisionObject(object);
  }

  void BulletManager::removeCollisionObject(btCollisionObject *object){
    collisionWorld->removeCollisionObject(object);
  }

  void BulletManager::destroyRigidBody(btRigidBody *body){
    BulletManager::world->removeCollisionObject(body);
    delete body;
  }

  void BulletManager::update(){
    DamageWorldManager::update();
    world->stepSimulation(1000/30, 2);
    BulletManager::collisionWorld->performDiscreteCollisionDetection();

    //first->getWorldTransform().setOrigin(btVector3(0, pos, 0));

    //btManifoldArray manifoldArray;
    //btBroadphasePairArray& pairArray = first->getOverlappingPairCache()->getOverlappingPairArray();
    //btBroadphasePairArray& pairArray = first->getOverlappingPairs();

    //Get the collision list.
    btAlignedObjectArray<btCollisionObject*> pairArray = playerCollisionShape->getOverlappingPairs();
    //Checking the size of the collision array will determine if a change has occured.
    //If the size of the collision array is different then my values need to be updated.
    if(collisionCount != pairArray.size()){
      //
      for(int i = 0; i < pairArray.size(); i++){
        //This collider doesn't take into account action triggers.
        //if(pairArray[i]->getUserIndex() != COLLISION_OBJECT_TYPE_ACTION) continue;

        if(newContacts.find(pairArray[i]) == newContacts.end()){
          //New collision not in the set, so add it.

          newContacts.insert(pairArray[i]);
          playerEnteredObject(pairArray[i]);
        }else{
          //The collision is already listed, so no need to do anything.
        }
      }

      //Now check to see if any have been removed.
      std::set<btCollisionObject*>::iterator it = newContacts.begin();
      //Go through all the contacts.
      while(it != newContacts.end()){
        //Traverse the list and check to see if the value in the collision list is also in the contacts list.
        bool found = false;
        for(int i = 0; i < pairArray.size(); i++){
          if(pairArray[i] == (*it)){
            found = true;
            break;
          }
        }

        //If the value isn't in both the new collisions and the old collisions then that means the player has left the trigger.
        if(!found){
          playerLeftObject((*it));
          //So remove it.
          newContacts.erase(it);
          //it++;
        }else{
          //it++;
        }

        it++;
      }
      //Set the collision count to a new value.
      //This is used to determine what needs to be updated.
      collisionCount = pairArray.size();
    }
  }

  void BulletManager::handleActionTriggerCollision(btCollisionObject *object, bool actionEnter){
    entityx::Entity *entity = (entityx::Entity*)object->getUserPointer();
    if(entity && entity->valid()){
      entityx::ComponentHandle<ActionTriggerComponent> compHandle = entity->component<ActionTriggerComponent>();
      if(compHandle){
        ACTION_EVENT_TYPE actionType = ACTION_EVENT_TYPE_ENTER;
        if(!actionEnter) actionType = ACTION_EVENT_TYPE_LEAVE;

        ActionTriggerComponent *comp = compHandle.get();

        ActionEvent e;
        e.data = comp->data;
        e.eventType = actionType;
        EventManager::transmitActionEvent(e);

        if(comp->data2){
          ActionEvent d;
          d.data = *comp->data2;
          d.eventType = actionType;
          EventManager::transmitActionEvent(d);
        }
        if(comp->data3){
          ActionEvent d;
          d.data = *comp->data3;
          d.eventType = actionType;
          EventManager::transmitActionEvent(d);
        }
        if(comp->data4){
          ActionEvent d;
          d.data = *comp->data4;
          d.eventType = actionType;
          EventManager::transmitActionEvent(d);
        }
      }
    }
  }

  void BulletManager::handleTriggerCollision(btCollisionObject *object, bool triggerEntered){
    //Do this for now.
    //I'm not sure if triggers will actually need to send an event when they're exited.
    //if(!triggerEntered) return;

    TriggerEvent event;

    TRIGGER_EVENT_TYPE type = TRIGGER_EVENT_NULL;

    int id = object->getUserIndex();
    switch(id){
      case COLLISION_OBJECT_TYPE_TAREA_BOX:
        type = TRIGGER_EVENT_TAREA_BOX;
        break;
      case COLLISION_OBJECT_TYPE_TAREA_OUTER_BOX:
        type = TRIGGER_EVENT_TAREA_OUTER_BOX;
        break;
      case COLLISION_OBJECT_TYPE_TAREA_TRIGGER:
        type = TRIGGER_EVENT_TAREA_TRIGGER;
        break;
    }
    event.type = type;
    event.tarea = (TArea*)object->getUserPointer();
    event.triggerEntered = triggerEntered;

    EventManager::transmitTriggerEvent(event);
  }

  void BulletManager::playerEnteredObject(btCollisionObject *object){
    int id = object->getUserIndex();

    if(id == COLLISION_OBJECT_TYPE_ACTION) handleActionTriggerCollision(object, true);
    else if(id == COLLISION_OBJECT_TYPE_TRIGGER);
    else if(id == COLLISION_OBJECT_TYPE_TAREA_BOX || id == COLLISION_OBJECT_TYPE_TAREA_OUTER_BOX || id == COLLISION_OBJECT_TYPE_TAREA_TRIGGER){
      handleTriggerCollision(object, true);
    }
  }

  void BulletManager::playerLeftObject(btCollisionObject *object){
    int id = object->getUserIndex();

    if(id == COLLISION_OBJECT_TYPE_ACTION) handleActionTriggerCollision(object, false);
    else if(id == COLLISION_OBJECT_TYPE_TRIGGER);
    else if(id == COLLISION_OBJECT_TYPE_TAREA_BOX || id == COLLISION_OBJECT_TYPE_TAREA_OUTER_BOX || id == COLLISION_OBJECT_TYPE_TAREA_TRIGGER){
      handleTriggerCollision(object, false);
    }
  }

  btCollisionWorld::ClosestRayResultCallback BulletManager::rayCastWorld(btVector3 btFrom, btVector3 btTo){
    btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);

    BulletManager::world->rayTest(btFrom, btTo, res);

    return res;
  }

  void BulletManager::setObjectOrigin(Ogre::Vector3 pos, btCollisionObject *object){
    std::cout << pos << '\n';
    object->getWorldTransform().setOrigin(btVector3(pos.x, pos.y, pos.z));
  }
}
