#pragma once

#include "../EventManager/EventReceiver.h"
#include "Slots/ESlotPosition.h"
#include "Slots/SlotPosition.h"

#include <set>

namespace AV{
  class StatsEvent;
  class World;

  class ChunkRadiusLoader : public EventReceiver{
  public:
    ChunkRadiusLoader(World *world);
    ~ChunkRadiusLoader();

    bool chunkValid(int chunkX, int chunkY);
    bool eChunkValid(ESlotPosition pos);

    void notifyStatsEvent(StatsEvent &event);
    void notifyWorldEvent(WorldEvent &event);

  private:
    int radius = 100;
    SlotPosition centrePos;
    World *world;
    //std::vector<entityChunkId> loadedChunkIds;
    std::set<int> loadedChunks;

    void checkEChunks();
    void loadEChunk(ESlotPosition EPos);
    void unloadEChunk(ESlotPosition EPos);

    bool _checkRectCircleCollision(int tileX, int tileY, int rectSize, int radius, int circleX, int circleY);

  };
}
