#include "DialogManager.h"

#include <iostream>
#include "../../EventManager/EventManager.h"
#include "../../EventManager/Events/DialogEvent.h"
#include "../../EventManager/Events/InputEvent.h"

namespace AV{
  DialogManager::DialogManager(){
    EventManager::subscribeToEvent(this, EVENT_TYPE_DIALOG);
  }

  DialogManager::~DialogManager(){

  }

  void DialogManager::update(){
  if(!currentBlock) return;
  if(!running) return;
  if(!ready) return;
  tinyxml2::XMLHandle blockHandle(currentBlock);
  //std::cout << currentBlock << '\n';

  tinyxml2::XMLHandle c = blockHandle.FirstChild();
  for(int i = 0; i < blockIndex; i++){
    c = c.NextSibling();
  }

  //There are no elements left in the script, meaning an end has been found.
  if(c.ToElement() == NULL) {

    unloadScript();
    return;
  }
  checkCommand(c);
  //if(!running) return;

  /*for(; c.ToElement() != NULL; c = c.NextSibling()){
    checkCommand(c);
    if(!running) return;
  }*/


  /*tinyxml2::XMLText *t = f.FirstChild().ToText();
  std::cout << t->Value() << '\n';*/
}

void DialogManager::haltExecution(){
  running = false;
}

void DialogManager::unhaltExecution(){
  running = true;
}

bool DialogManager::isHalted(){
  return !running;
}

void DialogManager::checkCommand(tinyxml2::XMLHandle command){
  const char* value = command.ToNode()->Value();
  blockIndex++;

  if(strcmp(value, "text") == 0){
    tinyxml2::XMLText *t = command.FirstChild().ToText();
    //std::cout << t->Value() << '\n';
    DialogEvent d;
    d.dialogText = t->Value();
    d.dialogType = DIALOG_TEXT;
    EventManager::transmitDialogEvent(d);

    haltExecution();
  }else if(strcmp(value, "option") == 0){
    //std::cout << "Option" << '\n';
    processOption(command);
  }else if(strcmp(value, "dialogBlock") == 0){
    int target = command.ToElement()->IntAttribute("id");
    setCurrentBlock(target);
  }else{
    std::cout << "Don't know" << '\n';
  }
}

void DialogManager::processOption(tinyxml2::XMLHandle command){
  std::cout << "Option" << '\n';
  for(tinyxml2::XMLHandle c = command.FirstChild(); c.ToElement() != NULL; c = c.NextSibling()){
    //c.ToElement().Attribute("dialog")
    std::cout << c.ToElement()->Attribute("dialog") << '\n';

    tinyxml2::XMLText *t = c.FirstChild().ToText();
    std::cout << t->Value() << '\n';
  }
}

void DialogManager::setScript(std::string path){
  if(running){
    std::cout << "A dialog script is already loaded." << '\n';
    return;
  }
  std::cout << "Loading dialog script " << path << '\n';

  std::string s = "../resources/dialog/" + path + ".xml";

  std::cout << s << '\n';

  //tinyxml2::XMLDocument xmlDoc;
  //If the file was not loaded successfuly then abort.
  if(xmlDoc.LoadFile(s.c_str()) != tinyxml2::XML_SUCCESS) {
    std::cout << "There was a problem loading that file." << '\n';
    unloadScript();
    return;
  }
  else{
    std::cout << "Success" << '\n';
  }

  //There is no guarantee that a current block is set, so just stop running until one is set.
  running = false;
  ready = false;
  scanFileForBlocks();
  blockIndex = 0;
}

void DialogManager::unloadScript(){
  DialogEvent d;
  d.dialogType = DIALOG_END;
  EventManager::transmitDialogEvent(d);

  std::cout << "Unloading dialog script" << '\n';
  haltExecution();
  dialogBlocks.clear();
  blockIndex = 0;
  currentBlock = 0;
  ready = false;
}

void DialogManager::scanFileForBlocks(){
  tinyxml2::XMLNode *root = xmlDoc.FirstChild();
  if(!root) return;

  for(tinyxml2::XMLElement *e = root->FirstChildElement("dialog"); e != NULL; e = e->NextSiblingElement("dialog")){
    if(e){
      //std::cout << e->Attribute("id") << '\n';

      //Push these to a vector
      dialogBlocks.push_back(e);
    }
  }
}

  void DialogManager::setCurrentBlock(int blockId){
    bool found = false;

    for(int i = 0; i < dialogBlocks.size(); i++){
      if(dialogBlocks[i]->IntAttribute("id") == blockId){
        found = true;
        blockIndex = 0;
        currentBlock = dialogBlocks[i];
        break;
      }
    }


    if(!found){
      std::cout << "No dialog block could be found with the id " << blockId << '\n';
    }else {
      running = true;
      ready = true;
    }
  }

  void DialogManager::notifyDialogEvent(DialogEvent &e){
    if(e.dialogType == DIALOG_NULL) return;

    if(e.dialogType == DIALOG_START){
      std::cout << "Starting dialog." << '\n';
      setScript(e.dialogPath);
      setCurrentBlock(e.startingBlock);
    }else if(e.dialogType == DIALOG_INCREMENT){
      unhaltExecution();
    }else if(e.dialogType == DIALOG_END){
      std::cout << "Unloading dialog script" << '\n';
      haltExecution();
      dialogBlocks.clear();
      blockIndex = 0;
      currentBlock = 0;
      ready = false;
    }
  }

  void DialogManager::notifyInputEvent(InputEvent &e){
    if(e.inputType == KEY_DOWN && e.keyValue == I_ACCEPT){
      if(isHalted()){
        unhaltExecution();
      }
    }
  }
}
