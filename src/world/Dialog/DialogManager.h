#ifndef __DIALOG_MANAGER_H__
#define __DIALOG_MANAGER_H__

#include <vector>
#include "../../util/tinyxml2.h"
#include "../../EventManager/EventReceiver.h"
#include <string>

namespace AV{
  class DialogManager : public EventReceiver{
  public:
    DialogManager();
    ~DialogManager();

    void update();
    void setScript(std::string path);
    void unloadScript();

    void setCurrentBlock(int blockId);

    void notifyDialogEvent(DialogEvent &e);
    void notifyInputEvent(InputEvent &e);

    void haltExecution();
    void unhaltExecution();
    bool isHalted();
  private:
    tinyxml2::XMLDocument xmlDoc;
    tinyxml2::XMLElement *currentBlock;
    int blockIndex = 0;

    void processOption(tinyxml2::XMLHandle command);

    void scanFileForBlocks();
    void checkCommand(tinyxml2::XMLHandle command);

    bool ready = false;
    bool running = false;

    std::vector<tinyxml2::XMLElement*> dialogBlocks;
  };
}

#endif
