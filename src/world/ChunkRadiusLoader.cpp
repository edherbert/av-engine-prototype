#include "ChunkRadiusLoader.h"

#include "../EventManager/Events/StatsEvent.h"
#include "../EventManager/Events/WorldEvent.h"
#include "../EventManager/EventManager.h"

#include "World.h"
#include <iomanip>
#include "../scripting/ScriptManager.h"

namespace AV{
  ChunkRadiusLoader::ChunkRadiusLoader(World *world)
    : world(world) {

    centrePos = SlotPosition(0, 0, Ogre::Vector3::ZERO);

    EventManager::subscribeToEvent(this, EVENT_TYPE_STATS);
    EventManager::subscribeToEvent(this, EVENT_TYPE_WORLD);
  }

  ChunkRadiusLoader::~ChunkRadiusLoader(){

  }

  bool ChunkRadiusLoader::chunkValid(int chunkX, int chunkY){
    return false;
  }

  bool ChunkRadiusLoader::eChunkValid(ESlotPosition pos){
    //I could do a broad phase, but that would be slower.
    //I would need to find the eChunk on the grid so I could check the collision.

    //Circle coords world
    //Chunk coords world and the size.
    //Feed into the algorithm and return result.

    // Ogre::Real circleX = (centrePos.chunkX * CHUNK_SIZE_WORLD) + centrePos.position.x;
    // Ogre::Real circleY = (centrePos.chunkY * CHUNK_SIZE_WORLD) + centrePos.position.z;
    //
    // int eChunkSize = CHUNK_SIZE_WORLD / 3;
    // Ogre::Real chunkX = (pos.chunkX() * CHUNK_SIZE_WORLD) + pos.eChunkX() * eChunkSize;
    // Ogre::Real chunkY = (pos.chunkY() * CHUNK_SIZE_WORLD) + pos.eChunkY() * eChunkSize;
    //
    // return _checkRectCircleCollision(chunkX, chunkY, eChunkSize, radius, circleX, circleY);

    //Ogre::Vector3 centreOgre = centrePos.toOgre();

    int eChunkSize = CHUNK_SIZE_WORLD / 3;
    Ogre::Real circleX = (centrePos.chunkX * CHUNK_SIZE_WORLD) + centrePos.position.x;
    Ogre::Real circleY = (centrePos.chunkY * CHUNK_SIZE_WORLD) + centrePos.position.z;

    return _checkRectCircleCollision(pos.chunkX() * 3 + pos.eChunkX(), pos.chunkY() * 3 + pos.eChunkY(), eChunkSize, radius, circleX, circleY);
  }

  void ChunkRadiusLoader::checkEChunks(){
    int chunk_size = CHUNK_SIZE_WORLD / 3;
    //To start find the centre position of the circle
    Ogre::Real circleX = (centrePos.chunkX * CHUNK_SIZE_WORLD) + centrePos.position.x;
    Ogre::Real circleY = (centrePos.chunkY * CHUNK_SIZE_WORLD) + centrePos.position.z;

    //The coordinates of the circle's rectangle
    Ogre::Real startX = circleX - radius;
    Ogre::Real startY = circleY - radius;
    Ogre::Real endX = circleX + radius;
    Ogre::Real endY = circleY + radius;

    //Find the actual chunk coordinates that lie within the circle's rectangle
    int startXTile = floor(startX / chunk_size);
    int startYTile = floor(startY / chunk_size);
    int endXTile = ceil(endX / chunk_size);
    int endYTile = ceil(endY / chunk_size);


    std::set<int>::iterator z = loadedChunks.begin();
    while(z != loadedChunks.end()){
      ESlotPosition pos(*z);
      //std::cout << pos.getId() << '\n';
      if(!_checkRectCircleCollision((pos.chunkX() * 3) + pos.eChunkX(), (pos.chunkY() * 3) + pos.eChunkY(), chunk_size, radius, circleX, circleY)){
        //If the chunk isn't within the bounds of the circle then it's stale, so remove it.
        //unloadEntityChunk((*z).chunkX, (*z).chunkY, (*z).eChunkX, (*z).eChunkY);
        //std::cout << "Unload" << '\n';
        unloadEChunk(pos);
        loadedChunks.erase(z);
      }
      z++;
    }

    for (int y = startYTile; y < endYTile; y++) {
      for (int x = startXTile; x < endXTile; x++) {
        if(x < 0 || y < 0) continue;
        if(_checkRectCircleCollision(x, y, chunk_size, radius, circleX, circleY)){
          //Place it into a container that represents the eChunk.
          int chunkX = x / 3;
          int chunkY = y / 3;
          //Determine the entity chunks based on the tile coordinates. The left over values represent the entity chunk coordinates
          int eChunkX = x - (chunkX * 3);
          int eChunkY = y - (chunkY * 3);

          ESlotPosition ePos(chunkX, chunkY, eChunkX, eChunkY);
          if(loadedChunks.find(ePos.getId()) == loadedChunks.end()){
            loadEChunk(ePos);
            loadedChunks.insert(ePos.getId());
          }
        }
      }
    }
  }

  void ChunkRadiusLoader::loadEChunk(ESlotPosition EPos){
    std::cout << "Loading eChunk." << EPos.getId() << '\n';

    WorldEvent e;
    e.eventType = WORLD_EVENT_E_CHUNK_LOADED;
    e.eSlot = EPos;
    e.mapName = world->getCurrentMap();

    EventManager::transmitWorldEvent(e);


    std::stringstream ss;
    ss << "../resources/maps/";
    ss << world->getCurrentMap() << "/";
    ss << std::setfill('0') << std::setw(4) << EPos.chunkX() << std::setfill('0') << std::setw(4) << EPos.chunkY();
    ss << "/scripts/" << EPos.chunkNumber() << ".nut";

    ScriptManager::runScript(ss.str().c_str());
  }

  void ChunkRadiusLoader::unloadEChunk(ESlotPosition EPos){
    std::cout << "unloading eChunk. " << EPos.getId() << '\n';

    WorldEvent e;
    e.eventType = WORLD_EVENT_E_CHUNK_UNLOADED;
    e.eSlot = EPos;

    EventManager::transmitWorldEvent(e);
  }

  bool ChunkRadiusLoader::_checkRectCircleCollision(int tileX, int tileY, int rectSize, int radius, int circleX, int circleY){
    int distX = abs(circleX - (tileX * rectSize)-rectSize/2);
    int distY = abs(circleY - (tileY * rectSize)-rectSize/2);

    if(distX > (rectSize / 2 + radius)) return false;
    if(distY > (rectSize / 2 + radius)) return false;

    if(distX <= (rectSize/2)) return true;
    if(distY <= (rectSize/2)) return true;

    int dx = distX - rectSize / 2;
    int dy = distY - rectSize / 2;

    return (dx*dx+dy*dy<=(radius*radius));
  }

  void ChunkRadiusLoader::notifyStatsEvent(StatsEvent &event){
    if(event.statsType != STATS_SLOT_PLAYER_POS) return;
    centrePos = event.slotValue;

    checkEChunks();

    // if(eChunkValid(ESlotPosition(0, 0, 2, 2))){
    //   std::cout << "Its valid" << '\n';
    // }

  }

  void ChunkRadiusLoader::notifyWorldEvent(WorldEvent &event){
    if(event.eventType == WORLD_EVENT_MAP_CHANGED){
      loadedChunks.clear();
    }
  }
}
