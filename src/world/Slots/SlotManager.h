#ifndef __SLOTMANAGER_H__
#define __SLOTMANAGER_H__

#include <string>
#include <vector>

#include "Chunk.h"
#include "SlotPosition.h"

namespace AV{
  struct ChunkData{
    std::string mapName;
    int chunkX;
    int chunkY;
    Chunk *chunk = 0;
  };

  class SlotManager{
  friend SlotPosition;
  public:
    SlotManager();
    ~SlotManager();

    void setCurrentMap(std::string map);
    bool isChunkWithinOrigin(ChunkData chunk);

    void loadChunk(ChunkData chunk);
    bool unloadChunk(ChunkData chunk);
    void setOrigin(SlotPosition slotPosition);

    Ogre::String getCurrentMap() { return currentMap; };

    // void loadChunk(ChunkData chunk);
    // void unloadChunk(ChunkData chunk);
    //
    // void setChunkVisible(ChunkData chunk);
    // void setChunkInvisible(ChunkData chunk);

  private:
    // std::vector<ChunkData> loadedChunks;
    // std::vector<ChunkData> visibleChunks;
    //
    bool isChunkInLists(ChunkData chunk);
    std::string currentMap = "";
    static SlotPosition origin;

    std::vector<ChunkData> mapChunks;
    std::vector<ChunkData> loadedSlot;
  };
}

#endif
