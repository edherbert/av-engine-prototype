#include "ShapeChunk.h"

#include "../../Physics/BulletManager.h"

namespace AV{
  ShapeChunk::ShapeChunk(ESlotPosition pos)
    : slotPosition(pos){

  }

  ShapeChunk::~ShapeChunk(){
    destroyShapes();
  }

  void ShapeChunk::add(btRigidBody *shape){
    shapes.push_back(shape);
  }

  void ShapeChunk::destroyShapes(){
    for(btRigidBody* shape : shapes){
      BulletManager::destroyRigidBody(shape);
    }
  }
}
