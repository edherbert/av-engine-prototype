#pragma once

#include "../ESlotPosition.h"
#include "../../../EventManager/EventReceiver.h"

namespace AV{
  class EntityManager;

  class EntityFileParser : public EventReceiver{
  public:
    EntityFileParser(EntityManager *entityManager);
    ~EntityFileParser();

    void notifyWorldEvent(WorldEvent &event);

  private:
    EntityManager *entityManager;

    void parseFile(ESlotPosition chunk);
  };
}
