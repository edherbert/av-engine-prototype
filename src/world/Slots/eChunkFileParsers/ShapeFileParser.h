#pragma once

#include "../../../EventManager/EventReceiver.h"
#include "../ESlotPosition.h"
#include <OgreString.h>

#include <map>

class btCollisionShape;
class btVector3;

namespace AV{
  class BulletManager;
  class ShapeChunk;

  class ShapeFileParser : public EventReceiver{
  public:
    ShapeFileParser(BulletManager *bulletManager);
    ~ShapeFileParser();

    void notifyWorldEvent(WorldEvent &event);

  private:
    void parseFile(ESlotPosition chunk, Ogre::String &currentMap, ShapeChunk* shapeChunk);
    btCollisionShape* _createShape(std::string &line, btVector3 &scale);

    void loadShapeChunk(ESlotPosition &pos, Ogre::String &currentMap);
    void unloadShapeChunk(ESlotPosition &pos);

    std::map<const int, ShapeChunk*> loadedChunks;
  };
}
