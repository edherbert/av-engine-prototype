#pragma once

#include "../ESlotPosition.h"
#include <vector>

class btRigidBody;

namespace AV{
  class ShapeChunk{
  public:
    ShapeChunk(ESlotPosition pos);
    ~ShapeChunk();

    void add(btRigidBody *shape);

  private:
    ESlotPosition slotPosition;

    void destroyShapes();

    std::vector<btRigidBody*> shapes;
  };
}
