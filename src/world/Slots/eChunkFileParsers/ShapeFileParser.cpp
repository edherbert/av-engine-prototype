#include "ShapeFileParser.h"

#include "../../../EventManager/EventManager.h"
#include "../../../EventManager/Events/WorldEvent.h"

//#include "LinearMath/btVector3.h"
#include "btBulletDynamicsCommon.h"
#include <iomanip>
#include <iostream>
#include "OgreStringConverter.h"
#include "ShapeChunk.h"

#include "../../Physics/BulletManager.h"

namespace AV{
  ShapeFileParser::ShapeFileParser(BulletManager *bulletManager){
    EventManager::subscribeToEvent(this, EVENT_TYPE_WORLD);
  }

  ShapeFileParser::~ShapeFileParser(){

  }

  void ShapeFileParser::parseFile(ESlotPosition chunk, Ogre::String &currentMap, ShapeChunk* shapeChunk){
    std::cout << "Parsing shape file." << '\n';

    std::stringstream ss;
    ss << "../resources/maps/";
    ss << currentMap << "/";
    ss << std::setfill('0') << std::setw(4) << chunk.chunkX() << std::setfill('0') << std::setw(4) << chunk.chunkY();
    ss << "/physics/" << chunk.chunkNumber();
    ss << ".txt";

    std::cout << ss.str() << '\n';

    int indexCount = 0;

    std::string line;
    std::ifstream myfile(ss.str());
    if (myfile.is_open()){
      while(getline (myfile,line)){
          std::cout << line << '\n';

          //TODO there might be a bug here to do with the cretion of the vectors.
          btVector3 position;
          btVector3 rotation;
          btVector3 scale;
          if(indexCount == 0){
            Ogre::Vector3 o = Ogre::StringConverter::parseVector3(line);
            position = btVector3(o.x, o.y, o.z);

            indexCount++;
          }else if(indexCount == 1){
            Ogre::Vector3 o = Ogre::StringConverter::parseVector3(line);
            scale = btVector3(o.x, o.y, o.z);

            indexCount++;
          }else if(indexCount == 2){
            Ogre::Vector3 o = Ogre::StringConverter::parseVector3(line);
            rotation = btVector3(o.x, o.y, o.z);

            indexCount++;
          }else if(indexCount == 3){
            const btScalar mass = 0;
            btVector3 fallInertia(0, 0, 0);
            //btCollisionShape *fallShape = new btBoxShape(btVector3(0.5, 0.5, 0.5));
            btCollisionShape *fallShape = _createShape(line, scale);
            fallShape->calculateLocalInertia(mass, fallInertia);

            btDefaultMotionState *fallMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));

            btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);
            fallRigidBodyCI.m_friction = 1;

            btRigidBody *fallRigidBody = new btRigidBody(fallRigidBodyCI);

            //Set the values here.
            fallRigidBody->getWorldTransform().setOrigin(position);

            btQuaternion quat;
            quat.setEuler(rotation.x(),rotation.y(),rotation.z());
            fallRigidBody->getWorldTransform().setRotation(quat);

            BulletManager::world->addRigidBody(fallRigidBody);

            //Add the shape to the chunk.
            shapeChunk->add(fallRigidBody);
            indexCount = 0;
          }
      }
    }else{
      std::cout << "Unable to open shape file." << '\n';
    }
    myfile.close();
  }

  btCollisionShape* ShapeFileParser::_createShape(std::string &line, btVector3 &scale){
    if(line == "Box"){
      //return new btBoxShape(btVector3(0.5, 0.5, 0.5));
      return new btBoxShape(scale);
    }else if(line == "Sphere"){
      //Do the sphere later!
      //Depending on the scale the values needed might be a bit different.
      //I guess it might not need a rotation variable.

      //return new btSphereShape(1);
      return new btBoxShape(btVector3(0.5, 0.5, 0.5));
    }

    return 0;
  }

  void ShapeFileParser::loadShapeChunk(ESlotPosition &pos, Ogre::String &currentMap){
    ShapeChunk* shapeChunk = new ShapeChunk(pos);
    loadedChunks[pos.getId()] = shapeChunk;

    parseFile(pos, currentMap, shapeChunk);
  }

  void ShapeFileParser::unloadShapeChunk(ESlotPosition &pos){
    ShapeChunk *shapeChunk = loadedChunks[pos.getId()];
    if(shapeChunk != 0){
      //If the shape chunk is found (it should be found ok)
      delete shapeChunk;
      loadedChunks.erase(pos.getId());
    }
  }

  void ShapeFileParser::notifyWorldEvent(WorldEvent &event){
    if(event.eventType == WORLD_EVENT_E_CHUNK_LOADED){
      loadShapeChunk(event.eSlot, event.mapName);
    }else if(event.eventType == WORLD_EVENT_E_CHUNK_UNLOADED){
      unloadShapeChunk(event.eSlot);
    }
  }
}
