#include "EntityFileParser.h"

#include "../../Entity/EntityManager.h"
#include "../../../EventManager/EventManager.h"
#include "../../../EventManager/Events/WorldEvent.h"

#include "../../Entity/components/OgreMeshComponent.h"

#include "OgreStringConverter.h"
#include <iomanip>
#include <iostream>

namespace AV{
  EntityFileParser::EntityFileParser(EntityManager *entityManager)
    : entityManager(entityManager) {

    EventManager::subscribeToEvent(this, EVENT_TYPE_WORLD);
  }

  EntityFileParser::~EntityFileParser(){

  }

  void EntityFileParser::parseFile(ESlotPosition chunk){
    std::cout << "Parsing entity file" << '\n';

    std::stringstream ss;
    ss << "../resources/maps/";
    ss << entityManager->getCurrentMapName() << "/";
    ss << std::setfill('0') << std::setw(4) << chunk.chunkX() << std::setfill('0') << std::setw(4) << chunk.chunkY();
    ss << "/entity/" << chunk.chunkNumber();
    ss << ".txt";


    std::cout << ss.str() << '\n';
    int countIndex = 0;

    std::string line;
    std::ifstream myfile(ss.str());
    if (myfile.is_open())
    {
      entityx::Entity ent;
      //Get the line.
      while ( getline (myfile,line) )
      {

          //position
          //Determine which coordinate we're talking about.
          if(countIndex == 0){
            Ogre::Vector3 v = Ogre::StringConverter::parseVector3(line);
            ent = entityManager->createEntity(chunk.chunkX(), chunk.chunkY(), v);
            ent.assign<OgreMeshComponent>(EntityMeshManager::createOgreMesh("ogrehead2.mesh"));
            countIndex++;

          }else if(countIndex == 1){
            std::cout << line.c_str() << '\n';
            //ent.assign<PlayerDialogComponent>(line.c_str(), 0);
            std::string copy = line;
            //ent.assign<PlayerDialogComponent>(copy.c_str(), 0);

            //countIndex++;
            //entities.push_back(ent);

            countIndex = 0;
          }
      }
    }else std::cout << "Unable to open entity file" << std::endl;
    myfile.close();

  }

  void EntityFileParser::notifyWorldEvent(WorldEvent &event){
    if(event.eventType == WORLD_EVENT_E_CHUNK_LOADED){
      parseFile(event.eSlot);
    }
  }
}
