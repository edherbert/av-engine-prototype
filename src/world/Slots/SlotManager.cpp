#include "SlotManager.h"

#include <iostream>

#include "../../EventManager/Events/StatsEvent.h"
#include "../../EventManager/EventManager.h"
#include "../../EventManager/Events/WorldEvent.h"

#include <Ogre.h>

namespace AV{
  SlotPosition SlotManager::origin = {0, 0, Ogre::Vector3(0, 0, 0)};

  SlotManager::SlotManager(){

  }

  SlotManager::~SlotManager(){

  }

  void SlotManager::setCurrentMap(std::string map){
    this->currentMap = map;

    //Go through and set the visibility of the chunks based on their map.
    //I haven't done the loaded slot becuase that's never supposed to contain visible chunks (even if they're in the active map).
    for(int i = 0; i < mapChunks.size(); i++){
      if(mapChunks[i].mapName == map){
        mapChunks[i].chunk->setVisible(true);
      }else{
        mapChunks[i].chunk->setVisible(false);
      }
    }

    WorldEvent e;
    e.eventType = WORLD_EVENT_MAP_CHANGED;
    e.mapName = map;
    EventManager::transmitWorldEvent(e);
  }

  bool SlotManager::isChunkWithinOrigin(ChunkData chunk){
    //The chunk has to be entirely within the origin.
    //If it's not it's not allowed.

    //Go through the chunk and determine if the points are within the radius.
    //They would need to be as ints.

    //With a size of 8000 I can have precision of half a milimetre.
    //This would mean that I'm allowed a 4/4 chunks in either direction (-4 is also allowed)
    //Any time after that the origin would have to be shifted again.

    const int possibleDistance = 4000;

    bool possible = true;
    for(int y = 0; y < 2; y++){
      for(int x = 0; x < 2; x++){
        int x1 = origin.chunkX * CHUNK_SIZE_WORLD;
        int x2 = (chunk.chunkX + x) * CHUNK_SIZE_WORLD;
        int y1 = origin.chunkY * CHUNK_SIZE_WORLD;
        int y2 = (chunk.chunkY + y) * CHUNK_SIZE_WORLD;
        double distance = sqrt(pow(x2-x1, 2) + pow(y2-y1, 2));

        // std::cout << "Orign x: " << origin.chunkX << " Origin y: " << origin.chunkY << '\n';\
        // std::cout << "x: " << x << "y: " << y << '\n';
        // std::cout << "Distance : " << distance << '\n';
        if(distance > possibleDistance){
          possible = false;
          break;
        }
      }
    }
    return possible;
  }

  void SlotManager::loadChunk(ChunkData chunk){
    //Can't load a negative chunk.
    if(chunk.chunkX < 0 || chunk.chunkY < 0) return;
    //The chunk is already loaded, so don't do anything.
    if(isChunkInLists(chunk)) return;

    //Check if the chunk is within the bounds of the origin.
    if(isChunkWithinOrigin(chunk)){
      std::cout << "Placing chunk in the world" << '\n';
      //If it is then load it into place.
      int posX = chunk.chunkX * CHUNK_SIZE_WORLD;
      int posY = chunk.chunkY * CHUNK_SIZE_WORLD;

      chunk.chunk = new Chunk(chunk.mapName, chunk.chunkX, chunk.chunkY, posX, posY);
      mapChunks.push_back(chunk);

      if(chunk.mapName == currentMap) chunk.chunk->setVisible(true);
      else chunk.chunk->setVisible(false);

      StatsEvent e;
      e.statsType = STATS_MAP_CHUNKS;
      e.chunkVector = &mapChunks;
      EventManager::transmitStatsEvent(e);
    }else{
      std::cout << "Placing chunk in the loaded slot" << '\n';
      chunk.chunk = new Chunk(chunk.mapName, chunk.chunkX, chunk.chunkY, 0, 0);
      loadedSlot.push_back(chunk);

      chunk.chunk->setVisible(false);

      StatsEvent e;
      e.statsType = STATS_LOADED_CHUNKS;
      e.chunkVector = &loadedSlot;
      EventManager::transmitStatsEvent(e);
    }
  }

  bool SlotManager::unloadChunk(ChunkData chunk){
    if(chunk.chunkX < 0 || chunk.chunkY < 0) return false;

    bool found = false;
    std::vector<ChunkData>::iterator z = loadedSlot.begin();
    while(z != loadedSlot.end()){
      if(chunk.chunkX == (*z).chunkX && chunk.chunkY == (*z).chunkY && chunk.mapName == (*z).mapName){
        delete (*z).chunk;
        loadedSlot.erase(z);
        found = true;
        break;
      }else{
        z++;
      }
    }
    //If it was found in the loaded slot don't bother to search the map chunks.
    if(found){
      StatsEvent d;
      d.statsType = STATS_LOADED_CHUNKS;
      d.chunkVector = &loadedSlot;
      EventManager::transmitStatsEvent(d);

      return found;
    }

    std::vector<ChunkData>::iterator i = mapChunks.begin();
    while(i != mapChunks.end()){
      if(chunk.chunkX == (*i).chunkX && chunk.chunkY == (*i).chunkY && chunk.mapName == (*i).mapName){
        delete (*i).chunk;
        mapChunks.erase(i);
        found = true;
        break;
      }else{
        i++;
      }
    }

    if(found){
      StatsEvent e;
      e.statsType = STATS_MAP_CHUNKS;
      e.chunkVector = &mapChunks;
      EventManager::transmitStatsEvent(e);

      return found;
    }
  }

  void SlotManager::setOrigin(SlotPosition origin){
    //If the origin is already set to that then return.
    //if(origin.chunkX == this->origin.chunkX && origin.chunkY == this->origin.chunkY && origin.position == this->origin.position) return;
    if(origin == this->origin) return;

    SlotPosition oldOrigin = this->origin;
    //Set the origin at the end so the previous value can be referenced.
    this->origin = origin;

    StatsEvent e;
    e.statsType = STATS_ORIGIN_POS;
    e.slotValue = origin;
    EventManager::transmitStatsEvent(e);

    std::vector<ChunkData>::iterator i = mapChunks.begin();
    while(i != mapChunks.end()){
      //Traverse the chunks still in the map.
      if(isChunkWithinOrigin(*i)){
        //The chunk is within the origin.
        //The origin will have changed though, which means everything will need to be shifted a bit.

        Ogre::Vector3 diff = oldOrigin.ogreDistanceBetweenSlots(origin);
        std::cout << "Difference: " << diff << '\n';
        (*i).chunk->translate(-diff);
        //sceneManager->setRelativeOrigin(origin.toOgre());

        i++;
      }else{
        //The chunk is now no longer in the origin radius, so it needs to be moved to the loaded slot.
        loadedSlot.push_back({(*i).mapName, (*i).chunkX, (*i).chunkY, (*i).chunk});

        //Move it as well.
        //(*i).chunk->setPosition(Ogre::Vector3())
        //The position is set based on the current origin.
        //I think it's just the intended position minus the origin.

        //The slots need to be offset.
        //This is just a case of taking them away from one another.

        // int offsetX = (*i).chunkX - origin.chunkX;
        // int offsetY = (*i).chunkY - origin.chunkY;
        // Ogre::Vector3 dest = Ogre::Vector3(offsetX * CHUNK_SIZE_WORLD, 0, offsetY * CHUNK_SIZE_WORLD) - origin.position;
        // (*i).chunk->setPosition(dest);
        (*i).chunk->setPosition(Ogre::Vector3::ZERO);


        //Make it invisible.
        (*i).chunk->setVisible(false);

        //Remove it from that list.
        mapChunks.erase(i);
      }

    }

    std::vector<ChunkData>::iterator z = loadedSlot.begin();
    while(z != loadedSlot.end()){
      //The loaded chunk is not part of the current map, so don't bother checking it.
      //if((*z).mapName != this->currentMap) continue;

      //Traverse the chunks in the loaded slot
      //std::cout << "origin " << origin.chunkX << "  " << origin.chunkY << '\n';
      //std::cout << (*i).chunkX << "  " << (*i).chunkY << '\n';
      if(isChunkWithinOrigin(*z)){
        //std::cout << "Inside jfkajskfjklajsdlkfjsadkjflaksjlkj" << '\n';
        //The chunk is now in the radius, so it can be positioned accordingly.
        //Even if it's not in the active map, move it anyway.

        //Add it to the map chunks
        mapChunks.push_back({(*z).mapName, (*z).chunkX, (*z).chunkY, (*z).chunk});

        //move it.

        int offsetX = (*z).chunkX - origin.chunkX;
        int offsetY = (*z).chunkY - origin.chunkY;
        Ogre::Vector3 dest = Ogre::Vector3(offsetX * CHUNK_SIZE_WORLD, 0, offsetY * CHUNK_SIZE_WORLD) - origin.position;
        //Ogre::Vector3 dest = Ogre::Vector3(100, 0, 100);
        //std::cout << "dest " << dest << '\n';
        (*z).chunk->setPosition(dest);
        //(*i).chunk->setPosition(Ogre::Vector3(100, 100, 100));

        //Make it visible if it's in the current map.
        if((*z).mapName == currentMap){
          (*z).chunk->setVisible(true);
          std::cout << "setting to visible." << '\n';
        }

        //Remove it from that list.
        loadedSlot.erase(z);
      }else{
        //The chunk is outside the border, so don't do anything.
        z++;
      }
    }

    StatsEvent f;
    f.statsType = STATS_MAP_CHUNKS;
    f.chunkVector = &mapChunks;
    EventManager::transmitStatsEvent(f);

    StatsEvent d;
    d.statsType = STATS_LOADED_CHUNKS;
    d.chunkVector = &loadedSlot;
    EventManager::transmitStatsEvent(d);
  }

  bool SlotManager::isChunkInLists(ChunkData chunk){
    bool result = false;
    for(ChunkData d : mapChunks){
      if(chunk.chunkX == d.chunkX && chunk.chunkY == d.chunkY && chunk.mapName == d.mapName) result = true;
    }
    for(ChunkData d : loadedSlot){
      if(chunk.chunkX == d.chunkX && chunk.chunkY == d.chunkY && chunk.mapName == d.mapName) result = true;
    }

    return result;
  }


  // bool SlotManager::isChunkInLists(ChunkData chunk){
  //   bool result = false;
  //   for(ChunkData d : loadedChunks){
  //     if(chunk.chunkX == d.chunkX && chunk.chunkY == d.chunkY && chunk.mapName == d.mapName) result = true;
  //   }
  //   for(ChunkData d : visibleChunks){
  //     if(chunk.chunkX == d.chunkX && chunk.chunkY == d.chunkY && chunk.mapName == d.mapName) result = true;
  //   }
  //
  //   return result;
  // }
  //
  // void SlotManager::loadChunk(ChunkData chunk){
  //   if(isChunkInLists(chunk)) return;
  //
  //   chunk.chunk = new Chunk(chunk.mapName, chunk.chunkX, chunk.chunkY);
  //
  //   loadedChunks.push_back(chunk);
  //   std::cout << "Loaded chunk" << '\n';
  // }
  //
  // void SlotManager::unloadChunk(ChunkData chunk){
  //   if(!isChunkInLists(chunk)) return;
  //   ChunkData data = chunk;
  //   for(int c = 0; c < loadedChunks.size(); c++){
  //     ChunkData d = loadedChunks[c];
  //     if(chunk.chunkX == d.chunkX && chunk.chunkY == d.chunkY && chunk.mapName == d.mapName){
  //       data = d;
  //       loadedChunks.erase(loadedChunks.begin() + c);
  //       break;
  //     }
  //   }
  //   for(int c = 0; c < visibleChunks.size(); c++){
  //     ChunkData d = visibleChunks[c];
  //     if(chunk.chunkX == d.chunkX && chunk.chunkY == d.chunkY && chunk.mapName == d.mapName){
  //       data = d;
  //       visibleChunks.erase(visibleChunks.begin() + c);
  //       break;
  //     }
  //   }
  //
  //   delete data.chunk;
  // }
  //
  // //Assume that the chunk is loaded.
  // void SlotManager::setChunkVisible(ChunkData chunk){
  //   bool found = false;
  //   ChunkData d;
  //   for(int c = 0; c < loadedChunks.size(); c++){
  //     d = loadedChunks[c];
  //     if(chunk.chunkX == d.chunkX && chunk.chunkY == d.chunkY && chunk.mapName == d.mapName){
  //       loadedChunks.erase(loadedChunks.begin() + c);
  //       found = true;
  //       break;
  //     }
  //   }
  //   if(!found){
  //     std::cout << "That chunk isn't loaded." << '\n';
  //     return;
  //   }
  //
  //   visibleChunks.push_back(d);
  //
  //   std::cout << visibleChunks.size() << '\n';
  //   std::cout << loadedChunks.size() << '\n';
  //
  //   d.chunk->setVisible(true);
  // }
  //
  // void SlotManager::setChunkInvisible(ChunkData chunk){
  //   bool found = false;
  //   ChunkData d;
  //   for(int c = 0; c < visibleChunks.size(); c++){
  //     d = visibleChunks[c];
  //     if(chunk.chunkX == d.chunkX && chunk.chunkY == d.chunkY && chunk.mapName == d.mapName){
  //       visibleChunks.erase(visibleChunks.begin() + c);
  //       found = true;
  //       break;
  //     }
  //   }
  //   if(!found){
  //     std::cout << "That chunk isn't visible." << '\n';
  //     return;
  //   }
  //
  //   loadedChunks.push_back(d);
  //
  //   std::cout << visibleChunks.size() << '\n';
  //   std::cout << loadedChunks.size() << '\n';
  //
  //   d.chunk->setVisible(false);
  // }
}
