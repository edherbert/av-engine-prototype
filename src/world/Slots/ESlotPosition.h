#pragma once

namespace AV{
  class ESlotPosition{
  public:
    ESlotPosition();
    ESlotPosition(int chunkX, int chunkY, int eChunkX, int eChunkY);
    ESlotPosition(int id);
    ~ESlotPosition();

    bool operator==(const ESlotPosition &pos);

  public:
    int getId() { return _id; };
    int chunkX() { return _chunkX; };
    int chunkY() { return _chunkY; };
    int eChunkX() { return _eChunkX; };
    int eChunkY() { return _eChunkY; };
    int chunkNumber() { return _eChunkX + (_eChunkY * 3); };

  private:
    void _updateId();

    int _id = 0;

    int _chunkX = 0;
    int _chunkY = 0;
    int _eChunkX = 0;
    int _eChunkY = 0;
  };
}
