#include "ESlotPosition.h"

#include <iostream>

namespace AV{
  ESlotPosition::ESlotPosition(){

  }

  ESlotPosition::ESlotPosition(int chunkX, int chunkY, int eChunkX, int eChunkY)
    : _chunkX(chunkX),
      _chunkY(chunkY),
      _eChunkX(eChunkX),
      _eChunkY(eChunkY){

    _updateId();
  }

  ESlotPosition::ESlotPosition(int id){
    int eChunkAmmount = id % 10;

    _eChunkY = eChunkAmmount / 3;
    _eChunkX = eChunkAmmount % 3;

    _chunkY = (id % 100000) / 10;
    _chunkX = (id % 1000000000) / 100000;

    _updateId();

    // std::cout << "chunk: " << eChunkAmmount << '\n';
    // std::cout << "chunk: " << chunkX << '\n';
    // std::cout << "chunk: " << chunkY << '\n';
  }

  ESlotPosition::~ESlotPosition(){

  }

  void ESlotPosition::_updateId(){
    if(_chunkX > 9999) _chunkX = 9999;
    if(_chunkY > 9999) _chunkY = 9999;
    if(_chunkX < 0) _chunkX = 0;
    if(_chunkY < 0) _chunkY = 0;

    if(_eChunkX > 2) _eChunkX = 2;
    if(_eChunkY > 2) _eChunkY = 2;
    if(_eChunkX < 0) _eChunkX = 0;
    if(_eChunkY < 0) _eChunkY = 0;

    int eChunkAmmount = _eChunkX + (3 * _eChunkY);

    //Move it a single order of magnitude.
    int yChunkAmmount = _chunkY * 10;

    //Move it five
    int xChunkAmmount = _chunkX * 100000;

    _id = xChunkAmmount + yChunkAmmount + eChunkAmmount;
    //std::cout << _id << '\n';
  }

  bool ESlotPosition::operator==(const ESlotPosition &pos){
    // if(pos.chunkX == chunkX && pos.chunkY == chunkY && pos.eChunkX == eChunkX && pos.eChunkY == eChunkY) return true;
    // else return false;

    if(pos._id == _id) return true;
    else return false;
  }
}
