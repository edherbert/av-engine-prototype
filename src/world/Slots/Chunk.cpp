#include "Chunk.h"
#include <iostream>
#include <fstream>
#include <string>

#include <Ogre.h>

#include "../World.h"
#include "Terrain/Terrain.h"

#include "../../util/tinyxml2.h"

#include <iomanip>
#include "../../scripting/Script.h"
#include "TArea/TArea.h"

#include "../settings.h"

namespace AV{
  Chunk::Chunk(std::string mapName, int chunkX, int chunkY, int posX, int posY)
    : mapName(mapName),
      chunkX(chunkX),
      chunkY(chunkY){

        setup(posX, posY);
  }

  Chunk::~Chunk(){
    destroy();
  }

  void Chunk::setup(int posX, int posY){
    std::cout << mapName << '\n';
    sceneManager = Ogre::Root::getSingleton().getSceneManager("Scene Manager");

    //staticChunkNode = sceneManager->getRootSceneNode()->createChildSceneNode(Ogre::SCENE_STATIC);
    staticChunkNode = sceneManager->createSceneNode(Ogre::SCENE_STATIC);
    //staticChunkNode->setPosition(chunkX * CHUNK_SIZE_WORLD, 0, chunkY * CHUNK_SIZE_WORLD);
    staticChunkNode->setPosition(Ogre::Vector3(posX, 0, posY));

    sceneManager->getRootSceneNode()->addChild(staticChunkNode);

    //Tell Ogre to update the node, becuause it's static we need to do this ourselves.
    sceneManager->notifyStaticDirty(staticChunkNode);

    //loadStaticMeshFile();
    loadNewMeshFile();
    loadChunkScript();
    loadTAreaFile();

    std::stringstream ss;
    ss << "heightmap";
    ss << std::setfill('0') << std::setw(4) << chunkX << std::setfill('0') << std::setw(4) << chunkY;
    ss << ".png";

    heightMapPath = ss.str();

    if(heightMapPath == ""){
      terrain = new AV::Terrain(mapName, staticChunkNode, posX, posY);
    }else{
      terrain = new AV::Terrain(mapName, staticChunkNode, posX, posY, heightMapPath);
    }

    setVisible(false);
  }

  void Chunk::loadChunkScript(){
    std::stringstream ss;
    ss << "../resources/maps/";
    ss << mapName << "/";
    ss << std::setfill('0') << std::setw(4) << chunkX << std::setfill('0') << std::setw(4) << chunkY;
    ss << "/scripts/chunkScript.nut";
    if(chunkScript == 0){
      chunkScript = new Script();

      std::cout << "Running chunk script for chunk " << std::setfill('0') << std::setw(4) << chunkX << std::setfill('0') << std::setw(4) << chunkY << '\n';
      std::cout << ss.str() << '\n';
      chunkScript->compileFile(ss.str().c_str());
      chunkScript->run();
    }

  }

  void Chunk::setPosition(Ogre::Vector3 pos){
    std::cout << "Setting the position.y" << '\n';
    staticChunkNode->setPosition(pos);

    sceneManager->notifyStaticDirty(staticChunkNode);
    //terrain->notifyStaticDirty();
  }

  void Chunk::translate(Ogre::Vector3 ammount){
    staticChunkNode->translate(ammount);

    sceneManager->notifyStaticDirty(staticChunkNode);
    //terrain->notifyStaticDirty();
    //sceneManager->updateAllTransforms();
    //sceneManager->updateSceneGraph();
  }

  void Chunk::loadTAreaFile(){
    std::stringstream ss;
    ss << "../resources/maps/";
    ss << mapName << "/";
    ss << std::setfill('0') << std::setw(4) << chunkX << std::setfill('0') << std::setw(4) << chunkY;
    ss << "/teleAreas.txt";

    std::cout << ss.str() << '\n';

    std::string basePath = "../resources/maps/tAreas/";

    std::string line;
    std::ifstream myfile(ss.str());
    if (myfile.is_open()){
      while ( getline (myfile,line) ){

        std::cout << line << '\n';
        //Attempt to open the file to see if it actually exists.
        if (FILE *file = fopen((basePath + line + ".txt").c_str(), "r")) {
          fclose(file);

          //The file exists.
          std::cout << "TArea file exists. Creating TArea." << '\n';

          TArea *tArea = new TArea(line, staticChunkNode, sceneManager, mapName);
          tAreas.insert(tArea);
        }else{
          std::cout << "TArea file does not exist." << '\n';
        }

      }
    }
    myfile.close();
  }

  void Chunk::loadEntityFile(){
    // std::stringstream ss;
    // ss << "../resources/entities/";
    // ss << mapName;
    // ss << std::setfill('0') << std::setw(3) << chunkX << std::setfill('0') << std::setw(3) << chunkY;
    // ss << ".xml";
    //
    // tinyxml2::XMLDocument xmlDoc;
    // //If the file was not loaded successfuly then abort.
    // if(xmlDoc.LoadFile(ss.str().c_str()) != tinyxml2::XML_SUCCESS) return;
    //
    // tinyxml2::XMLNode *root = xmlDoc.FirstChild();
    // if(!root) return;
    //
    // for(tinyxml2::XMLElement *e = staticContent->FirstChildElement("entity"); e!= NULL; e = e->NextSiblingElement("entity")){
    //
    // }
  }

  void Chunk::loadNewMeshFile(){
    std::stringstream ss;
    ss << "../resources/maps/";
    ss << mapName << "/";
    ss << std::setfill('0') << std::setw(4) << chunkX << std::setfill('0') << std::setw(4) << chunkY;
    ss << "/meshes/1.txt";

    std::cout << ss.str() << '\n';

    int countIndex = 0;

    Ogre::SceneNode *newNode = staticChunkNode->createChildSceneNode(Ogre::SCENE_STATIC);
    //newNode->setPosition(staticContent->FloatAttribute("x"), staticContent->FloatAttribute("y"), staticContent->FloatAttribute("z"));

    std::string line;
    std::ifstream myfile(ss.str());
    if (myfile.is_open())
    {
      //Get the line.
      while ( getline (myfile,line) )
      {
          //std::cout << line << '\n';
          Ogre::Vector3 v = Ogre::StringConverter::parseVector3(line);
          //std::cout << v << '\n';

          //position
          //Determine which coordinate we're talking about.
          if(countIndex == 0){
            std::cout << v << '\n';
            newNode->setPosition(v);
            countIndex++;
          }else if(countIndex == 1){
            //Scale
            newNode->setScale(v);
            countIndex++;
          }else if(countIndex == 2){
            //rotation
            countIndex++;
          }else if(countIndex == 3){
            Ogre::Item *item = sceneManager->createItem("cube.mesh", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_STATIC);
            newNode->attachObject((Ogre::MovableObject*)item);

            sceneManager->notifyStaticDirty(newNode);
            newNode = staticChunkNode->createChildSceneNode(Ogre::SCENE_STATIC);
            countIndex = 0;
          }
      }
      myfile.close();
    }

    else std::cout << "Unable to open file";

  }

  void Chunk::loadStaticMeshFile(){
    std::stringstream ss;
    ss << "../resources/chunks/";
    ss << mapName;
    ss << std::setfill('0') << std::setw(3) << chunkX << std::setfill('0') << std::setw(3) << chunkY;
    ss << ".xml";

    tinyxml2::XMLDocument xmlDoc;
    //If the file was not loaded successfuly then abort.
    if(xmlDoc.LoadFile(ss.str().c_str()) != tinyxml2::XML_SUCCESS) return;

    tinyxml2::XMLNode *root = xmlDoc.FirstChild();
    if(!root) return;

    tinyxml2::XMLElement *header = root->FirstChildElement("Header");

    //tinyxml2::XMLElement *mapSettings = header->FirstChildElement("chunkData");
    tinyxml2::XMLElement *mapSettings = header->FirstChildElement("map");
    heightMapPath = mapSettings->Attribute("heightMap");

    tinyxml2::XMLElement *staticContent = root->FirstChildElement("item");
    if(staticContent){
      parseStaticItems(staticContent, staticChunkNode);
    }
  }

  void Chunk::destroy(){
    delete terrain;
    delete staticChunkNode;
  }

  void Chunk::setVisible(bool visible){
    staticChunkNode->setVisible(visible);

    //For now I'll assume that if the chunk is visible it's ok to create the shapes.
    for(TArea* t : tAreas){
      if(visible) t->activateShapes();
      else t->deactivateShapes();
    }
  }

  void Chunk::parseStaticItems(tinyxml2::XMLElement *staticContent, Ogre::SceneNode *node){
    Ogre::SceneNode *newNode = node->createChildSceneNode(Ogre::SCENE_STATIC);
    newNode->setPosition(staticContent->FloatAttribute("x"), staticContent->FloatAttribute("y"), staticContent->FloatAttribute("z"));
    sceneManager->notifyStaticDirty(newNode);

    if(staticContent->Attribute("model") != NULL){
      //If the item specifies a model then add it to the node.
      Ogre::Item *item = sceneManager->createItem(staticContent->Attribute("model"), Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_STATIC);
      newNode->attachObject((Ogre::MovableObject*)item);
    }

    for(tinyxml2::XMLElement *e = staticContent->FirstChildElement("item"); e!= NULL; e = e->NextSiblingElement("item")){
      if(e->FirstChildElement("item") != NULL){
        //The item has children so go over it.
        parseStaticItems(e, newNode);
      }
    }
  }
}
