#include "SlotPosition.h"

#include "SlotManager.h"

namespace AV{
  SlotPosition::SlotPosition()
    : chunkX(0),
      chunkY(0),
      position(Ogre::Vector3::ZERO){

  }

  SlotPosition::SlotPosition(int chunkX, int chunkY, Ogre::Vector3 position)
    : chunkX(chunkX),
      chunkY(chunkY),
      position(position){

  }

  SlotPosition::SlotPosition(Ogre::Vector3 vector){
  }

  ESlotPosition SlotPosition::toESlot() const{
    int eChunkSize = CHUNK_SIZE_WORLD / 3;
    ESlotPosition ePos(chunkX, chunkY, position.x / eChunkSize, position.z / eChunkSize);

    return ePos;
  }

  //TODO in the actual version, there should be two of these.
  //One that translates relative to the origin and one that doesn't.
  //Translating relative to the origin can lead to some unecessary overhead in certain situations.
  SlotPosition::SlotPosition(btVector3 v){
    //At the moment I'm not converting relative to the origin because it's slower.
    //This is only used for collision in the physics engine, and that's always relative to 0 because it's shifted somewhere else.
    //So to avoid wasted resources don't take the origin into account.

    // Ogre::Real chunkX = SlotManager::origin.position.x + SlotManager::origin.chunkX * CHUNK_SIZE_WORLD;
    // Ogre::Real chunkY = SlotManager::origin.position.z + SlotManager::origin.chunkY * CHUNK_SIZE_WORLD;

    int locChunkX = v.x() / CHUNK_SIZE_WORLD;
    int locChunkY = v.z() / CHUNK_SIZE_WORLD;

    chunkX = SlotManager::origin.chunkX - locChunkX;
    chunkY = SlotManager::origin.chunkY - locChunkY;

    Ogre::Real diffX = v.x() - locChunkX * CHUNK_SIZE_WORLD;
    Ogre::Real diffY = v.z() - locChunkX * CHUNK_SIZE_WORLD;
    position = Ogre::Vector3(diffX, v.y(), diffY) - SlotManager::origin.position;

    // chunkX = vector.x() / CHUNK_SIZE_WORLD;
    // chunkY = vector.z() / CHUNK_SIZE_WORLD;
    // position = Ogre::Vector3(vector.x() - (chunkX * CHUNK_SIZE_WORLD), vector.y(), vector.z() - (chunkY * CHUNK_SIZE_WORLD));
  }

  Ogre::Vector3 SlotPosition::toOgre(){
    int offsetX = chunkX - SlotManager::origin.chunkX;
    int offsetY = chunkY - SlotManager::origin.chunkY;
    Ogre::Vector3 posDiff = position - SlotManager::origin.position;
    Ogre::Vector3 dest = Ogre::Vector3(offsetX * CHUNK_SIZE_WORLD, 0, offsetY * CHUNK_SIZE_WORLD) + posDiff;

    return dest;

    //return Ogre::Vector3(chunkX * CHUNK_SIZE_WORLD + position.x, position.y, chunkY * CHUNK_SIZE_WORLD + position.z);
  }

  btVector3 SlotPosition::toBullet(){
    int offsetX = chunkX - SlotManager::origin.chunkX;
    int offsetY = chunkY - SlotManager::origin.chunkY;
    Ogre::Vector3 posDiff = position - SlotManager::origin.position;
    //Ogre::Vector3 dest = Ogre::Vector3(offsetX * CHUNK_SIZE_WORLD, 0, offsetY * CHUNK_SIZE_WORLD) + posDiff;
    btVector3 dest = btVector3(offsetX * CHUNK_SIZE_WORLD, 0, offsetY * CHUNK_SIZE_WORLD) + btVector3(posDiff.x, posDiff.y, posDiff.z);

    return dest;
  }

  bool SlotPosition::operator==(const SlotPosition &pos){
    if(pos.chunkX == chunkX && pos.chunkY == chunkY && pos.position == position) return true;
    else return false;
  }

  //This could lead to precision problems if the difference between the positions is too large.
  Ogre::Vector3 SlotPosition::ogreDistanceBetweenSlots(SlotPosition second){
    int diffx = second.chunkX - chunkX;
    int diffy = second.chunkY - chunkY;
    Ogre::Vector3 posDiff = second.position - position;

    Ogre::Vector3 done = Ogre::Vector3(diffx * CHUNK_SIZE_WORLD + posDiff.x, 0, diffy * CHUNK_SIZE_WORLD + posDiff.z);
    return done;
  }

  void SlotPosition::move(const Ogre::Vector3 &ammount){
        //If the ammount is greater than the chunksize it will abort.
        if(ammount.x >= CHUNK_SIZE_WORLD || ammount.z >= CHUNK_SIZE_WORLD){
          return;
        }

        Ogre::Vector3 pos = position;
        pos += ammount;
        position = pos;

        bool chunkSwitch = false;
        if(pos.x < 0){
          chunkX -= 1;
          position.x = CHUNK_SIZE_WORLD + pos.x;

          chunkSwitch = true;
        }
        if(pos.z < 0){
          chunkY -= 1;
          position.z = CHUNK_SIZE_WORLD + pos.z;

          chunkSwitch = true;
        }

        if(pos.x >= CHUNK_SIZE_WORLD){
          chunkX += 1;

          position.x = pos.x - CHUNK_SIZE_WORLD;

          chunkSwitch = true;
        }

        if(pos.z >= CHUNK_SIZE_WORLD){
          chunkY += 1;

          position.z = pos.z - CHUNK_SIZE_WORLD;

          chunkSwitch = true;
        }
  }
}
