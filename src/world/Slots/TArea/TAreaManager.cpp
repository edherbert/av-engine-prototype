#include "TAreaManager.h"

#include "../../../EventManager/EventManager.h"
#include "../../../EventManager/Events/TriggerEvent.h"
#include "../../World.h"
#include "TArea.h"
#include <iostream>

namespace AV{
  TAreaManager::TAreaManager(World *world)
    : world(world) {
    EventManager::subscribeToEvent(this, EVENT_TYPE_TRIGGER);
  }

  TAreaManager::~TAreaManager(){

  }

  void TAreaManager::notifyTriggerEvent(TriggerEvent &e){
    std::cout << "Trigger Event" << '\n';
    if(e.tarea == 0) return;

    if(e.type == TRIGGER_EVENT_TAREA_BOX){
      if(!currentTArea){
        currentTArea = e.tarea;
      }
    }

    if(e.type == TRIGGER_EVENT_TAREA_OUTER_BOX){
      if(!e.triggerEntered) currentTArea = 0;
    }

    if(e.type == TRIGGER_EVENT_TAREA_TRIGGER){
      std::cout << "Trigger" << '\n';
      std::cout << "" << '\n';
      std::cout << "" << '\n';

      if(currentTArea && currentTArea == e.tarea){
        std::cout << "Moving" << '\n';
        const TAreaData anchor = currentTArea->getDestinationAnchor();

        std::cout << anchor.pos.chunkX << '\n';
        std::cout << anchor.pos.chunkY << '\n';
        std::cout << anchor.pos.position << '\n';
        std::cout << anchor.mapName << '\n';

        world->teleportPlayer(anchor.pos.chunkX, anchor.pos.chunkY, anchor.pos.position, anchor.mapName);
      }
    }
  }
}
