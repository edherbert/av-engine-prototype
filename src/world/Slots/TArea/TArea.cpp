#include "TArea.h"

#include <OgreStringConverter.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

#include "../../Physics/BulletManager.h"

#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "BulletCollision/CollisionDispatch/btCollisionObject.h"

namespace AV{
  TArea::TArea(Ogre::String id, Ogre::SceneNode *chunkNode, Ogre::SceneManager *sceneManager, Ogre::String mapName)
    : id(id),
      sceneManager(sceneManager),
      mapName(mapName) {

      areaNode = chunkNode->createChildSceneNode(Ogre::SCENE_STATIC);

      parseFile();

      std::cout << "Target A for " << mapName << ": " << targetA << '\n';

      _setAreaNodePosition(getTargetAnchor());
  }

  TArea::~TArea(){

  }

  void TArea::parseFile(){
    std::string line;
    std::ifstream myfile("../resources/maps/tAreas/" + id + ".txt");
    std::cout << "../resources/maps/tAreas/" + id << '\n';

    std::cout << "Parsing file" << '\n';

    int count = -1;
    if(myfile.is_open()){
      Ogre::Vector3 pos;
      Ogre::Vector3 scale;
      Ogre::Vector3 rotation;

      Ogre::Vector3 triggerPos;
      Ogre::Vector3 triggerScale;
      while (getline(myfile,line)){
        std::cout << line << '\n';
        if(count == -1){
          //Parse anchor A
          _parseAnchor(line, anchorA);
          count--;
        }
        else if(count == -2){
          //Get the position of anchor A
          anchorA.tAreaAnchorPosition = Ogre::StringConverter::parseVector3(line);
          count--;
        }
        else if(count == -3){
          //Parse Anchor B
          _parseAnchor(line, anchorB);
          count--;
        }
        else if(count == -4){
          //Get the position of anchor B
          anchorB.tAreaAnchorPosition = Ogre::StringConverter::parseVector3(line);

          //Now that both anchors are parsed, determine the target map.
          _determineTargetMap();
          count--;
        }
        else if(count == -5){
          //Get the position of the bounding box
          bbPos = Ogre::StringConverter::parseVector3(line);
          count--;
        }
        else if(count == -6){
          //Get the scale of the bounding box.
          bbScale = Ogre::StringConverter::parseVector3(line);
          _createBoundingBox();
          count--;
        }
        else if(count == -7){
          triggerPos = Ogre::StringConverter::parseVector3(line);
          count--;
        }
        else if(count == -8){
          triggerScale = Ogre::StringConverter::parseVector3(line);
          std::cout << "SCALE: " << triggerScale << '\n';
          _createTrigger(triggerPos, triggerScale, true);
          count--;
        }
        else if(count == -9){
          triggerPos = Ogre::StringConverter::parseVector3(line);
          count--;
        }
        else if(count == -10){
          triggerScale = Ogre::StringConverter::parseVector3(line);
          _createTrigger(triggerPos, triggerScale, false);

          //Read another line for the space.
          getline(myfile,line);
          count = 0;
        }

        else if(count == 0){
          pos = Ogre::StringConverter::parseVector3(line);
          count++;
        }
        else if(count == 1){
          scale = Ogre::StringConverter::parseVector3(line);
          count++;
        }
        else if(count == 2){
          rotation = Ogre::StringConverter::parseVector3(line);
          count++;
        }
        else if(count == 3){
          Ogre::Item *item = sceneManager->createItem(line, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_STATIC);
          Ogre::SceneNode* newNode = areaNode->createChildSceneNode(Ogre::SCENE_STATIC);
          newNode->attachObject((Ogre::MovableObject*)item);

          newNode->setPosition(pos);
          //newNode->setOrientation(rotation);
          //newNode->setOrientation(Ogre::Quaternion(Ogre::Degree))
          newNode->setScale(scale);

          sceneManager->notifyStaticDirty(newNode);
          count = 0;
        }
      }
    }
    myfile.close();
  }

  Ogre::Vector3 TArea::getOrigin(){
    //For now say that anchor A will always be the origin

    const TAreaData &d = getTargetAnchor();
    //const TAreaData &d = getAnchorA();

    SlotPosition pos = d.pos;
    //pos.move()
    return pos.toOgre();
  }

  void TArea::activateShapes(){
    BulletManager::setObjectOrigin(bbPos + getOrigin(), boundingBox);
    BulletManager::setObjectOrigin(bbPos + getOrigin(), outerBoundingBox);
    BulletManager::setObjectOrigin(trigger.pos + getOrigin(), areaTrigger);

    //areaNode->setPosition(data.pos.position - data.tAreaAnchorPosition);

    BulletManager::addCollisionObject(boundingBox);
    BulletManager::addCollisionObject(outerBoundingBox);
    BulletManager::addCollisionObject(areaTrigger);
  }

  void TArea::deactivateShapes(){
    BulletManager::removeCollisionObject(boundingBox);
    BulletManager::removeCollisionObject(outerBoundingBox);
    BulletManager::removeCollisionObject(areaTrigger);
  }

  void TArea::_determineTargetMap(){
    //The target represents the anchor of the current map.
    if(anchorA.mapName == mapName) targetA = true;
    else if(anchorB.mapName == mapName) targetA = false;
  }

  void TArea::_createBoundingBox(){
    btBoxShape *shape = new btBoxShape(btVector3(bbScale.x, bbScale.y, bbScale.z));
    btBoxShape *outerShape = new btBoxShape(btVector3(bbScale.x, bbScale.y, bbScale.z) * 1.5);

    boundingBox = BulletManager::createCollisionObject(bbPos + getOrigin(), COLLISION_OBJECT_TYPE_TAREA_BOX, shape, false);
    outerBoundingBox = BulletManager::createCollisionObject(bbPos + getOrigin(), COLLISION_OBJECT_TYPE_TAREA_OUTER_BOX, outerShape, false);

    BulletManager::setTAreaPointer(boundingBox, this);
    BulletManager::setTAreaPointer(outerBoundingBox, this);
  }

  void TArea::_createTrigger(Ogre::Vector3 triggerPos, Ogre::Vector3 triggerScale, bool triggerA){
    //The system needs to produce the trigger of the opposite map
    if(triggerA == targetA) return;

    trigger.pos = triggerPos;
    trigger.scale = triggerScale;

    std::cout << "Producing trigger " << triggerA << '\n';

    btVector3 temp = btVector3(triggerScale.x, triggerScale.y, triggerScale.z);
    btBoxShape *shape = new btBoxShape(temp);

    areaTrigger = BulletManager::createCollisionObject(triggerPos + getOrigin(), COLLISION_OBJECT_TYPE_TAREA_TRIGGER, shape, false);

    BulletManager::setTAreaPointer(areaTrigger, this);

  }

  void TArea::_parseAnchor(std::string &line, TAreaData &target){
    std::cout << "Parsing Anchor" << '\n';
    Ogre::String s = Ogre::String(line);

    Ogre::vector<Ogre::String>::type vec = Ogre::StringUtil::split(s);

    int chunkX = std::stoi(vec[2]);
    int chunkY = std::stoi(vec[3]);

    Ogre::Real x = Ogre::StringConverter::parseReal(vec[4]);
    Ogre::Real y = Ogre::StringConverter::parseReal(vec[5]);
    Ogre::Real z = Ogre::StringConverter::parseReal(vec[6]);

    target.pos = SlotPosition(chunkX, chunkY, Ogre::Vector3(x, y, z));
    target.mapName = vec[1];
  }

  void TArea::_setAreaNodePosition(const TAreaData &data){
    //areaNode->setPosition(data.pos.toOgre() - data.tAreaAnchorPosition);
    areaNode->setPosition(data.pos.position - data.tAreaAnchorPosition);
  }

  const TAreaData& TArea::getTargetAnchor(){
    if(!targetA) return anchorB;
    else return anchorA;
  }

  const TAreaData& TArea::getDestinationAnchor(){
    if(targetA) return anchorB;
    else return anchorA;
  }
}
