#pragma once

#include "../../../EventManager/EventReceiver.h"

namespace AV{
  class TArea;
  class World;

  class TAreaManager : public EventReceiver{
  public:
    TAreaManager(World *world);
    ~TAreaManager();

    void notifyTriggerEvent(TriggerEvent &e);

  private:
    TArea *currentTArea = 0;
    World *world;
  };
}
