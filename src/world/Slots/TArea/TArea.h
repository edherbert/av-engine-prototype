#pragma once

#include <OgreString.h>
#include "../SlotPosition.h"

class btCollisionObject;

namespace Ogre{
  class SceneNode;
  class SceneManager;
}

namespace AV{
  struct TAreaData{
    SlotPosition pos;
    Ogre::String mapName;
    Ogre::Vector3 tAreaAnchorPosition;
  };

  struct TAreaTriggerData{
    Ogre::Vector3 pos;
    Ogre::Vector3 scale;
  };

  class TArea{
  public:
    TArea(Ogre::String id, Ogre::SceneNode *chunkNode, Ogre::SceneManager *sceneManager, Ogre::String mapName);
    ~TArea();

    const TAreaData& getAnchorA() { return anchorA; };
    const TAreaData& getAnchorB() { return anchorB; };
    const TAreaData& getTargetAnchor();
    const TAreaData& getDestinationAnchor();

    void activateShapes();
    void deactivateShapes();

    Ogre::Vector3 getOrigin();

  private:
    Ogre::String id;
    Ogre::SceneNode *areaNode;
    Ogre::SceneManager *sceneManager;
    Ogre::String mapName;

    Ogre::Vector3 bbPos;
    Ogre::Vector3 bbScale;

    void _createBoundingBox();
    void _createTrigger(Ogre::Vector3 triggerPos, Ogre::Vector3 triggerScale, bool triggerA);
    void _determineTargetMap();

    TAreaData anchorA;
    TAreaData anchorB;
    bool targetA = true;

    TAreaTriggerData trigger;

    btCollisionObject *boundingBox = 0;
    btCollisionObject *outerBoundingBox = 0;
    btCollisionObject *areaTrigger = 0;

    void parseFile();
    void _parseAnchor(std::string &line, TAreaData &target);
    void _setAreaNodePosition(const TAreaData &data);
  };
}
