#include "TerrainCell.h"

#include <OgreMeshManager2.h>
#include <OgreMesh2.h>
#include <OgreItem.h>
#include "OgreSubMesh2.h"

#include "Ogre.h"

#include "Terrain.h"

namespace AV{
  TerrainCell::TerrainCell(std::string mapName, Ogre::SceneNode *terrainNode, float (&heightData)[TERRAIN_RESOLUTION * TERRAIN_RESOLUTION], unsigned int cellX, unsigned int cellY, unsigned int terrainX, unsigned int terrainY)
    : mapName(mapName),
      terrainNode(terrainNode),
      cellX(cellX),
      cellY(cellY),
      terrainX(terrainX),
      terrainY(terrainY){

      Ogre::MeshPtr staticMesh = createStaticMesh(mapName, heightData);

      //Ogre::SceneNode *cellNode = terrainNode->createChildSceneNode(Ogre::SCENE_STATIC);
      //cellItem = Ogre::Root::getSingleton().getSceneManager("Scene Manager")->createItem(staticMesh, Ogre::SCENE_STATIC);
      cellItem = Ogre::Root::getSingleton().getSceneManager("Scene Manager")->createItem(staticMesh, Ogre::SCENE_DYNAMIC);
      terrainNode->attachObject((Ogre::MovableObject*)cellItem);

      cellItem->setLocalAabb(Ogre::Aabb(Ogre::Vector3(cellSize / 2, 0, cellSize / 2), Ogre::Vector3(cellSize / 2, 10, cellSize / 2)));
      //cellItem->setLocalAabb(Ogre::Aabb(Ogre::Vector3(cellSize / 4, 0, cellSize / 4), Ogre::Vector3(cellSize / 4, 10, cellSize / 4)));

      //cellItem->setDatablock("terrain2");
      cellItem->setDatablock("Grass");
      //cellItem->setDatablock("terrainGrass");
      //cellItem->setDatablock("aluminium");
      //cellItem->setDatablock("TerraExampleMaterial");
  }

  void TerrainCell::setDatablock(Ogre::HlmsDatablock* datablock){
    cellItem->setDatablock(datablock);
  }

  void TerrainCell::notifyStaticDirty(){
    //Ogre::Root::getSingleton().getSceneManager("Scene Manager")->notifyStaticDirty(terrainNode);
  }

  TerrainCell::~TerrainCell(){
    Ogre::RenderSystem *renderSystem = Ogre::Root::getSingletonPtr()->getRenderSystem();
    Ogre::VaoManager *vaoManager = renderSystem->getVaoManager();

    terrainNode->detachObject((Ogre::MovableObject*)cellItem);
    Ogre::Root::getSingleton().getSceneManager("Scene Manager")->destroyItem(cellItem);
    Ogre::String name = "TerrainCell" + mapName + Ogre::StringConverter::toString(terrainX) + Ogre::StringConverter::toString(terrainY) + Ogre::StringConverter::toString(cellX) + Ogre::StringConverter::toString(cellY);
    Ogre::MeshManager::getSingleton().remove(name);

    /*if(vao){
      //Go through the vao and delete all the buffers
      const Ogre::VertexBufferPackedVec &vertexBuffers = vao->getVertexBuffers();
      Ogre::VertexBufferPackedVec::const_iterator itBuffers = vertexBuffers.begin();
      Ogre::VertexBufferPackedVec::const_iterator enBuffers = vertexBuffers.end();
      while( itBuffers != enBuffers ){
          vaoManager->destroyVertexBuffer( *itBuffers );
          ++itBuffers;
      }
      if(vao->getIndexBuffer()){
        vaoManager->destroyIndexBuffer(vao->getIndexBuffer());
      }
      vaoManager->destroyVertexArrayObject(vao);
    }*/
  }

  Ogre::IndexBufferPacked* TerrainCell::createIndexBuffer(){
      Ogre::IndexBufferPacked *indexBuffer = 0;

      int arraySize = 6 * CELL_RESOLUTION * CELL_RESOLUTION;

      Ogre::uint16 c_indexData[arraySize];

      for(int i = 0; i < arraySize; i++){
          c_indexData[i] = 0;
      }

      int currentStart = 0;
      for(int y = 0; y < CELL_RESOLUTION; y++){
          for(int x = 0; x < CELL_RESOLUTION; x++){
              c_indexData[currentStart + 0] = x + y * (CELL_RESOLUTION + 1);
              c_indexData[currentStart + 1] = x + (y + 1) * (CELL_RESOLUTION + 1);
              c_indexData[currentStart + 2] = 1 + x + (y + 1) * (CELL_RESOLUTION + 1);
              c_indexData[currentStart + 3] = 1 + x + (y + 1) * (CELL_RESOLUTION + 1);
              c_indexData[currentStart + 4] = 1 + x + y * (CELL_RESOLUTION + 1);
              c_indexData[currentStart + 5] = x + y * (CELL_RESOLUTION + 1);

              currentStart += 6;
          }
      }

      Ogre::uint16 *cubeIndices = reinterpret_cast<Ogre::uint16*>( OGRE_MALLOC_SIMD(sizeof(Ogre::uint16) * arraySize, Ogre::MEMCATEGORY_GEOMETRY));
      memcpy( cubeIndices, c_indexData, sizeof( c_indexData ) );

      Ogre::RenderSystem *renderSystem = Ogre::Root::getSingletonPtr()->getRenderSystem();
      Ogre::VaoManager *vaoManager = renderSystem->getVaoManager();

      try
      {
  	//Actually create an index buffer and assign it to the pointer created earlier.
  	//Also populate the index buffer with these values.
  	//This goes, type, number of indices, Buffer type, the actual data, keep as shadow
          indexBuffer = vaoManager->createIndexBuffer(Ogre::IndexBufferPacked::IT_16BIT, arraySize, Ogre::BT_IMMUTABLE, cubeIndices, true);
      }
      catch( Ogre::Exception &e )
      {
          OGRE_FREE_SIMD( indexBuffer, Ogre::MEMCATEGORY_GEOMETRY );
          indexBuffer = 0;
          throw e;
      }

//Delete this
      return indexBuffer;
  }

  Ogre::Vector3 TerrainCell::calculateNormal(int x, int y, float (&heightData)[TERRAIN_RESOLUTION * TERRAIN_RESOLUTION]){
    Ogre::Vector3 finished;

    //Get the index and check to see if it's within the bounds.
    int leftIndex = (TERRAIN_RESOLUTION - terrainX * CELL_RESOLUTION) - (x+1) + ((TERRAIN_RESOLUTION - terrainY * CELL_RESOLUTION) - y) * TERRAIN_RESOLUTION;
    int rightIndex = (TERRAIN_RESOLUTION - terrainX * CELL_RESOLUTION) - (x-1) + ((TERRAIN_RESOLUTION - terrainY * CELL_RESOLUTION) - y) * TERRAIN_RESOLUTION;
    int upIndex = (TERRAIN_RESOLUTION - terrainX * CELL_RESOLUTION) - x + ((TERRAIN_RESOLUTION - terrainY * CELL_RESOLUTION) - (y+1)) * TERRAIN_RESOLUTION;
    int downIndex = (TERRAIN_RESOLUTION - terrainX * CELL_RESOLUTION) - x + ((TERRAIN_RESOLUTION - terrainY * CELL_RESOLUTION) - (y-1)) * TERRAIN_RESOLUTION;

    int total = TERRAIN_RESOLUTION * TERRAIN_RESOLUTION;

    if(leftIndex >= total) leftIndex = total - 1;
    if(rightIndex >= total) rightIndex = total - 1;
    if(upIndex >= total) upIndex = total - 1;
    if(downIndex >= total) downIndex = total - 1;

    float hLeft = heightData[leftIndex] * 10;
    float hRight = heightData[rightIndex] * 10;
    float hUp = heightData[upIndex] * 10;
    float hDown = heightData[downIndex] * 10;


    finished = Ogre::Vector3(hLeft-hRight, 2.0f, hDown-hUp);
    finished.normalise();

    return finished;
  }

  Ogre::MeshPtr TerrainCell::createStaticMesh(std::string mapName, float (&heightData)[TERRAIN_RESOLUTION * TERRAIN_RESOLUTION]){
      Ogre::RenderSystem *renderSystem = Ogre::Root::getSingletonPtr()->getRenderSystem();
      Ogre::VaoManager *vaoManager = renderSystem->getVaoManager();

      Ogre::String name = "TerrainCell" + mapName + Ogre::StringConverter::toString(terrainX) + Ogre::StringConverter::toString(terrainY) + Ogre::StringConverter::toString(cellX) + Ogre::StringConverter::toString(cellY);
      Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().createManual(name, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
      Ogre::SubMesh *subMesh = mesh->createSubMesh();

      Ogre::VertexElement2Vec vertexElements;
      vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_POSITION));
      vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_NORMAL));
      vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT2, Ogre::VES_TEXTURE_COORDINATES));

      //Create the verticies array but don't put anything in it.
      int count = (CELL_RESOLUTION + 1) * (CELL_RESOLUTION + 1);
      CubeVertices c_originalVertices[count];

      CubeVertices *cubeVertices = reinterpret_cast<CubeVertices*>( OGRE_MALLOC_SIMD(sizeof(CubeVertices) * count, Ogre::MEMCATEGORY_GEOMETRY ) );

      //Put the vertices in. Just walk through the grid adding them.
      int arrayCount = 0;
      for(int y = 0; y < CELL_RESOLUTION + 1; y++){
          for(int x = 0; x < CELL_RESOLUTION + 1; x++){
      //for(int y = CELL_RESOLUTION + 1; y > 0; y--){
          //for(int x = CELL_RESOLUTION + 1; x > 0; x--){
              //float value = 0;
              //float value = (rand() % 100) * 0.01;
              //float value = cellX
              //float value = heightData[(cellX * CELL_RESOLUTION) + x + ((cellY * CELL_RESOLUTION) + y) * TERRAIN_RESOLUTION];
              //float value = heightData[(terrainX * CELL_RESOLUTION) + x + ((terrainY * CELL_RESOLUTION) + y) * TERRAIN_RESOLUTION];
              //float value = heightData[x + y * TERRAIN_RESOLUTION];
              //float value = heightData[];
              //float value = heightData[(CELL_RESOLUTION - terrainX * CELL_RESOLUTION) - x - ((CELL_RESOLUTION - terrainY * CELL_RESOLUTION) - y) * TERRAIN_RESOLUTION];
              float value = heightData[(TERRAIN_RESOLUTION - terrainX * CELL_RESOLUTION) - x + ((TERRAIN_RESOLUTION - terrainY * CELL_RESOLUTION) - y) * TERRAIN_RESOLUTION];

              if(value < 0) value = 0;
              if(value > 1) value = 1;
              //Account for the negative values (0 is -terrain height, terrain height is 0 in the world, two terrain height is highest)
              value = value * (TERRAIN_HEIGHT * 2);

              //All values have the offset of the negative applied to them.
              value -= TERRAIN_HEIGHT;

              Ogre::Vector3 normal = calculateNormal(x, y, heightData);
              //std::cout << calculateNormal(x, y, heightData) << '\n';

              //c_originalVertices[arrayCount] = CubeVertices(x, value, y, 0.5, 0.5, 0.5, x * width, y * height);
              //float xVal = x * (cellSize / CELL_RESOLUTION);
              //float yVal = y * (cellSize / CELL_RESOLUTION);
              float xVal = ((float)x / CELL_RESOLUTION) * cellSize;
              float yVal = ((float)y / CELL_RESOLUTION) * cellSize;
              c_originalVertices[arrayCount] = CubeVertices(xVal, value, yVal, normal.x, normal.y, normal.z, ((float)x / (float)CELL_RESOLUTION), ((float)y / (float)CELL_RESOLUTION));
              arrayCount++;
          }
      }

      memcpy(cubeVertices, c_originalVertices, sizeof(CubeVertices) * count);

      //Delete this
      Ogre::VertexBufferPacked *vertexBuffer = 0;
      try{
          vertexBuffer = vaoManager->createVertexBuffer(vertexElements, count, Ogre::BT_DEFAULT, cubeVertices, true);
      }catch(Ogre::Exception &e){
          vertexBuffer = 0;
          throw e;
      }

      Ogre::VertexBufferPackedVec vertexBuffers;
      vertexBuffers.push_back(vertexBuffer);
      Ogre::IndexBufferPacked *indexBuffer = createIndexBuffer();

      //Delete this
      //Ogre::VertexArrayObject *vao = vaoManager->createVertexArrayObject(vertexBuffers, indexBuffer, Ogre::OT_TRIANGLE_LIST);
      vao = vaoManager->createVertexArrayObject(vertexBuffers, indexBuffer, Ogre::OT_TRIANGLE_LIST);

      subMesh->mVao[Ogre::VpNormal].push_back(vao);
      subMesh->mVao[Ogre::VpShadow].push_back(vao);

      mesh->_setBounds( Ogre::Aabb( Ogre::Vector3::ZERO, Ogre::Vector3::UNIT_SCALE ), false );
      mesh->_setBoundingSphereRadius( 1.732f );

      return mesh;
  }
}
