#include "Terrain.h"

#include "../../World.h"
#include "TerrainCell.h"

#include <OgrePixelFormat.h>
#include <OgreHlmsManager.h>
#include <OgreHlmsPbs.h>
#include <OgreString.h>

namespace AV{
  Terrain::Terrain(std::string mapName, Ogre::SceneNode *chunkNode, unsigned int posX, unsigned int posY, std::string heightMapPath)
    : posX(posX),
      posY(posY){

        //terrainNode = chunkNode->createChildSceneNode(Ogre::SCENE_STATIC);
        terrainNode = chunkNode->createChildSceneNode(Ogre::SCENE_DYNAMIC);
        setupHeightData(heightMapPath);
        setupCells(mapName);

        hlmsManager = Ogre::Root::getSingleton().getHlmsManager();
        hlmsPbs = (Ogre::HlmsPbs*)hlmsManager->getHlms(Ogre::HLMS_PBS);


        macro.mDepthCheck = true;
        macro.mScissorTestEnabled = false;
        macro.mDepthWrite = false;
        macro.mDepthFunc = Ogre::CMPF_LESS_EQUAL;
        macro.mDepthBiasConstant = 0;
        macro.mDepthBiasSlopeScale = 0;
        macro.mCullMode = Ogre::CULL_CLOCKWISE;
        macro.mPolygonMode = Ogre::PM_SOLID;

        const Ogre::HlmsMacroblock* macroBlock = hlmsManager->getMacroblock(macro);

        blend.mAlphaToCoverageEnabled = false;
        blend.mSeparateBlend = false;
        blend.mSourceBlendFactor = Ogre::SBF_ONE;
        blend.mDestBlendFactor = Ogre::SBF_ONE_MINUS_SOURCE_ALPHA;
        blend.mBlendOperation = Ogre::SBO_ADD;

        const Ogre::HlmsBlendblock* blendBlock = hlmsManager->getBlendblock(blend);

        //temp["name"] = "Something";
        //temp["name"] = "test";
  }

  Terrain::Terrain(std::string mapName, Ogre::SceneNode *chunkNode, unsigned int posX, unsigned int posY)
    : posX(posX),
      posY(posY){

        //terrainNode = chunkNode->createChildSceneNode(Ogre::SCENE_STATIC);
        terrainNode = chunkNode->createChildSceneNode(Ogre::SCENE_DYNAMIC);
        setupHeightDataBlank();
        setupCells(mapName);
  }

  Terrain::~Terrain(){
    //Go through the cells and delete them.
    for(int i = 0; i < sizeof(cells) / sizeof(TerrainCell*); i++){
      delete cells[i];
    }
    terrainNode->removeAllChildren();
  }

  void Terrain::notifyStaticDirty(){
    // for(int i = 0; i < sizeof(cells) / sizeof(TerrainCell*); i++){
    //   cells[i]->notifyStaticDirty();
    // }
    Ogre::Root::getSingleton().getSceneManager("Scene Manager")->notifyStaticDirty(terrainNode);
  }

  void Terrain::setupHeightDataBlank(){
    for(int i = 0; i < TERRAIN_RESOLUTION * TERRAIN_RESOLUTION; i++){
      heightData[i] = 0;
    }
  }

  void Terrain::setupHeightData(const Ogre::String &imageName){
    Ogre::Image image;
    image.load(imageName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    int imgWidth = image.getWidth();
    int imgHeight = image.getHeight();

    //const Ogre::uint8 * RESTRICT_ALIAS data = reinterpret_cast<Ogre::uint8*RESTRICT_ALIAS>(image.getData());
    Ogre::PixelBox data = image.getPixelBox();

    for(int y = 0; y < TERRAIN_RESOLUTION; y++){
      int yVal = (int)(((float)y / TERRAIN_RESOLUTION) * imgHeight);
      for(int x = 0; x < TERRAIN_RESOLUTION; x++){
          int xVal = (int)(((float)x / TERRAIN_RESOLUTION) * imgWidth);

          //heightData[x + y * TERRAIN_RESOLUTION] = (float)data[(int)(xVal + yVal * imgWidth)] / 255;
          //heightData[x + y * TERRAIN_RESOLUTION] = 0;
          heightData[x + y * TERRAIN_RESOLUTION] = data.getColourAt(xVal, yVal, 0).r;
      }
    }
  }

  void Terrain::setupCells(std::string mapName){
    int cellsWidth = TERRAIN_RESOLUTION / CELL_RESOLUTION;
    int cellsHeight = TERRAIN_RESOLUTION / CELL_RESOLUTION;
    for(int y = 0; y < cellsHeight; y++){
      for(int x = 0; x < cellsWidth; x++){
        int numChunks = CHUNK_SIZE_WORLD / (TERRAIN_RESOLUTION / CELL_RESOLUTION);
        //Ogre::SceneNode *cellNode = terrainNode->createChildSceneNode(Ogre::SCENE_STATIC);
        Ogre::SceneNode *cellNode = terrainNode->createChildSceneNode(Ogre::SCENE_DYNAMIC);
        cellNode->setPosition(x * numChunks, 0, y * numChunks);

        Ogre::Root::getSingleton().getSceneManager("Scene Manager")->notifyStaticDirty(cellNode);

        //Ogre::HlmsParamVec temp;
        //Ogre::String name = mapName + Ogre::StringConverter::toString(posX) + Ogre::StringConverter::toString(posY) + Ogre::StringConverter::toString(x) + Ogre::StringConverter::toString(y);
        //temp["name"] = mapName;
        //Ogre::HlmsDatablock *block = hlmsPbs->createDatablock(name, "Something", macro, blend, temp);
        //temp.push_back(std::pair<Ogre::IdString, Ogre::String>("name", name));
        //Ogre::HlmsDatablock *block = hlmsPbs->createDatablock(name, "Something", macro, blend, temp);

        cells[x + y * cellsWidth] = new TerrainCell(mapName, cellNode, heightData, posX, posY, x, y);
        //cells[x + y * cellsWidth]->setDatablock(block);
      }
    }
  }
}
