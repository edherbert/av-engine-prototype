#ifndef __TERRAINCELL_H__
#define __TERRAINCELL_H__

#include <Ogre.h>

#include "../../settings.h"

namespace Ogre{
  class HlmsDatablock;
}

namespace AV{
  struct CubeVertices
  {
      float px, py, pz;   //Position
      float nx, ny, nz;   //Normals
      float tx, ty;   //Texture coordinates

      CubeVertices() {}
      //Populate the floats with values.
      CubeVertices( float _px, float _py, float _pz,
                    float _nx, float _ny, float _nz,
                    float _tx, float _ty ) :
          px( _px ), py( _py ), pz( _pz ),
          nx( _nx ), ny( _ny ), nz( _nz ),
          tx(_tx), ty(_ty)
      {
      }
  };

  class TerrainCell{
  public:
    TerrainCell(std::string mapName, Ogre::SceneNode *terrainNode, float (&heightData)[TERRAIN_RESOLUTION * TERRAIN_RESOLUTION], unsigned int cellX, unsigned int cellY, unsigned int terrainX, unsigned int terrainY);
    ~TerrainCell();

    Ogre::IndexBufferPacked* createIndexBuffer();
    Ogre::MeshPtr createStaticMesh(std::string mapName, float (&heightData)[TERRAIN_RESOLUTION * TERRAIN_RESOLUTION]);

    void setDatablock(Ogre::HlmsDatablock* datablock);
    void notifyStaticDirty();

  private:
    std::string mapName;
    Ogre::SceneNode *terrainNode;
    unsigned int cellX;
    unsigned int cellY;
    unsigned int terrainX;
    unsigned int terrainY;

    Ogre::Vector3 calculateNormal(int x, int y, float (&heightData)[TERRAIN_RESOLUTION * TERRAIN_RESOLUTION]);

    Ogre::VertexArrayObject *vao;

    Ogre::Item *cellItem;

    float cellSize = CHUNK_SIZE_WORLD / (TERRAIN_RESOLUTION / CELL_RESOLUTION);
  };
}

#endif
