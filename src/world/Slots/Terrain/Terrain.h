#ifndef TERRAIN_H
#define TERRAIN_H

#include <iostream>

#include "../../settings.h"
#include "OgreString.h"
#include <string>

#include <OgreHlmsDatablock.h>

namespace Ogre{
  class SceneNode;
  class HlmsManager;
  class HlmsPbs;
}

namespace AV{
  class TerrainCell;

  class Terrain{
  public:
    Terrain(std::string mapName, Ogre::SceneNode *chunkNode, unsigned int posX, unsigned int posY);
    Terrain(std::string mapName, Ogre::SceneNode *chunkNode, unsigned int posX, unsigned int posY, std::string heightMapPath);
    ~Terrain();

    void notifyStaticDirty();

  private:
    Ogre::SceneNode *terrainNode;
    unsigned int posX;
    unsigned int posY;

    Ogre::HlmsManager *hlmsManager;
    Ogre::HlmsPbs *hlmsPbs;

    Ogre::HlmsMacroblock macro;
    Ogre::HlmsBlendblock blend;

    static const int cellSizeCount = 16;

    float heightData[TERRAIN_RESOLUTION * TERRAIN_RESOLUTION];
    TerrainCell* cells[(TERRAIN_RESOLUTION / cellSizeCount) * (TERRAIN_RESOLUTION / cellSizeCount)];

    void setupHeightData(const Ogre::String &imageName);
    void setupHeightDataBlank();
    void setupCells(std::string mapName);
  };
}
#endif
