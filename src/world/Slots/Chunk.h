#ifndef __CHUNK_H__
#define __CHUNK_H__

#include <string>
#include "OgreVector3.h"
#include "OgreString.h"

#include <set>

namespace Ogre{
  class SceneManager;
  class SceneNode;
}

namespace tinyxml2{
  class XMLElement;
}

namespace AV{
  class Terrain;
  class Script;
  class TArea;

  class Chunk{
  public:
    Chunk(std::string mapName, int chunkX, int chunkY, int posX, int posY);
    ~Chunk();

    int getChunkX() { return chunkX; };
    int getChunkY() { return chunkY; };
    Ogre::String getMapName() { return mapName; };

    void setVisible(bool visible);
    void setPosition(Ogre::Vector3 pos);
    void translate(Ogre::Vector3 ammount);

  private:
    int chunkX;
    int chunkY;
    std::string mapName;

    void loadStaticMeshFile();
    void loadNewMeshFile();
    void loadEntityFile();
    void loadChunkScript();
    void loadTAreaFile();

    std::set<TArea*> tAreas;

    Script *chunkScript = 0;

    void parseStaticItems(tinyxml2::XMLElement *staticContent, Ogre::SceneNode *node);

    std::string heightMapPath = "";

    Ogre::SceneManager *sceneManager;

    AV::Terrain *terrain;
    Ogre::SceneNode *staticChunkNode;

    void setup(int posX, int posY);
    void destroy();
  };
}

#endif
