#ifndef __SLOT_POSITION_H__
#define __SLOT_POSITION_H__

#include "../settings.h"
#include "OgreVector3.h"
#include <LinearMath/btVector3.h>
#include "ESlotPosition.h"

#include <iostream>

namespace AV{
  class SlotPosition{
  public:
    int chunkX, chunkY;
    Ogre::Vector3 position;

    bool operator==(const SlotPosition &pos);

  public:
    SlotPosition();
    SlotPosition(int chunkX, int chunkY, Ogre::Vector3 position);
    SlotPosition(Ogre::Vector3 vector);
    SlotPosition(btVector3 vector);

    Ogre::Vector3 toOgre();
    btVector3 toBullet();
    ESlotPosition toESlot() const;
    void move(const Ogre::Vector3 &ammount);
    Ogre::Vector3 ogreDistanceBetweenSlots(SlotPosition second);
  };


  // struct SlotPosition{
  //   int chunkX, chunkY;
  //   Ogre::Vector3 position;
  // };
  //
  // /*static SlotPosition ogreToSlotPosition(Ogre::Vector3 pos){
  //   return Ogre::Vector3(position.get()->chunkX * CHUNK_SIZE_WORLD, 0, position.get()->chunkY * CHUNK_SIZE_WORLD)
  // }*/
  //
  // static Ogre::Vector3 slotPositionToOgre(SlotPosition pos){
  //   //return Ogre::Vector3(position.get()->chunkX * CHUNK_SIZE_WORLD, 0, position.get()->chunkY * CHUNK_SIZE_WORLD)
  //   return Ogre::Vector3(pos.chunkX * CHUNK_SIZE_WORLD + pos.position.x, pos.position.y, pos.chunkY * CHUNK_SIZE_WORLD + pos.position.z);
  //   //return Ogre::Vector3(0, 0, 0);
  // }
  //
  // //Might lead to precision errors if the range is too large.
  // static Ogre::Vector3 ogreDistanceBetweenSlots(SlotPosition first, SlotPosition second){
  //   //It depends on the position.
  //
  //   int diffx = second.chunkX - first.chunkX;
  //   int diffy = second.chunkY - first.chunkY;
  //   Ogre::Vector3 posDiff = second.position - first.position;
  //
  //   std::cout << "diffx: " << diffx << '\n';
  //   std::cout << "diffy: " << diffy << '\n';
  //   std::cout << posDiff << '\n';
  //
  //   Ogre::Vector3 done = Ogre::Vector3(diffx * CHUNK_SIZE_WORLD + posDiff.x, 0, diffy * CHUNK_SIZE_WORLD + posDiff.z);
  //   return done;
  // }
  //
  // static void movePosition(SlotPosition &original, Ogre::Vector3 ammount){
  //   //entityx::ComponentHandle<PositionComponent> position = entity.component<PositionComponent>();
  //
  //   //If the ammount is greater than the chunksize it will abort.
  //   if(ammount.x >= CHUNK_SIZE_WORLD || ammount.z >= CHUNK_SIZE_WORLD){
  //     return;
  //   }
  //
  //   Ogre::Vector3 pos = original.position;
  //   pos += ammount;
  //   original.position = pos;
  //
  //   bool chunkSwitch = false;
  //   if(pos.x < 0){
  //     original.chunkX -= 1;
  //     original.position.x = CHUNK_SIZE_WORLD + pos.x;
  //
  //     chunkSwitch = true;
  //   }
  //   if(pos.z < 0){
  //     original.chunkY -= 1;
  //     original.position.z = CHUNK_SIZE_WORLD + pos.z;
  //
  //     chunkSwitch = true;
  //   }
  //
  //   if(pos.x >= CHUNK_SIZE_WORLD){
  //     original.chunkX += 1;
  //
  //     original.position.x = pos.x - CHUNK_SIZE_WORLD;
  //
  //     chunkSwitch = true;
  //   }
  //
  //   if(pos.z >= CHUNK_SIZE_WORLD){
  //     original.chunkY += 1;
  //
  //     original.position.z = pos.z - CHUNK_SIZE_WORLD;
  //
  //     chunkSwitch = true;
  //   }
  //
  //   //if(position){
  //     // Ogre::Vector3 pos = original.position;
  //     // pos += ammount;
  //     // position.get()->position = pos;
  //
  //     //original.position += ammount;
  //
  //     //Find out how many slot switches would be caused by the interval.
  //     //int intervalX = original.position.x / CHUNK_SIZE_WORLD;
  //     //int intervalY = original.position.z / CHUNK_SIZE_WORLD;
  //
  //     //How many chunk switches are caused by the ammount itself.
  //     /*int intervalX = ammount.x / CHUNK_SIZE_WORLD;
  //     int intervalY = ammount.z / CHUNK_SIZE_WORLD;
  //     //std::cout << ammount.x << "  " << CHUNK_SIZE_WORLD << '\n';
  //     std::cout << "x : " << intervalX << " y: " << intervalY << '\n';
  //
  //     //Make the ammount less than a single chunk switch.
  //     ammount -= Ogre::Vector3(intervalX * CHUNK_SIZE_WORLD, 0, intervalY * CHUNK_SIZE_WORLD);
  //
  //     //Now add the ammount onto the position.
  //     //If that added value also makes a switch then do that.
  //     Ogre::Vector3 temp = original.position + ammount;
  //     int otherX = temp.x / CHUNK_SIZE_WORLD;
  //     int otherY = temp.z / CHUNK_SIZE_WORLD;
  //     std::cout << "otherX: " << otherX << "otherY: " << otherY << '\n';
  //
  //     //Check if the value goes over.
  //     //If it does increase the interval and set the position.
  //     if(otherX != 0){
  //       intervalX += otherX;
  //       temp.x -= otherX * CHUNK_SIZE_WORLD;
  //     }
  //     if(otherY != 0){
  //       intervalY += otherY;
  //       temp.y -= otherY * CHUNK_SIZE_WORLD;
  //     }
  //     std::cout << temp << '\n';
  //
  //     original.chunkX = intervalX;
  //     original.chunkY = intervalY;
  //     original.position = temp;*/
  //
  //     //original.chunkX += intervalX;
  //     //original.chunkY += intervalY;
  //
  //     //Ogre::Vector3 temp = original.position + ammount;
  //     //temp += (Ogre::Vector3(original.chunkX * CHUNK_SIZE_WORLD, 0, original.chunkY * CHUNK_SIZE_WORLD));
  //     //std::cout << temp << '\n';
  //     //temp.position = original
  //     //temp = Ogre::Vector3(original.position.x % CHUNK_SIZE_WORLD, original.position.y % CHUNK_SIZE_WORLD, original.position.z % CHUNK_SIZE_WORLD);
  //
  //     //The problem is that moving might also push the slot count over to a new boundary.
  //     //I would need to pre-determine how many boundaries will be moved.
  //     //For example, I could
  //
  //     /*bool chunkSwitch = false;
  //     if(pos.x < 0){
  //       position.get()->chunkX -= 1;
  //       position.get()->position.x = CHUNK_SIZE_WORLD + pos.x;
  //
  //       chunkSwitch = true;
  //     }
  //     if(pos.z < 0){
  //       position.get()->chunkY -= 1;
  //       position.get()->position.z = CHUNK_SIZE_WORLD + pos.z;
  //
  //       chunkSwitch = true;
  //     }
  //
  //     if(pos.x >= CHUNK_SIZE_WORLD){
  //       position.get()->chunkX += 1;
  //
  //       position.get()->position.x = pos.x - CHUNK_SIZE_WORLD;
  //
  //       chunkSwitch = true;
  //     }
  //
  //     if(pos.z >= CHUNK_SIZE_WORLD){
  //       position.get()->chunkY += 1;
  //
  //       position.get()->position.z = pos.z - CHUNK_SIZE_WORLD;
  //
  //       chunkSwitch = true;
  //     }
  //
  //
  //     if(chunkSwitch){
  //       std::cout << "Slot switch to " << position.get()->chunkX << ", " << position.get()->chunkY << '\n';
  //     }*/
  // //}
  // }
};

#endif
