#include "World.h"

#include <iostream>

#include <Ogre.h>
#include "settings.h"

#include "../Input.h"

#include "Slots/SlotManager.h"
#include "Entity/EntityManager.h"
#include "Dialog/DialogManager.h"
#include "Physics/BulletManager.h"
#include "ChunkRadiusLoader.h"
#include "Slots/TArea/TAreaManager.h"

#include "Entity/Player/PlayerManager.h"

#include "../scripting/WorldScripts.h"

#include "Slots/ESlotPosition.h"

namespace AV{

//AA-D80073

World::World(){
  _setupWorld();

  bulletManager = new BulletManager();
  slotManager = new SlotManager();
  entityManager = new EntityManager(this);
  dialogManager = new DialogManager();
  chunkRadiusLoader = new ChunkRadiusLoader(this);
  tAreaManager = new TAreaManager(this);
}

World::~World(){
  delete dialogManager;
}

void World::_setupWorld(){
  sceneManager = Ogre::Root::getSingletonPtr()->createSceneManager(Ogre::ST_GENERIC, 2, Ogre::INSTANCING_CULLING_SINGLETHREAD, "Scene Manager");

  camera = sceneManager->createCamera("Main Camera");

  camera->setPosition(Ogre::Vector3( 0, 0, 0 ));  //camera->setPosition( Ogre::Vector3( 0, 50, -100 ) );
  // Look back along -Z
  //camera->lookAt( Ogre::Vector3( 0, 0, 0 ) );
  camera->setNearClipDistance( 0.2f );
  camera->setFarClipDistance( 1000.0f );
  camera->setAutoAspectRatio( true );

  //sceneManager->setAmbientLight( Ogre::ColourValue( 0.3f, 0.3f, 0.3f ), Ogre::ColourValue( 0.02f, 0.53f, 0.96f ) * 0.1f, Ogre::Vector3::UNIT_Y );

  Ogre::Light *light = sceneManager->createLight();
  Ogre::SceneNode *lightNode = sceneManager->getRootSceneNode()->createChildSceneNode();
  lightNode->attachObject( light );
  light->setPowerScale( Ogre::Math::PI );
  light->setType( Ogre::Light::LT_DIRECTIONAL );
  light->setDirection( Ogre::Vector3( -1, -1, -1 ).normalisedCopy() );

  sceneManager->setForward3D( true, 4,4,5,96,3,200 );
}

EntityManager* World::getEntityManager(){
  return entityManager;
}

Ogre::String World::getCurrentMap(){
  return slotManager->getCurrentMap();
}

void World::update(){

  if(!testVal){
    //slotManager->setCurrentMap("overworld");
    //slotManager->loadChunk({"overworld", 1, 0});
    loadChunk("overworld", 0, 0);
    loadChunk("overworld", 1, 0);
    //slotManager->loadChunk({"overworld", 5, 0});
    //slotManager->unloadChunk({"overworld", 5, 0});

    loadChunk("cave", 11, 0);
    //slotManager->unloadChunk({"cave", 11, 0});

    teleportPlayer(0, 0, Ogre::Vector3(0, 0, 0), "overworld");
    testVal = true;

    camera->lookAt(Ogre::Vector3::ZERO);

    ESlotPosition pos(11, 0, 0, 0);
    std::cout << "Thing " << pos.getId() << '\n';

    SlotPosition pos2(11, 0, Ogre::Vector3::ZERO);
    std::cout << "Thing " << pos2.toESlot().getId() << '\n';
  }
  entityManager->update();
  dialogManager->update();
  bulletManager->update();

  // if(Input::getInput(I_ACCEPT)){
  //   slotManager->setCurrentMap("cave");
  //   //slotManager->setOrigin({11, 0, Ogre::Vector3(0, 0, 0)});
  //   SlotPosition thing = {11, 0, entityManager->getPlayerManager()->getPlayerSlotPosition().position};
  //   //SlotPosition thing = {11, 0, Ogre::Vector3(100, 0, 100)};
  //   slotManager->setOrigin(thing);
  // }
  // if(Input::getInput(I_DECLINE)){
  //   slotManager->setCurrentMap("overworld");
  //   //slotManager->setOrigin({11, 0, Ogre::Vector3(0, 0, 0)});
  //   SlotPosition thing = {0, 0, Ogre::Vector3::ZERO};
  //   //SlotPosition thing = {11, 0, Ogre::Vector3(100, 0, 100)};
  //   slotManager->setOrigin(thing);
  // }

  // if(Input::getInput(I_ACCEPT)){
  //   //slotManager->setCurrentMap("cave");
  //   //teleportPlayer(11, 0, Ogre::Vector3::ZERO, "cave");
  //   teleportPlayer(11, 0, Ogre::Vector3(100, 0, 0), "cave");
  // }
  // if(Input::getInput(I_DECLINE)){
  //   teleportPlayer(0, 0, Ogre::Vector3(0, 0, 0), "overworld");
  // }
}

void World::teleportPlayer(int chunkX, int chunkY, Ogre::Vector3 position, Ogre::String mapName){
  slotManager->setOrigin({chunkX, chunkY, position});
  if(mapName == "" || mapName == slotManager->getCurrentMap()){
    //keep on the current map
    std::cout << "Keeping on the same map." << '\n';

    //entityManager->
  }else{
    //Move it to a different map.
    std::cout << "Switching maps." << '\n';

    slotManager->setCurrentMap(mapName);
    //entityManager->clearEntities();
  }

  //entityManager->loadEntityChunkFile(mapName, chunkX, chunkY);
  entityManager->setPlayerPosition({chunkX, chunkY, position});
  entityManager->shiftEntityOrigin();
}

void World::setOrigin(SlotPosition newOrigin){
  worldOrigin = newOrigin;

  entityManager->shiftEntityOrigin();
}

Ogre::Camera* World::getCamera(){
  return camera;
}

Ogre::SceneManager* World::getSceneManager(){
  return sceneManager;
}

void World::loadChunk(Ogre::String mapName, int chunkX, int chunkY){
  slotManager->loadChunk({mapName, chunkX, chunkY});
  //entityManager->loadEntityChunkFile(mapName, chunkX, chunkY);
}

/*
void World::loadChunk(Ogre::String mapName, int chunkX, int chunkY){
  slotManager->loadChunk({mapName, chunkX, chunkY});
  entityManager->loadChunkEntities(mapName, chunkX, chunkY);
}

void World::unloadChunk(Ogre::String mapName, int chunkX, int chunkY){
  slotManager->unloadChunk({mapName, chunkX, chunkY});
}

void World::setChunkVisible(Ogre::String mapName, int chunkX, int chunkY){
  slotManager->setChunkVisible({mapName, chunkX, chunkY});
}

void World::setChunkInvisible(Ogre::String mapName, int chunkX, int chunkY){
  slotManager->setChunkInvisible({mapName, chunkX, chunkY});
}

entityx::Entity World::createEntity(int chunkX, int chunkY, Ogre::Vector3 position){
  return entityManager->createEntity(chunkX, chunkY, position);
}

entityx::Entity World::getEntity(entityx::Entity::Id *id){
  return entityManager->getEntity(id);
}

void World::moveEntity(entityx::Entity::Id *id, Ogre::Vector3 ammount){
  entityManager->moveEntity(getEntity(id), ammount);
}

void World::update(){
  entityManager->update();
}
*/
/*entityx::ComponentHandle getComponent(int componentId, entityx::Entity::Id *id){
  entityManager->getComponent(componentId, id);
}*/

}
