#include "EntityManager.h"

#include "Ogre.h"

#include "components/PlayerInputComponent.h"
#include "components/PositionComponent.h"
#include "components/OgreMeshComponent.h"
#include "components/PlayerDialogComponent.h"
#include "components/RigidBodyComponent.h"
#include "components/ActionTriggerComponent.h"
#include "components/AnimationComponent.h"
#include "components/MetaComponent.h"
#include "components/LifespanComponent.h"
#include "components/HealthComponent.h"
#include "components/CombatStatsComponent.h"
#include "components/AfflictionComponent.h"
#include "../../EventManager/EventManager.h"
#include "../../EventManager/Events/InputEvent.h"
#include "../../EventManager/Events/DialogEvent.h"
#include "../../EventManager/Events/StatsEvent.h"

#include "../Physics/BulletManager.h"
#include "Controllers/ControllerManager.h"

#include "Player/PlayerManager.h"
#include "EntityMeshManager.h"
#include "EntityChunkManager.h"
#include "../Slots/eChunkFileParsers/EntityFileParser.h"

#include "Factory/EntityFactory.h"
#include "OgreItem.h"

#include "../World.h"
#include "../settings.h"
#include "ai/AiManager.h"

#include "Damage/DamageInflictor.h"
#include "../../serialisation/EntitySerialisationManager.h"


//Temp
#include "components/PlayerInputComponent.h"
#include "components/PositionComponent.h"
#include "components/OgreMeshComponent.h"
#include "components/PlayerDialogComponent.h"
#include "components/AnimationComponent.h"
#include "components/MetaComponent.h"
#include "components/RigidBodyComponent.h"
#include "components/AiComponent.h"
#include "components/MovementComponent.h"
#include "components/DamageTriggerComponent.h"
#include "EntityBodyManager.h"
#include "../Physics/DamageWorldManager.h"

#include "../../util/BrainTree.h"

namespace AV{
  EntityManager::EntityManager(World *world)
    : world(world){
    //ex.systems.add<PlayerInputComponentSystem>();
    ex.systems.add<OgreMeshComponentSystem>();
    ex.systems.add<RigidBodyComponentSystem>();
    ex.systems.add<ActionTriggerComponentSystem>();
    ex.systems.add<AnimationComponentSystem>();
    ex.systems.add<DamageTriggerComponentSystem>();
    ex.systems.configure();

    entityMeshManager = new EntityMeshManager(world->getSceneManager());
    playerManager = new PlayerManager(this);
    //entityChunk = new EntityChunk(this);
    entityChunkManager = new EntityChunkManager(this, world->getChunkRadiusLoader());

    entityFileParser = new EntityFileParser(this);
    entityFactory = new EntityFactory(this);
    aiManager = new AiManager(this);
    damageInflictor = new DamageInflictor(this);

    DamageWorldManager::entityManager = this;

    //entityx::Entity player = createEntity(0, 0, Ogre::Vector3(0, 0, 0));

    ControllerManager::setup(this);

    EntitySerialisationManager::initialise(&ex.entities);
  }

  EntityManager::~EntityManager(){
    delete entityFileParser;
    delete damageInflictor;
  }

  Ogre::String EntityManager::getCurrentMapName(){
    return world->getCurrentMap();
  }

  entityx::Entity EntityManager::createEntity(int chunkX, int chunkY, Ogre::Vector3 position, bool bind){
    return createEntity(SlotPosition(chunkX, chunkY, position), bind);
  }

  entityx::Entity EntityManager::createEntity(SlotPosition pos, bool bind){
    entityx::Entity entity = ex.entities.create();
    //entity.assign<PositionComponent>(chunkX, chunkY, position);
    entity.assign<PositionComponent>(pos);

    //entityChunkManager->insertEntityIntoChunk(entity, 0, 0, 0, 0);

    if(bind){
      entityChunkManager->addEntity(entity);
    }

    return entity;
  }

  entityx::Entity EntityManager::getEntity(entityx::Entity::Id id){
    entityx::Entity e = entityx::Entity(&ex.entities, id);
    return e;
  }

  void EntityManager::update(){
    playerManager->update();
    ex.systems.update<OgreMeshComponentSystem>(1);
    //ex.systems.update<RigidBodyComponentSystem>(1);
    ex.systems.update<ActionTriggerComponentSystem>(1);
    ex.systems.update<AnimationComponentSystem>(1);
    ex.systems.update<DamageTriggerComponentSystem>(1);

    ex.entities.each<RigidBodyComponent>([this](entityx::Entity entity, RigidBodyComponent &comp) {
      btTransform trans;
      comp.body->getMotionState()->getWorldTransform(trans);

      entityx::ComponentHandle<PositionComponent> position = entity.component<PositionComponent>();
      if(position){
        this->setEntityPosition(entity, SlotPosition(trans.getOrigin()));
      }
    });

    ex.entities.each<MovementComponent>([this](entityx::Entity entity, MovementComponent &comp) {
      entityx::ComponentHandle<PositionComponent> position = entity.component<PositionComponent>();
      if(position){
        SlotPosition pos = position.get()->pos;
        pos.move(comp.direction * comp.speed);

        this->setEntityPosition(entity, pos);
      }
    });

    ex.entities.each<LifespanComponent>([this](entityx::Entity entity, LifespanComponent &comp) {
      if(comp.lifespan > 0) comp.lifespan--;
      else{
        this->destroyEntity(entity);
      }
    });

    ex.entities.each<MetaComponent>([this](entityx::Entity entity, MetaComponent &comp) {
      if(comp.controllerType == CONTROLLER_TYPE_PHYSICS){
        ControllerManager::updatePhysicsController(entity);
      }
    });
    ControllerManager::updatePlayerController(playerManager->getPlayer());

    ex.entities.each<AiComponent>([this](entityx::Entity entity, AiComponent &comp) {
      comp.tree->update();
    });

    ex.entities.each<AfflictionComponent>([this](entityx::Entity entity, AfflictionComponent &comp) {
      //Check whether it's time to do anything for that entity.
      bool shouldContinue = true;
      if(comp.currentTickPeriod > 0) {
        comp.currentTickPeriod--;
        shouldContinue = false;
      }
      else{
        comp.currentTickPeriod = comp.afflictionTickPeriod;
      }

      //I couldn't find a way to continue inside this loop, so I did an if instead :/
      if(shouldContinue){
        DamageInflictor::processAffliction(entity, comp);
        if(comp.afflictionLifeTime > 0) comp.afflictionLifeTime--;
        else{
          entity.remove<AfflictionComponent>();
        }
      }
    });

    if(!testVal){
      testVal = true;

      //entityChunkManager->
      entityx::Entity e = createEntity(0, 0, Ogre::Vector3::ZERO);
      e.assign<OgreMeshComponent>(EntityMeshManager::createOgreMesh("Sinbad.mesh"));

      entityChunkManager->removeEntity(e);
      //entityChunkManager->addEntity(e);

      destroyEntity(e);
      //entityChunkManager->destroyChunk(ESlotPosition(0, 0, 0, 0));
      // entityChunkManager->destroyChunk(ESlotPosition(0, 0, 2, 0));

      for(int i = 0; i < 6; i++){
        tempEntity = createEntity(0, 0, Ogre::Vector3(50, 0, 50), true);
        Ogre::SceneNode *pNode = EntityMeshManager::createOgreMesh("Sinbad.mesh");
        tempEntity.assign<OgreMeshComponent>(pNode);

        tempEntity.assign<AnimationComponent>();
        tempEntity.assign<MetaComponent>();
        tempEntity.assign<AiComponent>(aiManager->createTree(tempEntity));
        tempEntity.assign<HealthComponent>(50);
        tempEntity.assign<CombatStats>(1, 5);

        btCollisionObject* entityObject = DamageWorldManager::createDamageReceiver(Ogre::Vector3(50, 0, 50), tempEntity, new btBoxShape(btVector3(1, 1, 1)));
        tempEntity.assign<DamageTriggerComponent>(entityObject);

        AfflictionData aData;
        aData.afflictionValue = 1;
        tempEntity.assign<AfflictionComponent>(aData, 5, 50);
        //tempEntity.assign<RigidBodyComponent>(EntityBodyManager::createCapsuleBody(btVector3(50, 10, 50), 1, 6));
        setEntityPosition(tempEntity, SlotPosition(0, 0, Ogre::Vector3(50 + i * 20, 10, 75 + i * 20)));

        setEntityController(tempEntity, CONTROLLER_TYPE_ABSOLUTE);

        //DamageInflictor::damageEntity(tempEntity, 5);
        // DamageInflictor::DamageData data;
        // data.attackPower = 10;
        // DamageInflictor::damageEntity(tempEntity, data);
      }

      //entityFactory->createProjectile(SlotPosition(0, 0, Ogre::Vector3(50, 0, 50)), Ogre::Vector3::ZERO);

      tempEntity = createEntity(0, 0, Ogre::Vector3(30, 50, 30));
      Ogre::SceneNode *pNode = EntityMeshManager::createOgreMesh("ogrehead2.mesh");
      pNode->setScale(Ogre::Vector3(0.1, 0.1, 0.1));
      tempEntity.assign<OgreMeshComponent>(pNode);
      btRigidBody* body = EntityBodyManager::createCubeBody(btVector3(30, 50, 30), btVector3(1, 1, 1));
      body->setLinearVelocity(btVector3(10, 0, 0));
      tempEntity.assign<RigidBodyComponent>(body);
      //tempEntity.assign<MovementComponent>(0.1, Ogre::Vector3(-1, 0, 0.3));

    }

    if(tempEntity){
      //moveEntity(tempEntity, Ogre::Vector3(0.6, 0, 0));
      //aiManager->update(tempEntity);
    }
  }

  void EntityManager::shiftEntityOrigin(){
    ex.entities.each<OgreMeshComponent>([](entityx::Entity entity, OgreMeshComponent &comp) {
      comp.dirty = true;
    });
  }

  void EntityManager::loadEntityChunkFile(Ogre::String mapName, int chunkX, int chunkY){
    //entityChunk->loadEntityChunkFile(mapName, chunkX, chunkY);
  }

  void EntityManager::clearEntities(){
    entityx::Entity player = playerManager->getPlayer();
    ex.entities.each<PositionComponent>([player](entityx::Entity entity, PositionComponent &comp){
      if(entity != player){
        entity.destroy();
      }
    });

    EntityMeshManager::removeAndDestroyMeshes();
  }

  void EntityManager::destroyEntity(entityx::Entity::Id entityId){
    destroyEntity(getEntity(entityId));
  }

  void EntityManager::destroyEntity(entityx::Entity entity){
    entityx::ComponentHandle<OgreMeshComponent> mesh = entity.component<OgreMeshComponent>();
    if(mesh){
      EntityMeshManager::destroyOgreMesh(mesh.get()->parentNode);
    }
    entityx::ComponentHandle<ActionTriggerComponent> actionTrigger = entity.component<ActionTriggerComponent>();
    if(actionTrigger){
      BulletManager::destroyCollisionObject(actionTrigger.get()->collisionObject);
    }
    entityx::ComponentHandle<DamageTriggerComponent> damageTrigger = entity.component<DamageTriggerComponent>();
    if(damageTrigger){
      DamageWorldManager::destroyCollisionObject(damageTrigger.get()->collisionObject);
    }
    entityx::ComponentHandle<RigidBodyComponent> rigidBody = entity.component<RigidBodyComponent>();
    if(rigidBody){
      //BulletManager::destroyCollisionObject(actionTrigger.get()->collisionObject);
      BulletManager::destroyRigidBody(rigidBody.get()->body);
    }
    entityChunkManager->removeEntity(entity);
    entity.destroy();
  }

  Ogre::Real EntityManager::distanceBetweenEntities(entityx::Entity f, entityx::Entity s){
    entityx::ComponentHandle<PositionComponent> fPosComp = f.component<PositionComponent>();
    entityx::ComponentHandle<PositionComponent> sPosComp = s.component<PositionComponent>();

    if(fPosComp && sPosComp){
      Ogre::Vector3 f = fPosComp.get()->pos.toOgre();
      Ogre::Vector3 s = sPosComp.get()->pos.toOgre();

      return f.distance(s);
    }

    return 0;
  }

  entityx::Entity EntityManager::getPlayer(){
    return playerManager->getPlayer();
  }

  void EntityManager::setEntityPosition(entityx::Entity entity, SlotPosition position){
    entityx::ComponentHandle<PositionComponent> compPos = entity.component<PositionComponent>();

    //setEntityChunk(entity, compPos.get()->pos, position);
    entityChunkManager->updateEntity(entity, compPos.get()->pos.toESlot(), position.toESlot());
    if(compPos){
      // compPos.get()->pos.chunkX = position.chunkX;
      // compPos.get()->pos.chunkY = position.chunkY;
      // compPos.get()->pos.position = position.position;
      compPos.get()->pos = position;
    }

    entityx::ComponentHandle<OgreMeshComponent> meshComponent = entity.component<OgreMeshComponent>();
    if(meshComponent){
      meshComponent.get()->dirty = true;
    }
    entityx::ComponentHandle<ActionTriggerComponent> actionComponent = entity.component<ActionTriggerComponent>();
    if(actionComponent){
      actionComponent.get()->dirty = true;
    }
    entityx::ComponentHandle<DamageTriggerComponent> damageTriggerComponent = entity.component<DamageTriggerComponent>();
    if(damageTriggerComponent){
      damageTriggerComponent.get()->dirty = true;
    }
    entityx::ComponentHandle<RigidBodyComponent> body = entity.component<RigidBodyComponent>();
    if(body){
        body.get()->body->getWorldTransform().setOrigin(position.toBullet());
    }
    entityx::ComponentHandle<PlayerInputComponent> player = entity.component<PlayerInputComponent>();
    if(player){
      //This entity is the player, so send out the events.
      StatsEvent e;
      e.statsType = STATS_SLOT_PLAYER_POS;
      e.slotValue = position;
      EventManager::transmitStatsEvent(e);

      StatsEvent d;
      d.statsType = STATS_OGRE_PLAYER_POS;
      //d.position = slotPositionToOgre(position.get()->pos);
      d.position = position.toOgre();
      EventManager::transmitStatsEvent(d);
    }
  }

  void EntityManager::setEntityController(entityx::Entity entity, CONTROLLER_TYPE id){
    entityx::ComponentHandle<MetaComponent> meta = _checkForEntityMeta(entity);
    if(meta){
      meta.get()->controllerType = id;

      if(id == CONTROLLER_TYPE_PHYSICS){
        entityx::ComponentHandle<RigidBodyComponent> body = entity.component<RigidBodyComponent>();
        //If the entity doesn't have a rigid body, set their controller to the absolute one.
        if(!body) meta.get()->controllerType = CONTROLLER_TYPE_ABSOLUTE;
      }
    }
  }

  entityx::ComponentHandle<MetaComponent> EntityManager::_checkForEntityMeta(entityx::Entity entity){
    entityx::ComponentHandle<MetaComponent> meta = entity.component<MetaComponent>();
    if(meta){
      return meta;
    }else{
      return entity.assign<MetaComponent>();
    }
  }

  CONTROLLER_TYPE EntityManager::getEntityController(entityx::Entity entity){
    entityx::ComponentHandle<MetaComponent> meta = entity.component<MetaComponent>();
    if(meta){
      return meta.get()->controllerType;
    } return CONTROLLER_TYPE_ABSOLUTE;
  }

  void EntityManager::setEntityAnimation(entityx::Entity entity, const Ogre::String &animName){
    //Go through the array and see if there's an empty animation.
    //If there is one then load it up and feed it in.

    entityx::ComponentHandle<OgreMeshComponent> mesh = entity.component<OgreMeshComponent>();
    entityx::ComponentHandle<AnimationComponent> anim = entity.component<AnimationComponent>();
    if(!mesh || !anim) return;

    Ogre::Item *playerItem = (Ogre::Item*)mesh.get()->parentNode->getAttachedObject(0);
    Ogre::SkeletonInstance *skeleton = playerItem->getSkeletonInstance();

    anim.get()->anims[0] = skeleton->getAnimation(animName);
    anim.get()->anims[0]->setEnabled(true);
    anim.get()->anims[0]->setLoop(true);
    anim.get()->anims[0]->mWeight = 0;
    anim.get()->fadingIn[0] = true;
  }

  void EntityManager::setEntityChunk(entityx::Entity entity, SlotPosition oldPos, SlotPosition newPos){
    //Determine the entity chunk in question.

    float eChunkSize = CHUNK_SIZE_WORLD / 3;
    int oldEChunkX = floor(oldPos.position.x / eChunkSize);
    int oldEChunkY = floor(oldPos.position.z / eChunkSize);

    int newEChunkX = floor(newPos.position.x / eChunkSize);
    int newEChunkY = floor(newPos.position.z / eChunkSize);

    // std::cout << oldEChunkX << '\n';
    // std::cout << oldEChunkY << '\n';

    //entityChunkManager->switchEntityToChunk(entity, oldPos.chunkX, oldPos.chunkY, oldEChunkX, oldEChunkY, newPos.chunkX, newPos.chunkY, newEChunkX, newEChunkY);
  }

  void EntityManager::setPlayerPosition(SlotPosition position){
    setEntityPosition(playerManager->getPlayer(), position);
    playerManager->positionCamera();

    StatsEvent e;
    e.statsType = STATS_SLOT_PLAYER_POS;
    e.slotValue = position;
    EventManager::transmitStatsEvent(e);

    StatsEvent d;
    d.statsType = STATS_OGRE_PLAYER_POS;
    d.position = position.toOgre();
    EventManager::transmitStatsEvent(d);
  }

  void EntityManager::sendEntityControllerCommand(entityx::Entity entity, PlayerControllerAction &action){
    CONTROLLER_TYPE cType = getEntityController(entity);

    ControllerManager::performCommand(entity, action, cType);
  }

  void EntityManager::moveEntity(entityx::Entity entity, const Ogre::Vector3 &ammount){
    entityx::ComponentHandle<PositionComponent> position = entity.component<PositionComponent>();

    if(position){
      const SlotPosition oldPos = position.get()->pos;

      CONTROLLER_TYPE cType = getEntityController(entity);

      //position.get()->pos.move(ammount);
      SlotPosition newPos = ControllerManager::moveSlotPosition(oldPos, ammount, cType, entity);
      //position.get()->pos = newPos;

      setEntityPosition(entity, newPos);

      //if(entity != playerManager->getPlayer()){
      //}
    }
  }
}
