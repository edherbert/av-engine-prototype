#include "PlayerManager.h"

#include "../EntityManager.h"

#include "../../../EventManager/EventManager.h"
#include "../components/PlayerInputComponent.h"
#include "../components/PositionComponent.h"
#include "../components/OgreMeshComponent.h"
#include "../components/PlayerDialogComponent.h"
#include "../components/AnimationComponent.h"
#include "../components/MetaComponent.h"
#include "../components/RigidBodyComponent.h"

#include "../../../EventManager/Events/DialogEvent.h"
#include "../../../EventManager/Events/InputEvent.h"
#include "../../../EventManager/Events/StatsEvent.h"

#include "../EntityMeshManager.h"
#include "../EntityBodyManager.h"
#include "../Controllers/PlayerControllerManager.h"
#include "../Controllers/ControllerActions.h"

#include "Ogre.h"
#include "OgreItem.h"

#include "../../../Input.h"

#include "../../World.h"

namespace AV{
  PlayerManager::PlayerManager(EntityManager *entityManager)
    : entityManager(entityManager){

    //Construct the player here.
    //Do this with the factory later.
    entityx::Entity p = entityManager->createEntity(0, 0, Ogre::Vector3::ZERO, false);
    Ogre::SceneNode *pNode = EntityMeshManager::createOgreMesh("Sinbad.mesh");
    p.assign<OgreMeshComponent>(pNode);
    p.assign<PlayerInputComponent>();

    p.assign<AnimationComponent>();
    p.assign<MetaComponent>();
    p.assign<RigidBodyComponent>(EntityBodyManager::createCapsuleBody(btVector3(0, 0, 0), 1, 6));
    setPlayer(p);

    entityx::ComponentHandle<AnimationComponent> anim = p.component<AnimationComponent>();
    //mesh.get();
    Ogre::Item *playerItem = (Ogre::Item*)pNode->getAttachedObject(0);
    Ogre::SkeletonInstance *skeleton = playerItem->getSkeletonInstance();

    // anim.get()->idleTop = skeleton->getAnimation("IdleTop");
    // anim.get()->idleBottom = skeleton->getAnimation("IdleBase");
    //
    // anim.get()->idleTop->setEnabled(true);
    // anim.get()->idleTop->setLoop(true);
    // anim.get()->idleBottom->setEnabled(true);
    // anim.get()->idleBottom->setLoop(true);

    // anim.get()->anims[0] = skeleton->getAnimation("IdleTop");
    // anim.get()->anims[0]->setEnabled(true);
    // anim.get()->anims[0]->setLoop(true);


    //entityManager->setEntityAnimation(p, "Dance");
    entityManager->setEntityController(p, CONTROLLER_TYPE_PLAYER);

    entityManager->setEntityPosition(p, SlotPosition(0, 0, Ogre::Vector3(50, 50, 50)));

    EventManager::subscribeToEvent(this, EVENT_TYPE_DIALOG);
    EventManager::subscribeToEvent(this, EVENT_TYPE_INPUT);
    EventManager::subscribeToEvent(this, EVENT_TYPE_STATS);
  }

  PlayerManager::~PlayerManager(){

  }

  void PlayerManager::setPlayer(entityx::Entity e){
    this->player = e;
    entityx::ComponentHandle<OgreMeshComponent> mesh = e.component<OgreMeshComponent>();
    if(mesh){
      //Another node was registered as the player, so before setting the new one return the old one to the usual entity node.
      if(playerNode){
        EntityMeshManager::attachNode(playerNode);
      }

      //Get the node
      playerNode = mesh.get()->parentNode;
      //Remove it from the parent.
      playerNode->getParentSceneNode()->removeChild(playerNode);
      //Attach it to the root node.
      Ogre::Root::getSingletonPtr()->getSceneManager("Scene Manager")->getRootSceneNode()->createChildSceneNode(Ogre::SCENE_DYNAMIC)->addChild(playerNode);
    }else playerNode = 0;
  }

  void PlayerManager::setPlayerLock(bool lock){
    playerLock = lock;
  }

  void PlayerManager::update(){
    if(!playerLock){

      // entityManager->moveEntity(player, Ogre::Vector3(1, 0, 0));
      // positionCamera();
      //
      // return;

      entityManager->ex.entities.each<PlayerInputComponent>([this](entityx::Entity entity, PlayerInputComponent &comp) {
        Ogre::Vector3 ammount(0, 0, 0);
        if(Input::getInput(I_UP)) ammount.z += 4;
        if(Input::getInput(I_DOWN)) ammount.z -= 4;
        if(Input::getInput(I_LEFT)) ammount.x += 4;
        if(Input::getInput(I_RIGHT)) ammount.x -= 4;
        if(Input::getInput(I_JUMP)) {
          //entityManager->sendEntityControllerCommand(entity, Ogre::IdString("Jump"));
          PlayerControllerActionJump action;
          entityManager->sendEntityControllerCommand(entity, action);
        }

        bool aimVals[4] = {
          Input::getInput(I_AIM_UP),
          Input::getInput(I_AIM_DOWN),
          Input::getInput(I_AIM_LEFT),
          Input::getInput(I_AIM_RIGHT)
        };

        bool aimInput = false;
        for(int i = 0; i < 4; i++){
          if(aimVals[i]) aimInput = true;
        }
        if(aimInput){
          Ogre::Vector2 dir = Ogre::Vector2::ZERO;
          if(aimVals[0]) dir.y += 1;
          if(aimVals[1]) dir.y -= 1;
          if(aimVals[2]) dir.x += 1;
          if(aimVals[3]) dir.x -= 1;
          PlayerControllerActionShoot shootAction(dir);
          entityManager->sendEntityControllerCommand(entity, shootAction);
        }


        if(ammount != Ogre::Vector3::ZERO){
          entityManager->moveEntity(entity, ammount);

          entityx::ComponentHandle<PositionComponent> position = entity.component<PositionComponent>();
          if(position){
            // StatsEvent e;
            // e.statsType = STATS_SLOT_PLAYER_POS;
            // e.slotValue = position.get()->pos;
            // EventManager::transmitStatsEvent(e);
            //
            // StatsEvent d;
            // d.statsType = STATS_OGRE_PLAYER_POS;
            // //d.position = slotPositionToOgre(position.get()->pos);
            // d.position = position.get()->pos.toOgre();
            // EventManager::transmitStatsEvent(d);
          }
          //Position the camera here.
          //positionCamera();
        }
      });
    }
  }

  void PlayerManager::positionCamera(){
    entityx::ComponentHandle<PositionComponent> position = player.component<PositionComponent>();
    if(position){
      //entityManager->world->getCamera()->setPosition(position.get()->pos.toOgre() += Ogre::Vector3(0, 70, -80));
      entityManager->world->getCamera()->setPosition(position.get()->pos.toOgre() += Ogre::Vector3(0, 150, -160));
      //entityManager->world->getCamera()->setPosition(position.get()->pos.toOgre() += Ogre::Vector3(0, 10, 50));
      entityManager->world->getCamera()->lookAt(position.get()->pos.toOgre());
      //entityManager->world->getCamera()->setPosition(position.get()->pos.toOgre() += Ogre::Vector3(0, 30, -30));
    }
  }

  bool PlayerManager::isPlayerMoving(){
    return false;
  }

  void PlayerManager::notifyDialogEvent(DialogEvent &e){
    if(e.dialogType == DIALOG_END){
      setPlayerLock(false);
    }
    if(e.dialogType == DIALOG_START){
      setPlayerLock(true);
    }
  }

  void PlayerManager::notifyInputEvent(InputEvent &e){
    if(!playerLock){
      if(e.inputType == KEY_DOWN && e.keyValue == I_ACCEPT){
        entityManager->ex.entities.each<PlayerDialogComponent>([this](entityx::Entity entity, PlayerDialogComponent &comp) {
          entityx::ComponentHandle<PositionComponent> playerPos = this->player.component<PositionComponent>();
          entityx::ComponentHandle<PositionComponent> entityPos = entity.component<PositionComponent>();

          double x1 = playerPos.get()->pos.position.x;
          double x2 = entityPos.get()->pos.position.x;
          double y1 = playerPos.get()->pos.position.z;
          double y2 = entityPos.get()->pos.position.z;
          double distance = sqrt(pow(x2-x1, 2) + pow(y2-y1, 2));

          if(distance <= 100){
            // DialogEvent d;
            // d.dialogType = DIALOG_START;
            // d.dialogPath = comp.dialogPath;
            // d.startingBlock = comp.dialogBlock;
            // EventManager::transmitDialogEvent(d);
          }
        });
      }
    }
    if(e.inputType == KEY_DOWN && e.keyValue == I_DEBUG_TERMINAL){
      setPlayerLock(!playerLock);
    }
  }

  void PlayerManager::notifyStatsEvent(StatsEvent &e){
    if(e.statsType == STATS_OGRE_PLAYER_POS){
      positionCamera();
    }
  }

  SlotPosition PlayerManager::getPlayerSlotPosition(){
    entityx::ComponentHandle<PositionComponent> position = this->player.component<PositionComponent>();
    if(position){
      return position.get()->pos;
    }else{
      return {0, 0, Ogre::Vector3(0, 0, 0)};
    }
  }
}
