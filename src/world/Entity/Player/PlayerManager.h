#ifndef __PLAYER_MANAGER_H__
#define __PLAYER_MANAGER_H__

#include "entityx/entityx.h"
#include "../../Slots/SlotPosition.h"

#include "../../../EventManager/EventReceiver.h"

class btGhostObject;

namespace Ogre{
  class SceneNode;
}

namespace AV{
  class EntityManager;

  class PlayerManager : public EventReceiver{
  public:
    PlayerManager(EntityManager *entityManager);
    ~PlayerManager();

    void setPlayer(entityx::Entity entity);
    entityx::Entity getPlayer() { return player; };
    void setPlayerLock(bool lock);

    void notifyDialogEvent(DialogEvent &e);
    void notifyInputEvent(InputEvent &e);
    void notifyStatsEvent(StatsEvent &e);

    bool isPlayerMoving();

    SlotPosition getPlayerSlotPosition();

    void positionCamera();

    void update();

  private:
    EntityManager *entityManager;

    Ogre::SceneNode *playerNode = 0;

    entityx::Entity player;
    bool playerLock = false;
  };
}

#endif
