#pragma once

#include <map>
#include "../Slots/ESlotPosition.h"
#include "entityx/entityx.h"

#include "../../EventManager/EventReceiver.h"

namespace AV{
  class EntityManager;
  class EntityChunk;
  class ChunkRadiusLoader;

  class EntityChunkManager : public EventReceiver{
  public:
    EntityChunkManager(EntityManager *entityManager, ChunkRadiusLoader *chunkRadiusLoader);
    ~EntityChunkManager();

    void updateEntity(entityx::Entity e, ESlotPosition oldPos, ESlotPosition newPos);
    bool addEntity(entityx::Entity e);
    bool removeEntity(entityx::Entity e);

    void destroyChunk(ESlotPosition ePos);

    void notifyWorldEvent(WorldEvent &e);

    void destroyAllChunks();

  private:
    EntityManager *entityManager;
    ChunkRadiusLoader *chunkRadiusLoader;

    std::map<const int, EntityChunk*> entityChunks;
    //std::map<const ESlotPosition, EntityChunk*> entityChunks;

    bool _chunkLoaded(ESlotPosition pos);
  };
}
