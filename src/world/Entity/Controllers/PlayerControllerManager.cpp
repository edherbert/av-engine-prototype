#include "PlayerControllerManager.h"

#include "PlayerControllerStates/State.h"

#include "PlayerControllerStates/PlayerControllerStateStanding.h"
#include "PlayerControllerStates/PlayerControllerStateFalling.h"
#include "PlayerControllerStates/PlayerControllerStateWalking.h"
#include "PlayerControllerStates/PlayerControllerStateJumping.h"

#include "../EntityManager.h"
#include "ControllerActions.h"

#include "OgreVector3.h"

namespace AV{
  PlayerControllerManager::PlayerControllerManager(EntityManager *entityManager)
    : states{
      new PlayerControllerStateFalling(this),
      new PlayerControllerStateStanding(this, entityManager),
      new PlayerControllerStateWalking(this),
      new PlayerControllerStateJumping(this)
    }, entityManager(entityManager){

      setState(PLAYER_CONTROLLER_STATE_FALLING);
  }

  PlayerControllerManager::~PlayerControllerManager(){
    for(int i = 0; i < PLAYER_CONTROLLER_MANAGER_STATES_COUNT; i++){
      delete states[i];
    }
  }

  void PlayerControllerManager::update(entityx::Entity entity){
    currentState->update(entity);

    if(stateChangedCheck){
      stateChangedCheck = false;
      _stateChanged(entity);
    }
  }

  //A function which is run once each time the state changes.
  void PlayerControllerManager::_stateChanged(entityx::Entity entity){
    std::cout << _getStateName(currentStateId) << '\n';

    Ogre::String targetAnim = "IdleTop";
    Ogre::String targetStrings[4] = {"IdleTop", "IdleTop", "RunTop", "JumpStart"};

    // PLAYER_CONTROLLER_STATE_FALLING,
    // PLAYER_CONTROLLER_STATE_STANDING,
    // PLAYER_CONTROLLER_STATE_WALKING,
    // PLAYER_CONTROLLER_STATE_JUMPING,

    entityManager->setEntityAnimation(entity, targetStrings[currentStateId]);
  }

  Ogre::String PlayerControllerManager::_getStateName(PLAYER_CONTROLLER_STATE state){
    Ogre::String val = "Unknown";
    if(state == PLAYER_CONTROLLER_STATE_FALLING) val = "Falling";
    else if(state == PLAYER_CONTROLLER_STATE_JUMPING) val = "Jumping";
    else if(state == PLAYER_CONTROLLER_STATE_STANDING) val = "Standing";
    else if(state == PLAYER_CONTROLLER_STATE_WALKING) val = "Walking";

    return val;
  }

  // void PlayerControllerManager::performAction(entityx::Entity e, PLAYER_CONTROLLER_ACTION_TYPE action){
  //   currentState->action(e, action);
  // }

  void PlayerControllerManager::performAction(entityx::Entity e, PlayerControllerAction &action){
    currentState->action(e, action);
  }

  void PlayerControllerManager::move(entityx::Entity e, const Ogre::Vector3 &ammount){
    currentState->move(e, ammount);
  }

  void PlayerControllerManager::setState(PLAYER_CONTROLLER_STATE state){
    currentState = states[state];
    currentStateId = state;

    stateChangedCheck = true;
  }
}
