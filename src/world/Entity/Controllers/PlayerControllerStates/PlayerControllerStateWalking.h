#pragma once

#include "State.h"
#include "../ControllerActions.h"

namespace AV{
  class PlayerControllerStateWalking : public State{
  public:
    PlayerControllerStateWalking(PlayerControllerManager *manager) : State(manager, PLAYER_CONTROLLER_STATE_WALKING) { };

    void update(entityx::Entity e){
      entityx::ComponentHandle<RigidBodyComponent> body = e.component<RigidBodyComponent>();
      if(body){
        btRigidBody* rigidBody = body.get()->body;
        _updateRigidBody(rigidBody);

        btVector3 velocity = rigidBody->getLinearVelocity();
        if(velocity.x() == 0 && velocity.z() == 0){
          manager->setState(PLAYER_CONTROLLER_STATE_STANDING);
        }
        if(!_isOnGround(rigidBody->getWorldTransform().getOrigin())){
          manager->setState(PLAYER_CONTROLLER_STATE_FALLING);
        }
      }
    }

    void move(entityx::Entity e, const Ogre::Vector3 &ammount){
      entityx::ComponentHandle<RigidBodyComponent> body = e.component<RigidBodyComponent>();
      if(body){
        _moveRigidBody(body.get()->body, ammount);
      }
      _orientatePlayer(e, ammount);
    }

    void action(entityx::Entity e, PlayerControllerAction &action){
      if(action._type == PLAYER_CONTROLLER_ACTION_DATA_TYPE_JUMP){
        entityx::ComponentHandle<RigidBodyComponent> body = e.component<RigidBodyComponent>();
        if(body){
          _jump(body.get()->body);
          manager->setState(PLAYER_CONTROLLER_STATE_JUMPING);
        }
      }
      else if(action._type == PLAYER_CONTROLLER_ACTION_DATA_TYPE_SHOOT){

      }
    }

  };
}
