#pragma once

#include "State.h"
#include "../ControllerActions.h"

namespace AV{
  class PlayerControllerStateFalling : public State{
  public:
    PlayerControllerStateFalling(PlayerControllerManager *manager) : State(manager, PLAYER_CONTROLLER_STATE_FALLING) { };

    void update(entityx::Entity e){
      entityx::ComponentHandle<RigidBodyComponent> body = e.component<RigidBodyComponent>();
      if(body){
        btRigidBody* rigidBody = body.get()->body;
        _updateRigidBody(rigidBody);

        if(_isOnGround(rigidBody->getWorldTransform().getOrigin())){
          manager->setState(PLAYER_CONTROLLER_STATE_STANDING);
        }
      }
    }

    void move(entityx::Entity e, const Ogre::Vector3 &ammount){
      entityx::ComponentHandle<RigidBodyComponent> body = e.component<RigidBodyComponent>();
      if(body){
        _moveRigidBody(body.get()->body, ammount);
      }
      _orientatePlayer(e, ammount);
    }

    void action(entityx::Entity e, PlayerControllerAction &action){

    }
  };
}
