#pragma once

#include "State.h"
#include "../ControllerActions.h"
#include "../../EntityManager.h"
#include "../../Factory/EntityFactory.h"

namespace AV{
  class PlayerControllerStateStanding : public State{
  private:
    EntityManager* entityManager;

    int shootCooldown = 0;
  public:
    PlayerControllerStateStanding(PlayerControllerManager *manager, EntityManager *entityManager) : State(manager, PLAYER_CONTROLLER_STATE_STANDING)
    , entityManager(entityManager) { };

    void update(entityx::Entity e){
      entityx::ComponentHandle<RigidBodyComponent> body = e.component<RigidBodyComponent>();
      if(body){
        btRigidBody* rigidBody = body.get()->body;
        _updateRigidBody(rigidBody);
      }

      if(shootCooldown > 0) shootCooldown--;
    }

    void move(entityx::Entity e, const Ogre::Vector3 &ammount){
      entityx::ComponentHandle<RigidBodyComponent> body = e.component<RigidBodyComponent>();
      if(body){
        _moveRigidBody(body.get()->body, ammount);

        manager->setState(PLAYER_CONTROLLER_STATE_WALKING);
      }
    }

    void action(entityx::Entity e, PlayerControllerAction &action){
      if(action._type == PLAYER_CONTROLLER_ACTION_DATA_TYPE_JUMP){
        entityx::ComponentHandle<RigidBodyComponent> body = e.component<RigidBodyComponent>();
        if(body){
          _jump(body.get()->body);
          manager->setState(PLAYER_CONTROLLER_STATE_JUMPING);
        }
      }

      if(action._type == PLAYER_CONTROLLER_ACTION_DATA_TYPE_SHOOT){
        entityx::ComponentHandle<PositionComponent> posComp = e.component<PositionComponent>();
        if(posComp){
          PlayerControllerActionShoot shootAction = (PlayerControllerActionShoot&) action;
          Ogre::Vector3 direction = Ogre::Vector3(shootAction.direction.x, 0, shootAction.direction.y);

          //Shoot
          if(shootCooldown <= 0){
            entityManager->getEntityFactory()->createProjectile(posComp.get()->pos, direction);
            shootCooldown = 10;
          }
          
        }
      }
    }
  };
}
