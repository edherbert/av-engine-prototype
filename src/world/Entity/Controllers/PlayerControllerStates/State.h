#pragma once

#include "entityx/entityx.h"
#include "OgreVector3.h"

#include "../../components/RigidBodyComponent.h"
#include "../../components/OgreMeshComponent.h"
#include "../../../Physics/BulletManager.h"

#include "../ControllerActions.h"

namespace AV{
  enum PLAYER_CONTROLLER_STATE{
    PLAYER_CONTROLLER_STATE_FALLING,
    PLAYER_CONTROLLER_STATE_STANDING,
    PLAYER_CONTROLLER_STATE_WALKING,
    PLAYER_CONTROLLER_STATE_JUMPING,
  };

  enum PLAYER_CONTROLLER_ACTION_TYPE{
    PLAYER_CONTROLLER_ACTION_TYPE_JUMP,
    PLAYER_CONTROLLER_ACTION_TYPE_SHOOT
  };

  class PlayerControllerManager;

  class State{
  public:
    State(PlayerControllerManager *manager, PLAYER_CONTROLLER_STATE state) : manager(manager), stateId(state) { };
    ~State() { };

    virtual void update(entityx::Entity e){

    }

    virtual void move(entityx::Entity e, const Ogre::Vector3 &ammount){

    }

    virtual void action(entityx::Entity e, PlayerControllerAction &action){

    }

  protected:
    PLAYER_CONTROLLER_STATE stateId;
    PlayerControllerManager* manager;

    void _updateRigidBody(btRigidBody *body){
        btVector3 currentVel = body->getLinearVelocity();

        const float dither = 0.9;
        currentVel = btVector3(currentVel.getX() * dither, currentVel.getY(), currentVel.getZ() * dither);

        if(body->getLinearVelocity().getY() > 0){
          int ammount = -5.5;
          currentVel.setY(currentVel.getY() + ammount);
        }

        if(currentVel.x() >= -0.01 && currentVel.x() <= 0.01) currentVel.setX(0);
        if(currentVel.z() >= -0.01 && currentVel.z() <= 0.01) currentVel.setZ(0);
        body->setLinearVelocity(currentVel);
    }

    void _jump(btRigidBody *body){
      int maxJumpVel = 90;

      btVector3 currentVel = body->getLinearVelocity();
      currentVel.setY(maxJumpVel);

      body->setLinearVelocity(currentVel);
    }

    void _orientatePlayer(entityx::Entity e, Ogre::Vector3 direction){
      entityx::ComponentHandle<OgreMeshComponent> mesh = e.component<OgreMeshComponent>();
      if(mesh){
        Ogre::Vector3 normalisedAmmount = direction.normalisedCopy();
        Ogre::Real radian = atan2(normalisedAmmount.x, normalisedAmmount.z);

        Ogre::Quaternion quatFirst = mesh.get()->parentNode->getOrientation();
        Ogre::Quaternion quatSecond = Ogre::Quaternion(Ogre::Degree(Ogre::Radian(radian)), Ogre::Vector3(0, 1, 0));

        mesh.get()->parentNode->setOrientation(Ogre::Quaternion::Slerp(0.2, quatFirst, quatSecond, true));
      }
    }

    void _moveRigidBody(btRigidBody *body, const Ogre::Vector3 &ammount){
      int maxVel = 30;
      int increment = 10;
      int maxJumpVel = 90;

      btVector3 currentVel = btVector3(ammount.x, ammount.y, ammount.z) + body->getLinearVelocity();

      // if(body->getLinearVelocity().getY() > 0){
      //   int ammount = -5.5;
      //   currentVel.setY(currentVel.getY() + ammount);
      // }

      if(currentVel.getY() > maxJumpVel) currentVel.setY(maxJumpVel);
      if(currentVel.getY() < -maxJumpVel) currentVel.setY(-maxJumpVel);

      if(currentVel.getX() > maxVel) currentVel.setX(maxVel);
      if(currentVel.getX() < -maxVel) currentVel.setX(-maxVel);

      if(currentVel.getZ() > maxVel) currentVel.setZ(maxVel);
      if(currentVel.getZ() < -maxVel) currentVel.setZ(-maxVel);


      body->setActivationState(ACTIVE_TAG);
      body->setLinearVelocity(currentVel);
    }

    bool _isOnGround(btVector3 playerPos){
      // btVector3 btFrom(playerPos.getX(), playerPos.getY()-0.5, playerPos.getZ());
      // btVector3 btTo(playerPos.getX(), -10000.0, playerPos.getZ());
      //
      // btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);
      //
      // BulletManager::world->rayTest(btFrom, btTo, res);
      //
      // bool ground = false;
      // if(res.hasHit()){
      //   if(playerPos.getY() - res.m_hitPointWorld.getY() <= 6.5){
      //     ground = true;
      //   }
      // }
      //
      // return ground;

      btVector3 btFrom(playerPos.getX(), playerPos.getY()-0.5, playerPos.getZ());
      btVector3 btTo(playerPos.getX(), -10000.0, playerPos.getZ());

      btCollisionWorld::ClosestRayResultCallback res = BulletManager::rayCastWorld(btFrom, btTo);

      bool ground = false;
      if(res.hasHit()){
        if(playerPos.getY() - res.m_hitPointWorld.getY() <= 6.5){
          ground = true;
        }
      }

      return ground;
    }
  };
}
