#pragma once

#include "OgreVector2.h"

namespace AV{
  enum PLAYER_CONTROLLER_ACTION_DATA_TYPES{
    PLAYER_CONTROLLER_ACTION_DATA_TYPE_NULL,
    PLAYER_CONTROLLER_ACTION_DATA_TYPE_JUMP,
    PLAYER_CONTROLLER_ACTION_DATA_TYPE_SHOOT,
  };

  struct PlayerControllerAction{
    PlayerControllerAction(PLAYER_CONTROLLER_ACTION_DATA_TYPES type) : _type(type) { }
    PlayerControllerAction() : _type(PLAYER_CONTROLLER_ACTION_DATA_TYPE_NULL) { }

    PLAYER_CONTROLLER_ACTION_DATA_TYPES _type;
  };

  struct PlayerControllerActionJump : public PlayerControllerAction{
    PlayerControllerActionJump() : PlayerControllerAction(PLAYER_CONTROLLER_ACTION_DATA_TYPE_JUMP) { }
  };

  struct PlayerControllerActionShoot : public PlayerControllerAction{
    PlayerControllerActionShoot(Ogre::Vector2 direction) : PlayerControllerAction(PLAYER_CONTROLLER_ACTION_DATA_TYPE_SHOOT),
    direction(direction) { }

    Ogre::Vector2 direction = Ogre::Vector2::ZERO;
  };
}
