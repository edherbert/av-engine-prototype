#include "ControllerManager.h"

#include "../components/MetaComponent.h"
#include "../components/RigidBodyComponent.h"

#include "PlayerControllerManager.h"
#include "ControllerActions.h"

#include "../../Physics/BulletManager.h"

namespace AV{
  PlayerControllerManager* ControllerManager::playerControllerManager = 0;

  const Ogre::IdString ControllerManager::IdJump = Ogre::IdString("Jump");

  ControllerManager::ControllerManager(){

  }

  ControllerManager::~ControllerManager(){

  }

  void ControllerManager::setup(EntityManager *entityManager){
    if(playerControllerManager != 0) return;

    ControllerManager::playerControllerManager = new PlayerControllerManager(entityManager);
  }

  SlotPosition ControllerManager::moveSlotPosition(const SlotPosition& pos, const Ogre::Vector3& ammount, CONTROLLER_TYPE type, entityx::Entity entity){
    SlotPosition newPos = pos;

    if(type == CONTROLLER_TYPE_ABSOLUTE) _moveAbsolutePosition(newPos, ammount);
    else if(type == CONTROLLER_TYPE_PHYSICS) _movePhysicsController(newPos, ammount, entity);
    else if(type == CONTROLLER_TYPE_PLAYER) _movePlayerController(newPos, ammount, entity);

    return newPos;
  }

  //void ControllerManager::performCommand(entityx::Entity entity, Ogre::IdString command, CONTROLLER_TYPE type){
  void ControllerManager::performCommand(entityx::Entity entity, PlayerControllerAction &action, CONTROLLER_TYPE type){
    if(!playerControllerManager) return;
    if(type == CONTROLLER_TYPE_PLAYER){
      //PLAYER_CONTROLLER_ACTION_TYPE t;
      //if(command == IdJump) t = PLAYER_CONTROLLER_ACTION_TYPE_JUMP;

      playerControllerManager->performAction(entity, action);
    }
  }

  void ControllerManager::_moveAbsolutePosition(SlotPosition &pos, const Ogre::Vector3 &ammount){
    pos.move(ammount);
  }

  void ControllerManager::updatePlayerController(entityx::Entity entity){
    if(!playerControllerManager) return;
    playerControllerManager->update(entity);
  }

  void ControllerManager::_movePlayerController(SlotPosition &pos, const Ogre::Vector3 &ammount, entityx::Entity entity){
    if(!playerControllerManager) return;
    playerControllerManager->move(entity, ammount);
  }

  void ControllerManager::updatePhysicsController(entityx::Entity entity){
    entityx::ComponentHandle<RigidBodyComponent> body = entity.component<RigidBodyComponent>();
    if(body){
      btRigidBody* pBody = body.get()->body;
      btVector3 currentVel = pBody->getLinearVelocity();

      const float dither = 0.9;
      currentVel = btVector3(currentVel.getX() * dither, currentVel.getY(), currentVel.getZ() * dither);
      pBody->setLinearVelocity(currentVel);
    }
  }

  void ControllerManager::_movePhysicsController(SlotPosition &pos, const Ogre::Vector3 &ammount, entityx::Entity entity){
    entityx::ComponentHandle<RigidBodyComponent> body = entity.component<RigidBodyComponent>();
    if(body){
      int maxVel = 30;
      int increment = 10;
      int maxJumpVel = 90;

      btRigidBody *playerRigidBody = body.get()->body;

      //Check if the player is already moving.
      //If they are do a bit of a spirt when they start.

      // int x, y, z;
      // x = y = z = 0;
      // if(Input::getInput(I_UP)) z = increment;
      // if(Input::getInput(I_DOWN)) z = -increment;
      // if(Input::getInput(I_LEFT)) x = increment;
      // if(Input::getInput(I_RIGHT)) x = -increment;
      //
      // if(Input::getInput(I_JUMP) && isOnGround()){
      //   y = maxJumpVel;
      // }

      //std::cout << ammount << '\n';

      btVector3 currentVel = btVector3(ammount.x, ammount.y, ammount.z) + playerRigidBody->getLinearVelocity();

      // const float dither = 0.7;
      // if(ammount.x == 0 && ammount.z == 0){
      //   currentVel = btVector3(currentVel.getX() * dither, currentVel.getY(), currentVel.getZ() * dither);
      // }
      if(playerRigidBody->getLinearVelocity().getY() > 0){
        int ammount = -5.5;
        currentVel.setY(currentVel.getY() + ammount);
      }

      if(currentVel.getY() > maxJumpVel) currentVel.setY(maxJumpVel);
      if(currentVel.getY() < -maxJumpVel) currentVel.setY(-maxJumpVel);

      if(currentVel.getX() > maxVel) currentVel.setX(maxVel);
      if(currentVel.getX() < -maxVel) currentVel.setX(-maxVel);

      if(currentVel.getZ() > maxVel) currentVel.setZ(maxVel);
      if(currentVel.getZ() < -maxVel) currentVel.setZ(-maxVel);


      playerRigidBody->setActivationState(ACTIVE_TAG);
      playerRigidBody->setLinearVelocity(currentVel);

      //playerPos = playerRigidBody->getWorldTransform().getOrigin();

      if(currentVel.x() > 0 || currentVel.z() > 0){
        //moving
      }
    }
  }

  bool ControllerManager::_isOnGround(btVector3 playerPos){
    btVector3 btFrom(playerPos.getX(), playerPos.getY()-0.5, playerPos.getZ());
    btVector3 btTo(playerPos.getX(), -10000.0, playerPos.getZ());

    btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);

    BulletManager::world->rayTest(btFrom, btTo, res);

    bool ground = false;
    if(res.hasHit()){
      if(playerPos.getY() - res.m_hitPointWorld.getY() <= 6.5){
        ground = true;
      }
    }

    return ground;
  }
}
