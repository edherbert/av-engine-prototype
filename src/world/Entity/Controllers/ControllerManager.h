#pragma once

#include "../../Slots/SlotPosition.h"

#include "entityx/entityx.h"
#include "OgreIdString.h"

namespace AV{
  enum CONTROLLER_TYPE : unsigned int;

  class PlayerControllerManager;
  class EntityManager;

  struct PlayerControllerAction;

  class ControllerManager{
  public:
    ControllerManager();
    ~ControllerManager();

    static void setup(EntityManager *entityManager);

    static SlotPosition moveSlotPosition(const SlotPosition& pos, const Ogre::Vector3& ammount, CONTROLLER_TYPE type, entityx::Entity entity);
    static void updatePhysicsController(entityx::Entity entity);
    static void updatePlayerController(entityx::Entity entity);
    //static void performCommand(entityx::Entity entity, Ogre::IdString command, CONTROLLER_TYPE type);
    static void performCommand(entityx::Entity entity, PlayerControllerAction &action, CONTROLLER_TYPE type);

  private:
    static void _moveAbsolutePosition(SlotPosition &pos, const Ogre::Vector3 &ammount);
    static void _movePhysicsController(SlotPosition &pos, const Ogre::Vector3 &ammount, entityx::Entity entity);
    static void _movePlayerController(SlotPosition &pos, const Ogre::Vector3 &ammount, entityx::Entity entity);

    static bool _isOnGround(btVector3 playerPos);

    static const Ogre::IdString IdJump;

    static PlayerControllerManager* playerControllerManager;
  };
}
