#pragma once

#include "PlayerControllerStates/State.h"

#define PLAYER_CONTROLLER_MANAGER_STATES_COUNT 4

namespace Ogre{
  class Vector3;
}

namespace AV{
  class EntityManager;

  class PlayerControllerAction;

  class PlayerControllerManager{
  public:
    PlayerControllerManager(EntityManager *entityManager);
    ~PlayerControllerManager();

    //void performAction(entityx::Entity e, PLAYER_CONTROLLER_ACTION_TYPE action);
    void performAction(entityx::Entity e, PlayerControllerAction &action);

    void update(entityx::Entity entity);
    void move(entityx::Entity entity, const Ogre::Vector3 &ammount);
    void setState(PLAYER_CONTROLLER_STATE state);

  private:
    Ogre::String _getStateName(PLAYER_CONTROLLER_STATE state);
    void _stateChanged(entityx::Entity entity);

    State* states[PLAYER_CONTROLLER_MANAGER_STATES_COUNT];
    State* currentState = 0;
    PLAYER_CONTROLLER_STATE currentStateId;

    bool stateChangedCheck = false;

    EntityManager *entityManager;
  };
}
