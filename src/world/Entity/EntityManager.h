#ifndef __ENTITY_MANAGER_H__
#define __ENTITY_MANAGER_H__

#include "entityx/entityx.h"
#include "OgreVector3.h"

#include "../Slots/SlotManager.h"
#include "components/MetaComponent.h"

class EntityScripts;

namespace AV{
  class World;
  class InputEvent;
  class PlayerManager;
  class EntityChunk;
  class EntityMeshManager;
  class EntityChunkManager;
  class EntityFileParser;
  class EntityFactory;
  class AiManager;
  class DamageInflictor;

  enum CONTROLLER_TYPE : unsigned int;

  struct PlayerControllerAction;

  class EntityManager{
  friend EntityScripts;
  friend PlayerManager;
  public:
    EntityManager(World *world);
    ~EntityManager();

    entityx::Entity createEntity(int chunkX, int chunkY, Ogre::Vector3 position, bool bind = true);
    entityx::Entity createEntity(SlotPosition pos, bool bind = true);
    void clearEntities();
    void destroyEntity(entityx::Entity entity);
    void destroyEntity(entityx::Entity::Id entityId);

    void setPlayerPosition(SlotPosition position);
    void setEntityPosition(entityx::Entity entity, SlotPosition position);
    void setEntityChunk(entityx::Entity entity, SlotPosition oldPos, SlotPosition newPos);

    entityx::Entity getEntity(entityx::Entity::Id id);
    void moveEntity(entityx::Entity entity, const Ogre::Vector3 &ammount);

    void setEntityController(entityx::Entity entity, CONTROLLER_TYPE id);

    CONTROLLER_TYPE getEntityController(entityx::Entity entity);

    void sendEntityControllerCommand(entityx::Entity entity, PlayerControllerAction &action);

    void shiftEntityOrigin();

    Ogre::String getCurrentMapName();

    bool testVal = false;

    void loadEntityChunkFile(Ogre::String mapName, int chunkX, int chunkY);
    Ogre::Real distanceBetweenEntities(entityx::Entity f, entityx::Entity s);
    entityx::Entity getPlayer();

    void update();

    PlayerManager* getPlayerManager() { return playerManager; };
    EntityFactory* getEntityFactory() { return entityFactory; };

    void setEntityAnimation(entityx::Entity entity, const Ogre::String &animName);

    entityx::Entity tempEntity;

  private:
    World *world;
    entityx::EntityX ex;
    PlayerManager *playerManager;
    EntityChunk *entityChunk;
    EntityMeshManager *entityMeshManager;
    EntityChunkManager *entityChunkManager;
    EntityFileParser *entityFileParser;
    EntityFactory *entityFactory;
    AiManager *aiManager;
    DamageInflictor *damageInflictor;

    entityx::ComponentHandle<MetaComponent> _checkForEntityMeta(entityx::Entity entity);

    template <typename t>
    entityx::ComponentHandle<t> getComponent(entityx::Entity::Id id){
      ex.entities.get(id);
    }
  };
}
#endif
