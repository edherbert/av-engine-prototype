#include "EntityChunkManager.h"

#include "EntityManager.h"
#include "../ChunkRadiusLoader.h"
#include "components/PositionComponent.h"
#include "EntityChunk.h"

#include "../../EventManager/EventManager.h"
#include "../../EventManager/Events/WorldEvent.h"

namespace AV{
  EntityChunkManager::EntityChunkManager(EntityManager *entityManager, ChunkRadiusLoader *chunkRadiusLoader)
    : entityManager(entityManager),
      chunkRadiusLoader(chunkRadiusLoader){

    EventManager::subscribeToEvent(this, EVENT_TYPE_WORLD);
  }

  EntityChunkManager::~EntityChunkManager(){

  }

  void EntityChunkManager::destroyChunk(ESlotPosition ePos){
    EntityChunk* chunk = entityChunks[ePos.getId()];

    if(chunk != 0){
      chunk->destroyChunk(entityManager);
    }else{
      std::cout << "That chunk couldn't be destroyed as it does not exist." << '\n';
    }
  }

  void EntityChunkManager::updateEntity(entityx::Entity e, ESlotPosition oldPos, ESlotPosition newPos){
    if(oldPos == newPos) return;

    EntityChunk *oldChunk = entityChunks[oldPos.getId()];

    if(!oldChunk){
      std::cout << "Chunk issue" << '\n';
      //chunkRadiusLoader->eChunkValid(ESlotPosition(0, 0, 0, 0));
      return;
    }
    bool containsEntity = oldChunk->containsEntity(e);
    if(!containsEntity){
      //For some reason the entity isn't in the old chunk.
      //This most likely means the entity isn't tracked, so don't bother checking it.
      return;
    }

    EntityChunk *newChunk = entityChunks[newPos.getId()];
    if(!newChunk){
      //Create a new chunk if the destination of the entity couldn't be found.
      newChunk = new EntityChunk();

      newChunk->addEntity(e);

      entityChunks[newPos.getId()] = newChunk;

      // if(oldChunk) oldChunk->removeEntity(e);
      // std::cout << "Destroying entity" << '\n';
      // entityManager->destroyEntity(e);
      // return;
    }

    entityChunks[oldPos.getId()]->removeEntity(e);
    entityChunks[newPos.getId()]->addEntity(e);
  }

  bool EntityChunkManager::addEntity(entityx::Entity e){
    //std::cout << "Binding entity." << '\n';
    //chunkRadiusLoader->eChunkValid(ESlotPosition(0, 0, 0, 0));

    entityx::ComponentHandle<PositionComponent> position = e.component<PositionComponent>();
    if(position){
      ESlotPosition ePos = position.get()->pos.toESlot();

      // if(chunkRadiusLoader->eChunkValid(ePos)){
      //   std::cout << "Valid" << '\n';
      // }

      //Check if the chunk exists in the list.
      //If it does
      if(_chunkLoaded(ePos)){
        std::cout << "Chunk is loaded." << '\n';

        entityChunks[ePos.getId()]->addEntity(e);
      }else {
        std::cout << "Chunk is not loaded, creating it." << '\n';
        EntityChunk *entityChunk = new EntityChunk();

        entityChunk->addEntity(e);

        //entityChunks.insert(ePos.getId(), entityChunk);
        std::cout << ePos.getId() << '\n';
        entityChunks[ePos.getId()] = entityChunk;
      }
    }

    return false;
  }

  bool EntityChunkManager::_chunkLoaded(ESlotPosition pos){
    std::map<const int, EntityChunk*>::iterator it;
    it = entityChunks.find(pos.getId());

    return !(it == entityChunks.end());
  }

  bool EntityChunkManager::removeEntity(entityx::Entity e){
    entityx::ComponentHandle<PositionComponent> position = e.component<PositionComponent>();
    if(position){
      ESlotPosition ePos = position.get()->pos.toESlot();

      EntityChunk *entityChunk = entityChunks[ePos.getId()];
      return entityChunk->removeEntity(e);
    }

    return false;
  }

  void EntityChunkManager::destroyAllChunks(){
    for(auto const& [key, value] : entityChunks){
      if(value == 0) continue;
      value->destroyChunk(entityManager);
      delete value;
    }
    entityChunks.clear();
  }

  void EntityChunkManager::notifyWorldEvent(WorldEvent &e){
    if(e.eventType == WORLD_EVENT_E_CHUNK_UNLOADED){
      destroyChunk(e.eSlot);
    }
    if(e.eventType == WORLD_EVENT_MAP_CHANGED){
      destroyAllChunks();
    }
  }
}
