#include "EntityMeshManager.h"

#include "Ogre.h"
#include <iostream>

namespace AV{
  Ogre::SceneManager* EntityMeshManager::sceneManager = 0;
  Ogre::SceneNode* EntityMeshManager::entityNode = 0;

  EntityMeshManager::EntityMeshManager(Ogre::SceneManager *sceneManager){
    EntityMeshManager::sceneManager = sceneManager;

    EntityMeshManager::entityNode = sceneManager->getRootSceneNode()->createChildSceneNode(Ogre::SCENE_DYNAMIC);
  }

  EntityMeshManager::~EntityMeshManager(){

  }

  Ogre::SceneNode* EntityMeshManager::createOgreMesh(Ogre::String meshName){
    Ogre::SceneNode *node = EntityMeshManager::entityNode->createChildSceneNode(Ogre::SCENE_DYNAMIC);
    Ogre::Item *item = sceneManager->createItem(meshName, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
    node->attachObject((Ogre::MovableObject*)item);

    return node;
  }

  void EntityMeshManager::destroyOgreMesh(Ogre::SceneNode* sceneNode){
    if(!sceneNode){
      std::cout << "Empty scene node" << '\n';
      return;
    }
    Ogre::MovableObject* object = sceneNode->getAttachedObject(0);
    delete object;

    sceneNode->removeAndDestroyAllChildren();
    delete sceneNode;
  }

  void EntityMeshManager::removeAndDestroyMeshes(){
    entityNode->removeAndDestroyAllChildren();
  }

  void EntityMeshManager::attachNode(Ogre::SceneNode *sceneNode){
    entityNode->addChild(sceneNode);
  }

}
