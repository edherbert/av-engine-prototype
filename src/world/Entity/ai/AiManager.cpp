#include "AiManager.h"

#include "../../../util/BrainTree.h"
#include <iostream>
#include "OgreVector3.h"
#include "../EntityManager.h"

#include "../components/PositionComponent.h"

namespace AV{
  EntityManager* AiManager::entityManager = 0;

  class MoveToPlayer : public BrainTree::Leaf
  {
  public:
      MoveToPlayer(entityx::Entity e) : e(e) {}
      Status update() override
      {
          entityx::Entity p = AiManager::entityManager->getPlayer();
          entityx::ComponentHandle<PositionComponent> pComp = p.component<PositionComponent>();
          entityx::ComponentHandle<PositionComponent> eComp = e.component<PositionComponent>();

          if(pComp && eComp){
            Ogre::Vector3 f = pComp.get()->pos.toOgre();
            Ogre::Vector3 s = eComp.get()->pos.toOgre();

            Ogre::Vector3 diff = (f - s).normalisedCopy();

            AiManager::entityManager->moveEntity(e, diff * 0.3);

            Ogre::Real distance = AiManager::entityManager->distanceBetweenEntities(AiManager::entityManager->getPlayer(), e);
            //return Node::Status::Success;
            if(distance <= 5)
              return Node::Status::Success;
            else if(distance >= 30)
              return Node::Status::Failure;
            else
              return Node::Status::Running;
          }
          return Node::Status::Failure;
      }
    private:
      entityx::Entity e;
  };

  class DoneAction : public BrainTree::Node
  {
  public:
    DoneAction() {}

    Status update() override{
      std::cout << "Done!" << '\n';
      return Node::Status::Success;
    }
  };

  class CloseToPlayer : public BrainTree::Node
  {
  public:
    CloseToPlayer(int targetDistance, entityx::Entity e) : targetDistance(targetDistance), e(e) {}
    Status update() override
    {
        //std::cout << "Hello, World! " << distance << std::endl;
        //AiManager::entityManager->moveEntity(AiManager::e, Ogre::Vector3(0.6, 0, 0));
        Ogre::Real distance = AiManager::entityManager->distanceBetweenEntities(AiManager::entityManager->getPlayer(), e);
        if(distance <= targetDistance)
          return Node::Status::Success;
        else
          return Node::Status::Failure;
    }
  private:
    int targetDistance = 0;
    entityx::Entity e;
  };

  AiManager::AiManager(EntityManager *entityManager){
    AiManager::entityManager = entityManager;

    // auto sequence = std::make_shared<BrainTree::Sequence>();
    // auto closeToPlayer = std::make_shared<CloseToPlayer>(30);
    // auto move = std::make_shared<MoveToPlayer>();
    // auto done = std::make_shared<DoneAction>();
    //
    // sequence->addChild(closeToPlayer);
    // sequence->addChild(move);
    // sequence->addChild(done);
  }

  BrainTree::BehaviorTree* AiManager::createTree(entityx::Entity e){
    // BrainTree::BehaviorTree *tree = new BrainTree::BehaviorTree();
    // auto sequence = std::make_shared<BrainTree::Sequence>();
    // auto firstHello = std::make_shared<printHello>(e);
    // sequence->addChild(firstHello);
    //
    // tree->setRoot(sequence);
    //
    // return tree;

    BrainTree::BehaviorTree *tree = new BrainTree::BehaviorTree();
    //BrainTree::BehaviorTree tree;
    auto sequence = std::make_shared<BrainTree::Sequence>();
    auto closeToPlayer = std::make_shared<CloseToPlayer>(30, e);
    auto move = std::make_shared<MoveToPlayer>(e);
    auto done = std::make_shared<DoneAction>();

    sequence->addChild(closeToPlayer);
    sequence->addChild(move);
    sequence->addChild(done);

    tree->setRoot(sequence);

    return tree;
  }

  AiManager::~AiManager(){

  }
}
