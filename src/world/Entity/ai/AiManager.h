#pragma once

#include "entityx/entityx.h"

namespace BrainTree{
  class BehaviorTree;
}

namespace AV{
  class EntityManager;

  class AiManager{
  public:
    AiManager(EntityManager *entityManager);
    ~AiManager();

    BrainTree::BehaviorTree* createTree(entityx::Entity e);

    static EntityManager *entityManager;
  private:
  };
}
