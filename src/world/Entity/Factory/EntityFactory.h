#pragma once

#include "OgreString.h"
#include "OgreVector3.h"
#include "../../Slots/SlotPosition.h"
#include "entityx/entityx.h"

class btCollisionObject;

namespace AV{
  class EntityManager;

  enum ComponentIds{
    ACTION_TRIGGER_COMPONENT,
    HEALTH_COMPONENT,
    MOVEMENT_COMPONENT,
    OGRE_MESH_COMPONENT,
    PLAYER_DIALOG_COMPONENT,
    POSITION_COMPONENT,
    RIGID_BODY_COMPONENT
  };

  class EntityFactory{
  public:
    EntityFactory(EntityManager *entityManager);
    ~EntityFactory();

    bool createEntityArchetype(const Ogre::String &name);

    entityx::Entity createProjectile(SlotPosition pos, Ogre::Vector3 direction);

  private:
    EntityManager *entityManager;

    ComponentIds getComponentId(Ogre::String &id);
    void constructComponent(Ogre::String &line, ComponentIds id);

    void createComponent(Ogre::String &line);
  };
}
