#include "EntityFactory.h"
#include "OgreStringConverter.h"

#include <iostream>

#include "HealthComponentFactory.h"

#include "../components/OgreMeshComponent.h"
#include "../components/LifespanComponent.h"
#include "../components/MovementComponent.h"
#include "../components/DamageTriggerComponent.h"
#include "../EntityManager.h"

#include "../../Physics/DamageWorldManager.h"

#include "btBulletCollisionCommon.h"

#include "../Damage/AfflictionData.h"

namespace AV{
  EntityFactory::EntityFactory(EntityManager *entityManager)
    : entityManager(entityManager){

      //createEntity("test");

      createProjectile(SlotPosition(0, 0, Ogre::Vector3(50, 0, 50)), Ogre::Vector3::ZERO);
  }

  EntityFactory::~EntityFactory(){

  }

  entityx::Entity EntityFactory::createProjectile(SlotPosition pos, Ogre::Vector3 direction){
    entityx::Entity e = entityManager->createEntity(pos);

    Ogre::SceneNode *node = EntityMeshManager::createOgreMesh("ogrehead2.mesh");
    node->setPosition(50, 50, 50);
    node->setScale(0.1, 0.1, 0.1);

    e.assign<OgreMeshComponent>(node);
    e.assign<LifespanComponent>(100);
    e.assign<MovementComponent>(1, direction);

    DamageData *data = new DamageData();
    data->attackPower = 10;
    data->destroyOnDamage = true;

    btCollisionObject* entityObject = DamageWorldManager::createDamageSender(Ogre::Vector3(50, 0, 50), data, new btBoxShape(btVector3(5, 5, 5)), e.id());
    e.assign<DamageTriggerComponent>(entityObject);

    return e;
  }

  bool EntityFactory::createEntityArchetype(const Ogre::String &name){
    //For now just try and find the file describing that entity.
    Ogre::String path = "../resources/entities/" + name + ".txt";

    std::string line;
    std::ifstream myfile(path);
    if (myfile.is_open()){
      std::cout << "Reading entity file." << '\n';

      while ( getline (myfile,line) ){
        std::cout << line << '\n';

        createComponent(line);
      }
    }else{
      std::cout << "An entity archetype with that name couldn't be found." << '\n';
    }

    myfile.close();
    return true;
  }

  void EntityFactory::createComponent(Ogre::String &line){
    Ogre::String id = line.substr(0, line.find(' '));

    ComponentIds componentId = getComponentId(id);

    constructComponent(line, componentId);
  }

  void EntityFactory::constructComponent(Ogre::String &line, ComponentIds id){
    switch(id){
      case HEALTH_COMPONENT:
        HealthComponentFactory::constructComponent(line);
        break;
    }
  }

  ComponentIds EntityFactory::getComponentId(Ogre::String &id){
    int done = Ogre::StringConverter::parseInt(id);
    return (ComponentIds)done;
  }
}
