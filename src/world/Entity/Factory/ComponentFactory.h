#pragma once

#include "OgreString.h"

namespace AV{
  class ComponentFactory{
  public:
    ComponentFactory() { }
    ~ComponentFactory() { }

    virtual bool constructComponent(Ogre::String &line);
  };
}
