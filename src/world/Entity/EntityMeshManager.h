#ifndef __ENTITY_MESH_MANAGER_H__
#define __ENTITY_MESH_MANAGER_H__

#include "OgreString.h"

namespace Ogre{
  class SceneNode;
}

namespace AV{
  class EntityMeshManager{
  public:
    EntityMeshManager(Ogre::SceneManager *sceneManager);
    ~EntityMeshManager();

    static Ogre::SceneNode* createOgreMesh(Ogre::String meshName);
    static void removeAndDestroyMeshes();
    static void attachNode(Ogre::SceneNode *sceneNode);
    static void destroyOgreMesh(Ogre::SceneNode* sceneNode);

  private:
    static Ogre::SceneManager *sceneManager;
    static Ogre::SceneNode* entityNode;
  };
}

#endif
