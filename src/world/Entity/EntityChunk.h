#pragma once

#include <set>
#include "entityx/entityx.h"

namespace AV{
  class EntityManager;

  class EntityChunk{
  public:
    EntityChunk();
    ~EntityChunk();

    void addEntity(entityx::Entity e);
    bool removeEntity(entityx::Entity e);

    void destroyChunk(EntityManager *entityManager);

    bool containsEntity(entityx::Entity e);

  private:
    std::set<entityx::Entity> entities;
  };
}
