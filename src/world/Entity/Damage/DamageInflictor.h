#pragma once

#include "entityx/entityx.h"
#include "../components/CombatStatsComponent.h"
#include "AfflictionData.h"

namespace AV{
  class EntityManager;
  class AfflictionComponent;

  class DamageInflictor{
  public:
    DamageInflictor(EntityManager *entityManager);
    ~DamageInflictor();

    //static const AfflictionData afflictionDataZero;
    static const CombatStats combatStatsZero;

    static bool damageEntity(entityx::Entity entity, int ammount);
    static bool damageEntity(entityx::Entity entity, DamageData &data);
    static void processAffliction(entityx::Entity entity, AfflictionComponent &comp);

  private:
    static EntityManager *entityManager;
  };
}
