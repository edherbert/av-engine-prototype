#include "DamageInflictor.h"

#include "../components/HealthComponent.h"
#include "../components/AfflictionComponent.h"
#include "../EntityManager.h"

namespace AV{
  EntityManager* DamageInflictor::entityManager = 0;
  //const AfflictionData DamageInflictor::afflictionDataZero = {AFFLICTION_TYPE_NONE, 0};
  const CombatStats DamageInflictor::combatStatsZero = {0, 0};

  DamageInflictor::DamageInflictor(EntityManager *entityManager){
    DamageInflictor::entityManager = entityManager;
  }

  DamageInflictor::~DamageInflictor(){

  }

  bool DamageInflictor::damageEntity(entityx::Entity entity, DamageData &data){
    const CombatStats* stats = &combatStatsZero;
    entityx::ComponentHandle<CombatStats> statsComp = entity.component<CombatStats>();
    if(statsComp){
      stats = statsComp.get();
    }

    //For now I'm going to say that the closer the entity's defence is to 10 the less damage the attack will do.
    //So 10 defence means nothing gets through.
    int attackAmmount = data.attackPower - stats->defence;
    damageEntity(entity, attackAmmount);

  }

  bool DamageInflictor::damageEntity(entityx::Entity entity, int ammount){
    if(ammount <= 0) return false;

    entityx::ComponentHandle<HealthComponent> healthComp = entity.component<HealthComponent>();
    if(!healthComp) return false;

    int currentHealth = healthComp.get()->health;
    currentHealth -= ammount;
    healthComp.get()->health = currentHealth;

    std::cout << "New Health " << currentHealth << '\n';

    if(currentHealth <= 0){
      //The entity needs to be nuked.
      if(entityManager) entityManager->destroyEntity(entity);
    }

    return true;
  }

  void DamageInflictor::processAffliction(entityx::Entity entity, AfflictionComponent &comp){
    damageEntity(entity, comp.afflictionData.afflictionValue);
  }
}
