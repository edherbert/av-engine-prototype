#pragma once

namespace AV{
  enum AfflictionType{
    AFFLICTION_TYPE_NONE,
    AFFLICTION_TYPE_FIRE,
    AFFLICTION_TYPE_ICE,
  };

  struct AfflictionData{
    AfflictionType type = AFFLICTION_TYPE_NONE;
    int afflictionValue = 0;

  };

  //static const AfflictionData afflictionDataZero;

  struct DamageData{
    int attackPower = 1;
    bool destroyOnDamage = false;

    AfflictionData afflictionData = {AFFLICTION_TYPE_NONE, 0};
  };
}
