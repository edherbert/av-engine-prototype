#include "EntityChunk.h"

#include "EntityManager.h"

namespace AV{
  EntityChunk::EntityChunk(){

  }

  EntityChunk::~EntityChunk(){

  }

  void EntityChunk::addEntity(entityx::Entity e){
    entities.insert(e);
  }

  bool EntityChunk::removeEntity(entityx::Entity e){
    // std::set<entityx::Entity>::Iterator it = entities.begin();
    //
    // while(it != entities.end()){
    //
    // }

    std::set<entityx::Entity>::iterator it = entities.find(e);
    if(it != entities.end()){
      entities.erase(it);
      return true;
    }else return false;
  }

  bool EntityChunk::containsEntity(entityx::Entity e){
    //If there is more than one instance of this entity in the set it must be contained.
    return entities.count(e) > 0;

    // std::set<entityx::Entity>::iterator it = entities.find(e);
    // if(it == entities.end()) return false;
    // else return true;
    //return value;
  }

  void EntityChunk::destroyChunk(EntityManager *entityManager){
    std::set<entityx::Entity>::iterator it = entities.begin();
    while(it != entities.end()){
      if((*it).valid()) entityManager->destroyEntity((*it));
      it++;
    }

    entities.clear();
  }
}
