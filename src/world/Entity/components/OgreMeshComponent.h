#ifndef __OGRE_MESH_COMPONENT_H__
#define __OGRE_MESH_COMPONENT_H__

#include "Ogre.h"
#include "../../Slots/SlotPosition.h"
#include "../EntityMeshManager.h"
#include "PositionComponent.h"
#include "entityx/entityx.h"

namespace AV{
struct OgreMeshComponent {
  OgreMeshComponent(Ogre::SceneNode *parentNode)
    : parentNode(parentNode) {}

  Ogre::SceneNode *parentNode;
  bool dirty = true;
};

struct OgreMeshComponentSystem : public entityx::System<OgreMeshComponent>, public entityx::Receiver<OgreMeshComponentSystem>{
  void update(entityx::EntityManager &es, entityx::EventManager &events, entityx::TimeDelta dt) override {
    es.each<OgreMeshComponent>([dt](entityx::Entity entity, OgreMeshComponent &comp) {
      if(comp.dirty){
        entityx::ComponentHandle<PositionComponent> position = entity.component<PositionComponent>();

        //comp.parentNode->setPosition(position.get()->position + Ogre::Vector3(position.get()->chunkX * CHUNK_SIZE_WORLD, 0, position.get()->chunkY * CHUNK_SIZE_WORLD));
        //comp.parentNode->setPosition(position.get()->position.pos.position + slotPositionToOgre())
        //comp.parentNode->setPosition(position.get()->pos.position);
        comp.parentNode->setPosition(position.get()->pos.toOgre());
        comp.dirty = false;
      }
    //   entityx::ComponentHandle<RigidBodyComponent> body = entity.component<RigidBodyComponent>();
    //   if(body){
    //     btTransform trans;
    //     body.get()->body->getMotionState()->getWorldTransform(trans);
    //
    //     comp.parentNode->setPosition(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ());
    //   }
    });
  };

  void configure(entityx::EventManager &event_manager) {
    event_manager.subscribe<entityx::EntityDestroyedEvent>(*this);
  }

  //void receive(entityx::Entity entity) {
  void receive(const entityx::EntityDestroyedEvent &event) {
    //std::cout << "Destroy event" << '\n';
    //EntityMeshManager::destroyOgreMesh(event.entity.parentNode);
    // entityx::ComponentHandle<OgreMeshComponent> meshComponent = event.entity.component<OgreMeshComponent>();
    // if(mesh){
    //   EntityMeshManager::destroyOgreMesh(mesh.get()->parentNode);
    // }

    //event.entity.parentNode->;
  }
};

// static void addHealthComponent(entityx::Entity e, Ogre::String meshName){
//   createMesh(meshName);
//   Ogre::Makemesh (meshName);
//   e.assign<OgreMeshComponent>(mesh);
// }
}

#endif
