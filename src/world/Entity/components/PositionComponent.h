#ifndef __POSITION_COMPONENT_H__
#define __POSITION_COMPONENT_H__

#include "../../Slots/SlotPosition.h"

namespace AV{
struct PositionComponent {
  PositionComponent(SlotPosition pos) : pos(pos) { }
  PositionComponent(int chunkX, int chunkY, Ogre::Vector3 position) : pos(SlotPosition(chunkX, chunkY, position)) { };

  SlotPosition pos;
};
}

#endif
