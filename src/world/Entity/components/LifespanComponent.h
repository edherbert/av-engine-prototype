#pragma once

struct LifespanComponent {
  LifespanComponent(int lifespan)
    : lifespan(lifespan) { }

  int lifespan;
};
