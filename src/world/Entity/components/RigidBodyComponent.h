#ifndef __RIGID_BODY_COMPONENT_H__
#define __RIGID_BODY_COMPONENT_H__

#include "btBulletDynamicsCommon.h"

#include "OgreMeshComponent.h"
#include "PositionComponent.h"
#include "../../Slots/SlotPosition.h"

namespace AV{
  struct RigidBodyComponent {
    RigidBodyComponent(btRigidBody *body)
      : body(body) {}

    btRigidBody *body;
  };

  struct RigidBodyComponentSystem : public entityx::System<RigidBodyComponent>{
    void update(entityx::EntityManager &es, entityx::EventManager &events,entityx::TimeDelta dt) override {
      es.each<RigidBodyComponent>([dt](entityx::Entity entity, RigidBodyComponent &comp) {
        btTransform trans;
        comp.body->getMotionState()->getWorldTransform(trans);

        entityx::ComponentHandle<PositionComponent> position = entity.component<PositionComponent>();
        if(position){
          //position.get()->pos = SlotPosition(trans.getOrigin());

        }


        // entityx::ComponentHandle<OgreMeshComponent> mesh = entity.component<OgreMeshComponent>();
        // if(mesh){
        //   mesh.get()->parentNode->setPosition(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ());
        //
        //   //Don't tell it it's dirty. It would be more efficient to move the mesh here as it leads to the values needing to converted less.
        //   //mesh.get()->dirty = true;
        // }
      });
    };
  };
}
#endif
