#pragma once

#include "../Damage/AfflictionData.h"

namespace AV{
  struct AfflictionComponent {
    AfflictionComponent(AfflictionData data, int lifeTime, int tickPeriod)
      : afflictionData(data),
        maxAfflictionTime(lifeTime),
        afflictionLifeTime(lifeTime),
        afflictionTickPeriod(tickPeriod),
        currentTickPeriod(0) { }

      AfflictionData afflictionData;

      const int maxAfflictionTime;
      int afflictionLifeTime;

      const int afflictionTickPeriod;
      int currentTickPeriod;
  };
}
