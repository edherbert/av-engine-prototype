#pragma once

namespace AV{
  enum CONTROLLER_TYPE : unsigned int{
    CONTROLLER_TYPE_ABSOLUTE,
    CONTROLLER_TYPE_PHYSICS,
    CONTROLLER_TYPE_PLAYER
  };

  struct MetaComponent {
    MetaComponent(){}

    CONTROLLER_TYPE controllerType = CONTROLLER_TYPE_ABSOLUTE;

  };
}
