#pragma once

#include "entityx/entityx.h"

#define NUM_ANIMS 4

namespace AV{
  struct AnimationComponent {
    AnimationComponent()
        {}
    ~AnimationComponent() { }

    // Ogre::SkeletonAnimation *currentFirst = 0;
    // Ogre::SkeletonAnimation *currentSecond = 0;
    // Ogre::SkeletonAnimation *fadingFirst = 0;
    // Ogre::SkeletonAnimation *fadingSecond = 0;

    Ogre::SkeletonAnimation* anims[NUM_ANIMS] = {0, 0, 0, 0};
    bool fadingIn[NUM_ANIMS] = {false, false, false, false};
    bool fadingOut[NUM_ANIMS] = {false, false, false, false};
  };

  struct AnimationComponentSystem : public entityx::System<AnimationComponent>, public entityx::Receiver<AnimationComponentSystem>{
    void update(entityx::EntityManager &es, entityx::EventManager &events, entityx::TimeDelta dt) override {
      es.each<AnimationComponent>([dt](entityx::Entity entity, AnimationComponent &comp) {

        for(int i = 0; i < NUM_ANIMS; i++){
          if(comp.anims[i] == 0) continue;

          if(comp.fadingIn[i]) {

            Ogre::Real newWeight = comp.anims[i]->mWeight + 0.01;
            comp.anims[i]->mWeight = Ogre::Math::Clamp<Ogre::Real>(newWeight, 0, 1);
            if(newWeight >= 1) comp.fadingIn[i] = false;
          }

          if(comp.fadingOut[i]) {
            Ogre::Real newWeight = comp.anims[i]->mWeight - 0.01;
            comp.anims[i]->mWeight = Ogre::Math::Clamp<Ogre::Real>(newWeight, 0, 1);
            if(newWeight <= 0){
              comp.fadingOut[i] = false;
              comp.fadingIn[i] = false;
              comp.anims[i]->setEnabled(false);
              comp.anims[i] = 0;
              continue;
            }
          }
          comp.anims[i]->addTime(0.01);

        }


        // comp.idleTop->addTime(0.01);
        // comp.idleBottom->addTime(0.01);
        // comp.fadingFirst->addTime(0.01);
        // comp.fadingSecond->addTime(0.01);

        // if(currentFirst.mWeight < 1) currentFirst.mWeight += 0.01;
        // if(currentSecond.mWeight < 1) currentSecond.mWeight += 0.01;
        //
        // if(fadingFirst.mWeight > 0) fadingFirst.mWeight -= 0.01;
        // else {
        //   fadingFirst->setEnabled(false);
        //   fadingFirst = 0;
        // }
        //
        // if(fadingSecond.mWeight > 0) fadingSecond.mWeight -= 0.01;
        // else {
        //   fadingSecond->setEnabled(false);
        //   fadingSecond = 0;
        // }

      });
    };
  };
}
