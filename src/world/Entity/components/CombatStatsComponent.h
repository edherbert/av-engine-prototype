#pragma once

struct CombatStats {
  CombatStats(int attack, int defence)
    : attack(attack), defence(defence) { }

  int attack = 1;
  int defence = 1;

};
