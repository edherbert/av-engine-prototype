#ifndef __HEALTH_COMPONENT_H__
#define __HEALTH_COMPONENT_H__

struct HealthComponent {
  HealthComponent(int health)
    : health(health) {}

  int health;
};

#endif
