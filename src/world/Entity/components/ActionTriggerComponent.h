#pragma once

#include "entityx/entityx.h"

#include "PositionComponent.h"
#include "../../../EventManager/Events/ActionEvent.h"

namespace AV{

struct ActionTriggerComponent {
  ActionTriggerComponent(btCollisionObject* collisionObject)
    : collisionObject(collisionObject)
      {}
  ~ActionTriggerComponent(){
    delete data2;
    delete data3;
    delete data4;
  }

  btCollisionObject* collisionObject;
  bool dirty = false;

  ActionData data;
  ActionData *data2 = 0;
  ActionData *data3 = 0;
  ActionData *data4 = 0;
};

struct ActionTriggerComponentSystem : public entityx::System<ActionTriggerComponent>, public entityx::Receiver<ActionTriggerComponentSystem>{
  void update(entityx::EntityManager &es, entityx::EventManager &events, entityx::TimeDelta dt) override {
    es.each<ActionTriggerComponent>([dt](entityx::Entity entity, ActionTriggerComponent &comp) {
      if(comp.dirty){
        entityx::ComponentHandle<PositionComponent> position = entity.component<PositionComponent>();

        //comp.parentNode->setPosition(position.get()->pos.toOgre());

        Ogre::Vector3 pos = position.get()->pos.toOgre();
        comp.collisionObject->getWorldTransform().setOrigin(btVector3(pos.x, pos.y, pos.z));
        comp.dirty = false;
      }
    //   entityx::ComponentHandle<RigidBodyComponent> body = entity.component<RigidBodyComponent>();
    //   if(body){
    //     btTransform trans;
    //     body.get()->body->getMotionState()->getWorldTransform(trans);
    //
    //     comp.parentNode->setPosition(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ());
    //   }
    });
  };
};
}
