#ifndef __PLAYER_DIALOG_COMPONENT_H__
#define __PLAYER_DIALOG_COMPONENT_H__

#include <iostream>
#include <string>

struct PlayerDialogComponent {
  PlayerDialogComponent(const char* dialogPath, int dialogBlock)
    : dialogPath(dialogPath),
      dialogBlock(dialogBlock) {}

  std::string dialogPath = "";
  int dialogBlock = 0;
};

#endif
