#pragma once

//#include "../../../util/BrainTree.h"

namespace BrainTree{
  class BehaviorTree;
}

namespace AV{
  struct AiComponent {
    AiComponent(BrainTree::BehaviorTree* tree) : tree(tree) { }
    ~AiComponent() { }

    BrainTree::BehaviorTree* tree;
  };
}
