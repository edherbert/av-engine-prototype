#pragma once

#include "BulletCollision/CollisionDispatch/btCollisionObject.h"

namespace AV{
  struct DamageTriggerComponent{
    DamageTriggerComponent(btCollisionObject *object) : collisionObject(object) { };

    btCollisionObject* collisionObject;
    bool dirty = true;
  };

  struct DamageTriggerComponentSystem : public entityx::System<DamageTriggerComponent>, public entityx::Receiver<DamageTriggerComponentSystem>{
    void update(entityx::EntityManager &es, entityx::EventManager &events, entityx::TimeDelta dt) override {
      es.each<DamageTriggerComponent>([dt](entityx::Entity entity, DamageTriggerComponent &comp) {
        if(comp.dirty){
          entityx::ComponentHandle<PositionComponent> position = entity.component<PositionComponent>();

          Ogre::Vector3 pos = position.get()->pos.toOgre();
          comp.collisionObject->getWorldTransform().setOrigin(btVector3(pos.x, pos.y, pos.z));
          comp.dirty = false;
        }

      });
    };
  };
};
