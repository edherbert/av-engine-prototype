#ifndef __MOVEMENT_COMPONENT_H__
#define __MOVEMENT_COMPONENT_H__

#include "OgreVector3.h"

struct MovementComponent {
  MovementComponent(float speed, Ogre::Vector3 direction)
    : speed(speed), direction(direction) {}

  float speed;
  Ogre::Vector3 direction;
};

#endif
