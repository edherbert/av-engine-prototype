#pragma once

class btRigidBody;
//class btVector3;
class btCollisionShape;

#include "LinearMath/btVector3.h"

namespace AV{
  enum ENTITY_BODY_SHAPE_TYPE{
    ENTITY_BODY_CUBE,
    ENTITY_BODY_SPHERE,
    ENTITY_BODY_CAPSULE,
  };

  class EntityBodyManager{
  public:
    EntityBodyManager();
    ~EntityBodyManager();

    static btRigidBody* createRigidBody(ENTITY_BODY_SHAPE_TYPE type,
     btVector3 position = btVector3(0, 0, 0),
     btVector3 scale = btVector3(0, 0, 0),
     btScalar mass = 1);

    static btRigidBody* createCapsuleBody(btVector3 position, btScalar radius, btScalar height);

    static btRigidBody* createCubeBody(const btVector3 &position, const btVector3 &scale);
    //static btRigidBody* createSphereBody(const btVector3 &position, const btScalar &radius);

  private:
    static btCollisionShape* _createShape(ENTITY_BODY_SHAPE_TYPE type, btVector3 scale);
  };
}
