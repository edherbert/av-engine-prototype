#include "EntityBodyManager.h"

#include "../Physics/BulletManager.h"
#include "btBulletDynamicsCommon.h"

#include <iostream>

namespace AV{
  EntityBodyManager::EntityBodyManager(){

  }

  EntityBodyManager::~EntityBodyManager(){

  }

  btCollisionShape* EntityBodyManager::_createShape(ENTITY_BODY_SHAPE_TYPE type, btVector3 scale){
    std::cout << "scale " << scale.y() << '\n';
    btCollisionShape *shape = 0;
    switch(type){
      case ENTITY_BODY_CUBE:
        shape = new btBoxShape(scale);
        break;
      case ENTITY_BODY_SPHERE:
        shape = new btSphereShape(scale.x());
        break;
      case ENTITY_BODY_CAPSULE:
        shape = new btCapsuleShape(scale.x(), scale.y());
    }

    return shape;
  }

  btRigidBody* EntityBodyManager::createCapsuleBody(btVector3 position, btScalar radius, btScalar height){
    btCollisionShape *fallShape = _createShape(ENTITY_BODY_CAPSULE, btVector3(radius, height, 0));
    btDefaultMotionState *fallMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 50, 0)));
    btScalar mass = 1;
    btVector3 fallInertia(0, 0, 0);
    fallShape->calculateLocalInertia(mass, fallInertia);

    btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);
    fallRigidBodyCI.m_friction = 0.5;
    fallRigidBodyCI.m_rollingFriction = 0.1;
    btRigidBody* playerRigidBody = new btRigidBody(fallRigidBodyCI);

    playerRigidBody->setAngularFactor(btVector3(0, 1, 0));
    BulletManager::world->addRigidBody(playerRigidBody);

    return playerRigidBody;
  }

  btRigidBody* EntityBodyManager::createRigidBody(ENTITY_BODY_SHAPE_TYPE type, btVector3 position, btVector3 scale, btScalar mass){
    btVector3 fallInertia(0, 0, 0);
    //btCollisionShape *fallShape = new btBoxShape(scale);
    btCollisionShape *fallShape = _createShape(type, scale);
    fallShape->calculateLocalInertia(mass, fallInertia);

    btDefaultMotionState *fallMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 50, 0)));

    btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);
    fallRigidBodyCI.m_friction = 1;

    btRigidBody *fallRigidBody = new btRigidBody(fallRigidBodyCI);
    fallRigidBody->getWorldTransform().setOrigin(position);
    BulletManager::world->addRigidBody(fallRigidBody);

    return fallRigidBody;
  }

  btRigidBody* EntityBodyManager::createCubeBody(const btVector3 &position, const btVector3 &scale){
    btScalar mass = 1;
    btVector3 fallInertia(0, 0, 0);
    //btCollisionShape *fallShape = new btBoxShape(scale);
    btCollisionShape *fallShape = _createShape(ENTITY_BODY_CUBE, scale);
    fallShape->calculateLocalInertia(mass, fallInertia);

    btDefaultMotionState *fallMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 50, 0)));

    btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);
    fallRigidBodyCI.m_friction = 1;

    btRigidBody *fallRigidBody = new btRigidBody(fallRigidBodyCI);
    fallRigidBody->getWorldTransform().setOrigin(position);
    BulletManager::world->addRigidBody(fallRigidBody);

    return fallRigidBody;
  }

  // static btRigidBody* EntityBodyManager::createSphereBody(const btVector3 &position, const btScalar &radius){
  //
  // }
}
