#include "OgreMap.h"

#include "Window.h"
#include "world/World.h"

#include <iostream>

namespace AV{
  OgreMap::OgreMap(Ogre::SceneManager *sceneManager, Window *window, Ogre::Camera *camera, AV::World *world)
    : sceneManager(sceneManager),
      window(window),
      camera(camera),
      world(world){

    /*Ogre::SceneNode *node = sceneManager->getRootSceneNode()->createChildSceneNode(Ogre::SCENE_DYNAMIC);
    Ogre::Item *item = sceneManager->createItem("ogrehead2.mesh", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
    node->attachObject((Ogre::MovableObject*)item);*/

    //Terrain *terrain = new Terrain(sceneManager, 100, 100);
    camera->setPosition(0, 30, -30);
    //camera->setPosition(0, 50, 0);
    camera->lookAt(0, 0, 0);

    camera->move(Ogre::Vector3(0, 60, 0));
  }

  OgreMap::~OgreMap(){

  }

  void OgreMap::update(){
    /*if(window->getKey(KEY_W))camera->move(camera->getDirection() * cameraSpeed);
    if(window->getKey(KEY_S))camera->move(camera->getDirection() * -cameraSpeed);
    if(window->getKey(KEY_A))camera->move(camera->getRight() * -cameraSpeed);
    if(window->getKey(KEY_D))camera->move(camera->getRight() * cameraSpeed);*/

    if(window->getKey(KEY_W))camera->move(Ogre::Vector3(0, 0, 1) * cameraSpeed);
    if(window->getKey(KEY_S))camera->move(Ogre::Vector3(0, 0, -1) * cameraSpeed);
    if(window->getKey(KEY_A))camera->move(Ogre::Vector3(1, 0, 0) * cameraSpeed);
    if(window->getKey(KEY_D))camera->move(Ogre::Vector3(-1, 0, 0) * cameraSpeed * cameraSpeed);

    /*if(window->getKey(KEY_W) || window->getKey(KEY_A) || window->getKey(KEY_S) || window->getKey(KEY_D) ){
      //world->setCurrentPositionWorld(camera->getPosition());
      world->setLoadLocation(world->convertToChunkSpace(camera->getPosition()));
      //world->convertToChunkSpace(camera->getPosition());
    }*/
    //std::cout << camera->getPosition() << '\n';

    if(window->getKey(KEY_F)) window->setMouseGrab(true);
    if(window->getKey(KEY_G)) window->setMouseGrab(false);

    //pointCamera(window->xOffset, window->yOffset);
  }

  void OgreMap::pointCamera(int xOffset, int yOffset){
    float sense = 0.05;
    float xCamera = xOffset;
    float yCamera = yOffset;
    //xOffset *= sense;
    //yOffset *= sense;
    xCamera *= sense;
    yCamera *= sense;

    yaw += xCamera;
    pitch += yCamera;
    if(pitch > 89.0f) pitch = 89.0f;
    if(pitch < -89.0f) pitch = -89.0f;

    Ogre::Vector3 front;
    front.x = cos(radians(yaw)) * cos(radians(pitch));
    front.y = sin(radians(pitch));
    front.z = sin(radians(yaw)) * cos(radians(pitch));

    //Normalise the camera front
    float length = sqrt((front.x * front.x) + (front.y * front.y) + (front.z * front.z));
    Ogre::Vector3 cameraFront = Ogre::Vector3(front.x / length, front.y / length, front.z / length);

    camera->setDirection(cameraFront);

    window->xOffset = 0;
    window->yOffset = 0;
  }

  float OgreMap::radians(float value){
    return value * (M_PI / 180);
  }
}
