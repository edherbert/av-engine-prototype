#include "ActionManager.h"

#include "../EventManager/EventManager.h"
#include "../EventManager/Events/ActionEvent.h"
#include "../EventManager/Events/InputEvent.h"
#include "../EventManager/Events/DialogEvent.h"

#include "../scripting/ScriptManager.h"

#include <iostream>

#include <OgreOverlayManager.h>
#include "OgreOverlay.h"
#include "OgreOverlayContainer.h"
#include "OgreTextAreaOverlayElement.h"
#include "OgreOverlaySystem.h"
#include "OgrePanelOverlayElement.h"
#include "OgreFontManager.h"

namespace AV{
  ActionManager::ActionManager(){
    EventManager::subscribeToEvent(this, EVENT_TYPE_ACTION);
    EventManager::subscribeToEvent(this, EVENT_TYPE_INPUT);

    _setup();
  }

  ActionManager::~ActionManager(){

  }

  void ActionManager::_setup(){
    Ogre::v1::OverlayManager &overlayManager = Ogre::v1::OverlayManager::getSingleton();

    overlay = overlayManager.create( "ActionOverlay" );

    Ogre::v1::OverlayContainer *panel = static_cast<Ogre::v1::OverlayContainer*>(overlayManager.createOverlayElement("Panel", "ActionPanel"));

    //panel->setPosition(0.5, 0.5);

    Ogre::FontManager::getSingleton().getByName("DefaultFont")->load();

    dialogText = static_cast<Ogre::v1::TextAreaOverlayElement*>(overlayManager.createOverlayElement( "TextArea", "ActionText" ) );
    dialogText->setFontName( "DefaultFont" );
    dialogText->setCharHeight( 0.0225f );
    // dialogText->setTop(0.85);

    //dialogText->setAlignment(Ogre::v1::TextAreaOverlayElement::Right);
    //dialogText->setHorizontalAlignment(Ogre::v1::GHA_RIGHT);
    // dialogText->setVerticalAlignment(Ogre::v1::GVA_TOP);

    dialogText->setCaption("");

    panel->setColour(Ogre::ColourValue(1, 1, 1, 1));

    panel->addChild( dialogText );
    //overlay->setTransparent(false);
    overlay->add2D( panel );
    //overlay->add2D( e );
    //overlay->show();

    //dialogText->setFontName("Hello");

    overlay->show();

    dialogText->setAlignment(Ogre::v1::TextAreaOverlayElement::Right);
    dialogText->setHorizontalAlignment(Ogre::v1::GHA_RIGHT);

    dialogText->setTop((1 - dialogText->getCharHeight() * 2) - 0.01);
  }

  void ActionManager::notifyActionEvent(ActionEvent &e){
    if(e.eventType == ACTION_EVENT_TYPE_ENTER){
      getTargetList(e.data.inputId).push_back(e.data);
    }else if(e.eventType == ACTION_EVENT_TYPE_LEAVE){
      removeActionData(e.data);
    }

    //update the gui.
    updateText();
  }

  void ActionManager::notifyInputEvent(InputEvent &e){
    if(!acceptInput) return;
    if(!(e.keyValue == I_DECLINE || e.keyValue == I_ACCEPT)) return;
    if(e.inputType == KEY_DOWN){
      // InputType i
      // if(e.keyValue == I_ACCEPT){
      //   ActionData* data = getCurrentData(I_ACCEPT);
      //   if(data){
      //     performAction(data);
      //   }
      // }else if(e.keyValue == I_DECLINE){
      //
      // }
      ActionData* data = getCurrentData(e.keyValue);
      if(data){
        performAction(data);
      }
    }
  }

  void ActionManager::performAction(ActionData *d){
    std::cout << d->actionDescription << '\n';


    if(d->actionType == ACTION_TRIGGER_TYPE_SCRIPT){
      ScriptManager::runScript(d->resourceName.c_str());
    }else if(d->actionType == ACTION_TRIGGER_TYPE_DIALOG){
      DialogEvent s;
      s.dialogType = DIALOG_END;
      EventManager::transmitDialogEvent(s);

      DialogEvent e;
      e.dialogType = DIALOG_START;
      e.dialogPath = d->resourceName;

      e.startingBlock = 0;
      EventManager::transmitDialogEvent(e);
    }
  }

  void ActionManager::removeActionData(ActionData d){
    std::vector<ActionData>& list = getTargetList(d.inputId);
    for(int i = 0; i < list.size(); i++){
      if(list[i].actionDescription == d.actionDescription && list[i].inputId == d.inputId && list[i].resourceName == d.resourceName){
        list.erase(list.begin() + i);
        break;
      }
    }
  }

  ActionData* ActionManager::getCurrentData(INPUT_TYPE type){
    std::vector<ActionData>& list = getTargetList(type);

    if(list.size() > 0){
      return &(list[0]);
    }else{
      return 0;
    }
  }

  std::vector<ActionData>& ActionManager::getTargetList(INPUT_TYPE type){
    if(type == I_ACCEPT){
      return acceptButtonList;
    }else if(type == I_DECLINE){
      return declineButtonList;
    }
  }

  void ActionManager::updateText(){
    Ogre::String finalString = "";

    if(acceptButtonList.size() > 0){
      finalString += Input::getInputString(I_ACCEPT) + " - ";
      finalString += acceptButtonList[0].actionDescription;
    }
    finalString += "\n";

    if(declineButtonList.size() > 0){
      finalString += Input::getInputString(I_DECLINE) + " - ";
      finalString += declineButtonList[0].actionDescription;
    }
    finalString += "\n";

    dialogText->setCaption(finalString);
  }
}
