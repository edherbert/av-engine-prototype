#include "TerminalGui.h"

#include <iostream>

#include "../scripting/TerminalManager.h"

#include <OgreOverlayManager.h>
#include "OgreOverlay.h"
#include "OgreOverlayContainer.h"
#include "OgreTextAreaOverlayElement.h"
#include "OgreOverlaySystem.h"
#include "OgrePanelOverlayElement.h"
#include "OgreFontManager.h"

namespace AV{
  TerminalGui::TerminalGui(){
    _setup();
  }

  TerminalGui::~TerminalGui(){

  }

  void TerminalGui::setVisible(bool visible){
    this->visible = visible;
    if(visible) overlay->show();
    else overlay->hide();
  }

  void TerminalGui::_setup(){
    Ogre::v1::OverlayManager &overlayManager = Ogre::v1::OverlayManager::getSingleton();

    overlay = overlayManager.create( "TerminalOverlay" );

    Ogre::v1::OverlayContainer *panel = static_cast<Ogre::v1::OverlayContainer*>(overlayManager.createOverlayElement("Panel", "TerminalPanel"));

    Ogre::FontManager::getSingleton().getByName("DefaultFont")->load();

    dialogText = static_cast<Ogre::v1::TextAreaOverlayElement*>(overlayManager.createOverlayElement( "TextArea", "TerminalText" ) );
    dialogText->setFontName( "DefaultFont" );
    dialogText->setCharHeight( 0.0225f );

    dialogCommand = static_cast<Ogre::v1::TextAreaOverlayElement*>(overlayManager.createOverlayElement( "TextArea", "CommandText" ) );
    dialogCommand->setFontName( "DefaultFont" );
    dialogCommand->setCharHeight( 0.0225f );

    dialogText->setCaption("");
    dialogCommand->setCaption("*");

    // textAlignment = Ogre::v1::TextAreaOverlayElement::Right;
    // posAlignment = Ogre::v1::GHA_RIGHT;
    positionTerminal();

    //dialogText->setAlignment(textAlignment);
    //dialogText->setHorizontalAlignment(posAlignment);

    panel->setColour(Ogre::ColourValue(1, 1, 1, 1));

    panel->addChild(dialogCommand);
    panel->addChild( dialogText );
    //overlay->setTransparent(false);
    overlay->add2D( panel );
    //overlay->add2D( e );
    //overlay->show();
  }

  void TerminalGui::clearTerminal(){
    std::cout << "Clearning Terminal" << '\n';
    terminalString = "";
    terminalHeight = 0;
    dialogText->setCaption(terminalString);
  }

  void TerminalGui::addStringToTerminal(const char* text){
    int boundary = 50;
    size_t size = strlen(text);
    int ammount = (size / boundary) + 1;
    terminalHeight += ammount;
    Ogre::String s = Ogre::String(text);

    for(int i = 0; i < ammount; i++){
      terminalString += s.substr(i * boundary, boundary) + '\n';
    }

    //terminalString += Ogre::String(text);
    dialogText->setCaption(terminalString);

    positionTerminal();
  }

  void TerminalGui::commitCommand(){
    if(!visible) return;
    commandText.pop_back();
    addStringToTerminal(("$ " + commandText).c_str());

    if(terminalManager) terminalManager->runCommand(commandText.c_str());

    commandText = "*";
    dialogCommand->setCaption(commandText);
  }

  void TerminalGui::backspaceCommand(){
    if(commandText != ""){
      //Remove the marker and the text at the end, and re-add the marker.
      commandText.pop_back();
      if(commandText != "") commandText.pop_back();
      commandText += Ogre::String("*");
    }
    dialogCommand->setCaption(commandText);
  }

  void TerminalGui::addTextCommand(const char *text){
    if(commandText != "") commandText.pop_back();
    commandText += Ogre::String(text);
    commandText += Ogre::String("*");
    dialogCommand->setCaption(commandText);
  }

  void TerminalGui::positionTerminal(){
    dialogCommand->setTop(1 - dialogText->getCharHeight());
    dialogText->setTop((1 - dialogText->getCharHeight() * terminalHeight) - dialogText->getCharHeight() - 0.01);
  }

  void TerminalGui::setupTerminalManager(TerminalManager *terminalManager){
    this->terminalManager = terminalManager;
    TerminalManager::terminalGui = this;
  }
}
