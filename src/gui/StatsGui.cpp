#include "StatsGui.h"

#include <iostream>

#include <OgreOverlayManager.h>
#include "OgreOverlay.h"
#include "OgreOverlayContainer.h"
#include "OgreTextAreaOverlayElement.h"
#include "OgreOverlaySystem.h"
#include "OgrePanelOverlayElement.h"
#include "OgreFontManager.h"

#include "../EventManager/EventManager.h"
#include "../EventManager/Events/StatsEvent.h"
#include "OgreStringConverter.h"

#include "../world/Slots/SlotPosition.h"
#include "../world/Slots/SlotManager.h"


namespace AV{
  StatsGui::StatsGui(){
    _setup();

    EventManager::subscribeToEvent(this, EVENT_TYPE_STATS);
  }

  StatsGui::~StatsGui(){

  }

  void StatsGui::_setup(){
    Ogre::v1::OverlayManager &overlayManager = Ogre::v1::OverlayManager::getSingleton();

    overlay = overlayManager.create( "StatsOverlay" );

    Ogre::v1::OverlayContainer *panel = static_cast<Ogre::v1::OverlayContainer*>(overlayManager.createOverlayElement("Panel", "StatsPanel"));

    //panel->setPosition(0.5, 0.5);

    Ogre::FontManager::getSingleton().getByName("DefaultFont")->load();

    dialogText = static_cast<Ogre::v1::TextAreaOverlayElement*>(overlayManager.createOverlayElement( "TextArea", "StatsText" ) );
    dialogText->setFontName( "DefaultFont" );
    dialogText->setCharHeight( 0.0225f );
    // dialogText->setTop(0.85);

    setLeftAlignment(true);
    //dialogText->setAlignment(Ogre::v1::TextAreaOverlayElement::Right);
    //dialogText->setHorizontalAlignment(Ogre::v1::GHA_RIGHT);
    // dialogText->setVerticalAlignment(Ogre::v1::GVA_TOP);

    dialogText->setCaption("");

    panel->setColour(Ogre::ColourValue(1, 1, 1, 1));

    panel->addChild( dialogText );
    //overlay->setTransparent(false);
    overlay->add2D( panel );
    //overlay->add2D( e );
    //overlay->show();

    SlotPosition origin = {0, 0, Ogre::Vector3(0, 0, 0)};
    Ogre::Vector3 player = Ogre::Vector3::ZERO;
    updateOrigin(origin);
    updatePlayerSlot(origin);
    updatePlayerOgre(player);
    //updateLoadedChunks((void*)0);
    setText();
  }

  void StatsGui::setText(){
    Ogre::String done = "";

    done += originText + "\n";
    done += playerSlotText + "\n";
    done += playerOgreText + "\n";
    done += loadedChunksText + "\n";
    done += mapChunksText;
    dialogText->setCaption(done);
  }

  void StatsGui::setVisible(bool visible){
    if(visible) overlay->show();
    else overlay->hide();
  }

  void StatsGui::setLeftAlignment(bool left){
    auto textAlignment = Ogre::v1::TextAreaOverlayElement::Left;
    auto posAlignment = Ogre::v1::GHA_LEFT;
    if(!left){
      textAlignment = Ogre::v1::TextAreaOverlayElement::Right;
      posAlignment = Ogre::v1::GHA_RIGHT;
    }

    dialogText->setAlignment(textAlignment);
    dialogText->setHorizontalAlignment(posAlignment);
  }

  void StatsGui::notifyStatsEvent(StatsEvent &e){
    if(e.statsType == STATS_ORIGIN_POS){
      updateOrigin(e.slotValue);
    }
    if(e.statsType == STATS_SLOT_PLAYER_POS){
      updatePlayerSlot(e.slotValue);
    }
    if(e.statsType == STATS_OGRE_PLAYER_POS){
      updatePlayerOgre(e.position);
    }
    if(e.statsType == STATS_LOADED_CHUNKS){
      updateChunks(e.chunkVector, "Loaded Chunks", loadedChunksText);
    }
    if(e.statsType == STATS_MAP_CHUNKS){
      updateChunks(e.chunkVector, "Map Chunks", mapChunksText);
    }
    setText();
  }

  void StatsGui::updateOrigin(SlotPosition &origin){
    originText = "Origin: X:" + Ogre::StringConverter::toString(origin.chunkX) + " Y:" + Ogre::StringConverter::toString(origin.chunkY)
      + " position:(" + Ogre::StringConverter::toString(origin.position) + ")";
  }

  void StatsGui::updatePlayerSlot(SlotPosition &origin){
    playerSlotText = "Player Slot Position: X:" + Ogre::StringConverter::toString(origin.chunkX) + " Y:" + Ogre::StringConverter::toString(origin.chunkY)
      + " position:(" + Ogre::StringConverter::toString(origin.position) + ")";
  }

  void StatsGui::updatePlayerOgre(Ogre::Vector3 &playerPos){
    playerOgreText = "Player Ogre position:(" + Ogre::StringConverter::toString(playerPos) + ")";
  }

  void StatsGui::updateChunks(std::vector<ChunkData> *chunkVector, Ogre::String listName, Ogre::String &targetString){
    if(chunkVector){
      int listSize = chunkVector->size();
      targetString = listName + ". Size: " + Ogre::StringConverter::toString(listSize);
      if(listSize > 0) targetString += "\n";

      for(int i = 0; i < listSize; i++){
        targetString += "X: " + Ogre::StringConverter::toString((*chunkVector)[i].chunkX) +
         " Y: " + Ogre::StringConverter::toString((*chunkVector)[i].chunkY) +
         " name: " + (*chunkVector)[i].mapName + "\n";
      }
    }
  }
}
