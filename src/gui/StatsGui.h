#ifndef __STATS_GUI_H__
#define __STATS_GUI_H__

#include "OgreString.h"
#include "../EventManager/EventReceiver.h"
#include <vector>

//#include "../world/Slots/SlotPosition.h"

namespace Ogre
{
    namespace v1
    {
        class Overlay;
        class TextAreaOverlayElement;
    }
}

namespace AV{
  class StatsEvent;
  class SlotPosition;
  struct ChunkData;

  class StatsGui : public EventReceiver{
  public:
    StatsGui();
    ~StatsGui();

    void setVisible(bool visible);
    void setLeftAlignment(bool left);
    void setText();

    void notifyStatsEvent(StatsEvent &e);

  private:
    Ogre::v1::Overlay *overlay;
    Ogre::v1::TextAreaOverlayElement *dialogText;

    Ogre::String originText = "";
    Ogre::String playerSlotText = "";
    Ogre::String playerOgreText = "";
    Ogre::String loadedChunksText = "";
    Ogre::String mapChunksText = "";

    void updateOrigin(SlotPosition &origin);
    void updatePlayerSlot(SlotPosition &origin);
    void updatePlayerOgre(Ogre::Vector3 &playerPos);
    void updateChunks(std::vector<ChunkData> *chunkVector, Ogre::String listName, Ogre::String &targetString);

    void _setup();
  };
}

#endif
