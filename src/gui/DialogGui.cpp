#include "DialogGui.h"

#include <OgreOverlayManager.h>
#include "OgreOverlay.h"
#include "OgreOverlayContainer.h"
#include "OgreTextAreaOverlayElement.h"
#include "OgreOverlaySystem.h"
#include "OgrePanelOverlayElement.h"
#include "OgreFontManager.h"

namespace AV{
  DialogGui::DialogGui(){
    _setupDialog();
  }

  DialogGui::~DialogGui(){

  }

  void DialogGui::_setupDialog(){
    Ogre::v1::OverlayManager &overlayManager = Ogre::v1::OverlayManager::getSingleton();

    overlay = overlayManager.create( "DialogOverlay" );

    Ogre::v1::OverlayContainer *panel = static_cast<Ogre::v1::OverlayContainer*>(overlayManager.createOverlayElement("Panel", "DialogPanel"));

    //panel->setPosition(0.5, 0.5);

    Ogre::FontManager::getSingleton().getByName("DefaultFont")->load();

    dialogText = static_cast<Ogre::v1::TextAreaOverlayElement*>(overlayManager.createOverlayElement( "TextArea", "DialogText" ) );
    dialogText->setFontName( "DefaultFont" );
    dialogText->setCharHeight( 0.0325f );
    dialogText->setTop(0.85);

    dialogText->setAlignment(Ogre::v1::TextAreaOverlayElement::Center);
    dialogText->setHorizontalAlignment(Ogre::v1::GHA_CENTER);
    dialogText->setVerticalAlignment(Ogre::v1::GVA_TOP);

    dialogText->setCaption("");

    panel->setColour(Ogre::ColourValue(1, 1, 1, 1));

    panel->addChild( dialogText );
    //overlay->setTransparent(false);
    overlay->add2D( panel );
    //overlay->add2D( e );
    //overlay->show();

  }

  void DialogGui::displayDialog(Ogre::String text){
    dialogText->setCaption(text);
    overlay->show();
  }

  void DialogGui::endDialog(){
    dialogText->setCaption("");
    overlay->hide();
  }
}
