#ifndef __DIALOG_GUI_H__
#define __DIALOG_GUI_H__

#include "OgreString.h"

namespace Ogre
{
    namespace v1
    {
        class Overlay;
        class TextAreaOverlayElement;
    }
}

namespace AV{
  class DialogGui{
  public:
    DialogGui();
    ~DialogGui();

    void displayDialog(Ogre::String text);
    void endDialog();

  private:
    void _setupDialog();

    Ogre::v1::Overlay *overlay;
    Ogre::v1::TextAreaOverlayElement *dialogText;

  };
}

#endif
