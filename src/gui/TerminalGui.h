#pragma once

#include "OgreString.h"

namespace Ogre
{
    namespace v1
    {
        class Overlay;
        class TextAreaOverlayElement;
    }
}
namespace AV{
  class TerminalManager;
  class TerminalGui{
  public:
    TerminalGui();
    ~TerminalGui();

    void setVisible(bool visible);
    void positionTerminal();
    void addStringToTerminal(const char* text);
    void addTextCommand(const char *text);

    void clearTerminal();

    void backspaceCommand();
    void commitCommand();

    void setupTerminalManager(TerminalManager *terminalManager);

  private:
    bool visible = false;

    Ogre::v1::Overlay *overlay;
    Ogre::v1::TextAreaOverlayElement *dialogText;
    Ogre::v1::TextAreaOverlayElement *dialogCommand;

    Ogre::String terminalString = "";
    unsigned int terminalHeight = 0;

    Ogre::String commandText = "";

    TerminalManager *terminalManager = 0;

    void _setup();
  };
}
