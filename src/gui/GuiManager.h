#ifndef __GUI_MANAGER_H__
#define __GUI_MANAGER_H__

#include "../EventManager/EventReceiver.h"

namespace Ogre{
  class SceneManager;

  namespace v1
  {
      class OverlaySystem;
  }
}

namespace AV{
  class DialogGui;
  class StatsGui;
  class TerminalGui;
  class TerminalManager;
  class ActionManager;

  class GuiManager : public EventReceiver{
  public:
    GuiManager(Ogre::SceneManager *sceneManager, Ogre::v1::OverlaySystem *overlaySystem);
    ~GuiManager();

    void setupTerminalManager(TerminalManager *terminalManager);

  private:
    void notifyInputEvent(InputEvent &e);
    void notifyDialogEvent(DialogEvent &e);
    void notifyTypedEvent(TypedEvent &e);

    void _setupOverlay(Ogre::SceneManager *sceneManager);

    bool terminalShown = false;

    DialogGui *dialogGui;
    StatsGui *statsGui;
    TerminalGui *terminalGui;
    ActionManager *actionManager;
    Ogre::v1::OverlaySystem *overlaySystem;
  };
}

#endif
