#pragma once

#include "../EventManager/EventReceiver.h"
#include "../EventManager/Events/ActionEvent.h"

#include <vector>

namespace Ogre
{
    namespace v1
    {
        class Overlay;
        class TextAreaOverlayElement;
    }
}

namespace AV{
  class ActionManager : public EventReceiver{
    public:
       ActionManager();
       ~ActionManager();

       void notifyActionEvent(ActionEvent &e);
       void notifyInputEvent(InputEvent &e);

       bool acceptInput = true;

     private:
       std::vector<ActionData> acceptButtonList;
       std::vector<ActionData> declineButtonList;

       Ogre::v1::Overlay *overlay;
       Ogre::v1::TextAreaOverlayElement *dialogText;

       void performAction(ActionData *d);

       void updateText();

       std::vector<ActionData>& getTargetList(INPUT_TYPE type);
       ActionData* getCurrentData(INPUT_TYPE type);
       void _setup();

       void removeActionData(ActionData d);
  };
}
