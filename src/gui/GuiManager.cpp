#include "GuiManager.h"

#include <iostream>

#include "OgreSceneManager.h"
#include "../EventManager/EventManager.h"
#include "../EventManager/Events/DialogEvent.h"
#include "../EventManager/Events/InputEvent.h"
#include "../EventManager/Events/TypedEvent.h"

#include "OgreOverlayManager.h"
#include "OgreOverlaySystem.h"
#include "DialogGui.h"
#include "StatsGui.h"
#include "TerminalGui.h"
#include "ActionManager.h"

namespace AV{
  GuiManager::GuiManager(Ogre::SceneManager *sceneManager, Ogre::v1::OverlaySystem *overlaySystem)
    : overlaySystem(overlaySystem){

    EventManager::subscribeToEvent(this, EVENT_TYPE_INPUT);
    EventManager::subscribeToEvent(this, EVENT_TYPE_DIALOG);
    EventManager::subscribeToEvent(this, EVENT_TYPE_TYPED);

    _setupOverlay(sceneManager);

    dialogGui = new DialogGui();
    statsGui = new StatsGui();
    terminalGui = new TerminalGui();
    actionManager = new ActionManager();

    statsGui->setVisible(true);
  }

  GuiManager::~GuiManager(){
    delete dialogGui;
  }

  void GuiManager::notifyInputEvent(InputEvent &e){
    if(e.inputType == KEY_DOWN){
      if(e.keyValue == I_ACCEPT){
        DialogEvent d;
        d.dialogType = DIALOG_INCREMENT;

        EventManager::transmitDialogEvent(d);
      }
      if(e.keyValue == I_DEBUG_TERMINAL){
        TypedEvent event;

        if(!terminalShown){
          //Show terminal
          terminalGui->setVisible(true);
          statsGui->setLeftAlignment(false);

          event.type = TYPED_EVENT_START;
        }else{
          //Hide terminal
          terminalGui->setVisible(false);
          statsGui->setLeftAlignment(true);

          event.type = TYPED_EVENT_END;
        }
        EventManager::transmitTypedEvent(event);

        terminalShown = !terminalShown;
      }
    }
  }

  void GuiManager::notifyTypedEvent(TypedEvent &e){
    if(e.type == TYPED_EVENT_CHAR){
      terminalGui->addTextCommand(e.text);
    }
    if(e.type == TYPED_EVENT_COMMAND){
      if(e.command == TYPED_EVENT_BACKSPACE) terminalGui->backspaceCommand();
      if(e.command == TYPED_EVENT_ENTER) terminalGui->commitCommand();
    }
  }

  void GuiManager::notifyDialogEvent(DialogEvent &e){
    if(e.dialogType == DIALOG_TEXT){
      dialogGui->displayDialog(e.dialogText);
    }
    else if(e.dialogType == DIALOG_START){
      //EventManager::unsubscribeToEvent(actionManager, EVENT_TYPE_INPUT);
      actionManager->acceptInput = false;
    }
    else if(e.dialogType == DIALOG_END){
      actionManager->acceptInput = true;
      dialogGui->endDialog();

      //EventManager::subscribeToEvent(actionManager, EVENT_TYPE_INPUT);
    }
  }

  void GuiManager::setupTerminalManager(TerminalManager *terminalManager){
    terminalGui->setupTerminalManager(terminalManager);
  }

  void GuiManager::_setupOverlay(Ogre::SceneManager *sceneManager){
    //Ogre::v1::OverlaySystem *mOverlaySystem = OGRE_NEW Ogre::v1::OverlaySystem();

    Ogre::v1::OverlayManager &overlayManager = Ogre::v1::OverlayManager::getSingleton();
    sceneManager->addRenderQueueListener(overlaySystem);
    sceneManager->getRenderQueue()->setSortRenderQueue(Ogre::v1::OverlayManager::getSingleton().mDefaultRenderQueueId, Ogre::RenderQueue::StableSort);
  }
}
