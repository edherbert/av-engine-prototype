#ifndef __INPUT_H__
#define __INPUT_H__

#include <string>

namespace AV{
  #define INPUT_COUNT 14
  enum INPUT_TYPE{
    I_NULL,
    I_UP,
    I_DOWN,
    I_LEFT,
    I_RIGHT,

    I_AIM_UP,
    I_AIM_DOWN,
    I_AIM_LEFT,
    I_AIM_RIGHT,

    I_JUMP,
    I_ACCEPT,
    I_DECLINE,

    I_DEBUG_TERMINAL,
    I_DEBUG_PLAYER_CONTROLLER
  };

  class Input{
  public:
    Input();
    ~Input();

    static void setInput(INPUT_TYPE type, bool value);
    static bool getInput(INPUT_TYPE type);

    static void setInputStrings(std::string (&is)[INPUT_COUNT]);
    static const std::string& getInputString(INPUT_TYPE i);

  private:
    //bool I_UP, I_DOWN, I_LEFT, I_RIGHT;
    //bool I_ACCEPT, I_DECLINE;

    //The size of the enum.
    static bool activeInputs[INPUT_COUNT];
    static std::string inputStrings[INPUT_COUNT];
  };
}

#endif
