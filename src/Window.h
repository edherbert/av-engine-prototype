#ifndef __WINDOW_H__
#define __WINDOW_H__

//#include <Ogre.h>
#include <OgreRenderWindow.h>
#include <SDL.h>

#include "EventManager/EventReceiver.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE && OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
#include <AppKit/NSWindow.h>
#endif

#define KEY_W SDL_SCANCODE_W
#define KEY_A SDL_SCANCODE_A
#define KEY_S SDL_SCANCODE_S
#define KEY_D SDL_SCANCODE_D

//#define KEY_UP 82
#define KEY_LEFT SDL_SCANCODE_LEFT
//#define KEY_DOWN 81
#define KEY_RIGHT SDL_SCANCODE_RIGHT

#define KEY_SPACE SDL_SCANCODE_SPACE
#define KEY_F SDL_SCANCODE_F
#define KEY_G SDL_SCANCODE_G
#define KEY_ESC SDL_SCANCODE_ESCAPE
#define KEY_RETURN SDL_SCANCODE_RETURN
#define KEY_F1 SDL_SCANCODE_F1
#define KEY_F2 SDL_SCANCODE_F2

namespace AV{
	class Window : public EventReceiver{
	private:
		Ogre::RenderWindow *renderWindow;
		SDL_Window *window;

		int width = 800;
		int height = 600;

		int mouseX = 0;
		int mouseY = 0;

		void updateMouse();
		void updateKeys();

		int prevX, prevY;
		int offsetX, offsetY;
		int currentX, currentY;

		void keyPressed(SDL_Event &event);
		void keyReleased(SDL_Event &event);
		void resizeWindow(SDL_Event &event);
		void warpToCentre();

		void updateMouse(SDL_Event &event);

		bool open = false;
		bool mouseGrab = false;

		bool keys[300];

		SDL_Event e;

	public:
		Window();
		void update();

		bool getKey(int k);
		void setMouseGrab(bool grab);

		void notifyTypedEvent(TypedEvent &e);

		void close();
		bool isOpen();

		int xOffset, yOffset;

		Ogre::RenderWindow* getRenderWindow();
	};
}

#endif
