#pragma once
#include <string>

#include "SaveFileHandle.h"

#include <vector>

namespace AV{
  class SaveManager{
  public:
    static bool saveGame();
    static bool saveGame(const std::string &saveName);

    static void _getSaveList(std::vector<SaveFileHandle> &saveFiles);

  private:
    static bool _checkDirectoryStructure(const std::string &saveName);

    static std::string saveLocation;
  };
}
