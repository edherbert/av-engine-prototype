#include "EntitySerialisationManager.h"

#include "entityx/entityx.h"

#include "Components/PositionComponentSerialiser.h"
#include "Components/OgreMeshComponentSerialiser.h"
#include "Components/DamageTriggerComponentSerialiser.h"

#include <vector>
#include <string>

namespace AV{
  entityx::EntityManager* EntitySerialisationManager::entities = nullptr;

  void EntitySerialisationManager::serialiseEntities(const std::string& saveDirectoryPath, btDefaultSerializer *damageSerialiser){
    //Each valid entity will have a position component.
    //Iterating all of these will iterate all entities.
    int entityCount = 0;
    int* entityPointer = &entityCount;
    entities->each<PositionComponent>([entityPointer, damageSerialiser](entityx::Entity entity, PositionComponent &comp){
      processEntity((*entityPointer), entity, damageSerialiser);
      (*entityPointer)+=1;
    });

    std::cout << "Serialised " << entityCount << " entities." << '\n';
  }

  void EntitySerialisationManager::initialise(entityx::EntityManager* manager){
    EntitySerialisationManager::entities = manager;
  }

  void EntitySerialisationManager::processEntity(int count, entityx::Entity &e, btDefaultSerializer *damageSerialiser){
    std::vector<std::string> v;
    if(e.has_component<PositionComponent>()) PositionComponentSerialiser::serialise(v, e.component<PositionComponent>().get());
    if(e.has_component<OgreMeshComponent>()) OgreMeshComponentSerialiser::serialise(v, e.component<OgreMeshComponent>().get());
    if(e.has_component<DamageTriggerComponent>()) DamageTriggerComponentSerialiser::serialise(v, e.component<DamageTriggerComponent>().get(), count, damageSerialiser);

    for(std::string i : v){
      std::cout << i << '\n';
    }
  }
}
