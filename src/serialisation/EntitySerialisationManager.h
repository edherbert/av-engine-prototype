#pragma once

#include <string>

namespace entityx{
  class EntityManager;
  class Entity;
}

class btDefaultSerializer;

namespace AV{
  class EntitySerialisationManager{
  public:
    static void serialiseEntities(const std::string& saveDirectoryPath, btDefaultSerializer *damageSerialiser);

    static void initialise(entityx::EntityManager* manager);

  private:
    static entityx::EntityManager *entities;

    static void processEntity(int count, entityx::Entity &entity, btDefaultSerializer *damageSerialiser);
  };
}
