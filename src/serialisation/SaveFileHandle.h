#pragma once

namespace AV{
  class SaveFileHandle{
  public:
    SaveFileHandle();
    SaveFileHandle(const std::string& saveFileName);
    ~SaveFileHandle();

  private:
    std::string saveFileName;
  };
}
