#pragma once

#include "../../world/Entity/components/DamageTriggerComponent.h"

#include <string>
#include <vector>
#include "OgreStringConverter.h"

#include "LinearMath/btSerializer.h"

namespace AV{
  class DamageTriggerComponentSerialiser{
  public:
    static void serialise(std::vector<std::string> &vector, DamageTriggerComponent *comp, int eCount, btDefaultSerializer *serializer){
      vector.push_back("DamageTrigger");

      std::string countString = std::to_string(eCount);
      serializer->registerNameForPointer(comp->collisionObject, countString.c_str());

      vector.push_back(countString);
      vector.push_back(";");
    }
  };
}
