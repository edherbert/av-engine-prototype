#pragma once

#include "../../world/Entity/components/PositionComponent.h"

#include <string>
#include <vector>
#include "OgreStringConverter.h"

namespace AV{
  class PositionComponentSerialiser{
  public:
    static void serialise(std::vector<std::string> &vector, PositionComponent *comp){
      //Push some strings to the vector based on the component.
      vector.push_back("Position");

      vector.push_back(std::to_string(comp->pos.chunkX));
      vector.push_back(std::to_string(comp->pos.chunkY));
      vector.push_back(Ogre::StringConverter::toString(comp->pos.position));
      vector.push_back(";");
    }
  };
}
