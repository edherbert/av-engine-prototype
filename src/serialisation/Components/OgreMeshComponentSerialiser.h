#pragma once

#include "../../world/Entity/components/OgreMeshComponent.h"

#include <string>
#include <vector>
#include "OgreStringConverter.h"
#include "OgreItem.h"
#include "OgreMesh2.h"

namespace AV{
  class OgreMeshComponentSerialiser{
  public:
    static void serialise(std::vector<std::string> &vector, OgreMeshComponent *comp){
      vector.push_back("OgreMesh");

      Ogre::Item* item = (Ogre::Item*)comp->parentNode->getAttachedObject(0);

      vector.push_back(item->getMesh()->getName());
      vector.push_back(";");
    }
  };
}
