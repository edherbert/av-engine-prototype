#include "SaveManager.h"

#include <iostream>
#include "OgreFileSystemLayer.h"

#include "../world/Physics/DamageWorldManager.h"
#include "../world/Physics/BulletManager.h"

#include "EntitySerialisationManager.h"

#include "LinearMath/btSerializer.h"

namespace AV{
  std::string SaveManager::saveLocation = "/home/edward/Documents/avSaves/";
  //std::string SaveManager::saveLocation = "/avSaves";

  bool SaveManager::saveGame(){
    saveGame("first");
  }

  bool SaveManager::saveGame(const std::string &saveName){
    std::cout << "Saving save file: " << saveName << '\n';
    if(!_checkDirectoryStructure(saveName)) return false;

    std::string saveDirectory = saveLocation + saveName + "/";

    btDefaultSerializer *damageSerialiser = new btDefaultSerializer();
    EntitySerialisationManager::serialiseEntities(saveDirectory, damageSerialiser);

    DamageWorldManager::serialise(saveDirectory + "DamageWorld.bullet", damageSerialiser);
    BulletManager::serialise(saveDirectory);


    return true;
  }

  void SaveManager::_getSaveList(std::vector<SaveFileHandle> &saveFiles){

  }

  bool SaveManager::_checkDirectoryStructure(const std::string &saveName){
    //Try and create a directory then check if it actually exists.
    //If it doesn't then there was a write problem, so abort.
    //FileExists is the same thing as DirectoryExists.
    Ogre::FileSystemLayer::createDirectory(saveLocation);
    bool exists = Ogre::FileSystemLayer::fileExists(saveLocation);
    if(!exists) return false;

    //Now the directory exists, so try and write the contents directory.
    Ogre::FileSystemLayer::createDirectory(saveLocation + saveName);
    exists = Ogre::FileSystemLayer::fileExists(saveLocation + saveName);
    if(!exists) return false;

    //The directory for that save is ready to go.
    return true;
  }
};
