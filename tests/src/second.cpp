#include "gtest/gtest.h"
#include "../../src/world/Slots/SlotPosition.h"


TEST(SlotPosition, PositionZero){
    AV::SlotPosition position(0, 0, Ogre::Vector3::ZERO);
    EXPECT_EQ(position.position, Ogre::Vector3::ZERO);
}
