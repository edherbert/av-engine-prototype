local e = entity.createEntity(0, 0, 0, 0, 0);
component.ogreMesh.add(e, "ogrehead2.mesh");

component.actionTrigger.add(e);
component.actionTrigger.setActionResource(e, "dialog1", 0);

component.actionTrigger.setActionResource(e, "../resources/scripts/doorSecond.nut", 1);
component.actionTrigger.setActionType(e, 0, 1);
component.actionTrigger.setActionInputTrigger(e, 1, 1);
component.actionTrigger.setActionDescription(e, "Open door back to the old place", 1);

local n = entity.createEntity(0, 0, 0, 0, 0);
component.ogreMesh.add(n, "ogrehead2.mesh");
