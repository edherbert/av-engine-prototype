local data = {
  type = "cube",
  mass = 1,
  posX = 50,
  posY = 50,
  posZ = 0,
  scaleX = 1,
  scaleY = 50,
  scaleZ = 1
};


local e = entity.createEntity(0, 0, 100, 0, 0);
component.ogreMesh.add(e, "ogrehead2.mesh");

component.actionTrigger.add(e);
component.actionTrigger.setActionResource(e, "dialog1", 0);

component.actionTrigger.setActionResource(e, "../resources/scripts/door.nut", 1);
component.actionTrigger.setActionType(e, 0, 1);
component.actionTrigger.setActionInputTrigger(e, 1, 1);
component.actionTrigger.setActionDescription(e, "Open door", 1);

local f = entity.createEntity(0, 0, 0, 0, 0);
component.ogreMesh.add(f, "ogrehead2.mesh");

//component.rigidBody.add(f, 2, 0, 50, 0, 1, 40, 1, 1);
component.rigidBody.addCharacterController(f);
//component.rigidBody.remove(f);
//component.rigidBody.add(f, data);

//component.rigidBody.addSphere(f, pos, radius)
//component.rigidBody.addSphere(f, pos, radius, mass, friction)

entity.setController(f, 1);
